#ifndef color_h
#define color_h

struct Rgb
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

struct Rgba
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
};

#endif
