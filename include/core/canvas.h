#ifndef canvas_h
#define canvas_h

#include "color.h"

class Canvas
{
public:
	virtual int width() const=0;
	virtual int height() const=0;
	virtual void draw_line(float start_x, float start_y, float end_x, float end_y, int width, const Rgb color)=0;
	virtual void draw_rect(float x, float y, int width, int height, int width, const Rgb border)=0;
	virtual void draw_rect(float x, float y, int width, int height, int width, const Rgb border, const Rgb fill)=0;
	virtual void draw_text(float x, float y, const std::string & text)=0;
	virtual void clear()=0;
};

#endif
