#ifndef WORKER_H
#define WORKER_H

#include <deque>
#include <functional>
#include <thread>
#include <mutex>
#include <condition_variable>

#ifdef IS_THREAD_DEBUGGING
std::mutex DEBUG_MUTEX;
#endif

template <class ...Args>
inline void THREAD_DEBUG(Args ...args)
{
#ifdef IS_THREAD_DEBUGGING
	std::lock_guard<std::mutex> lock(DEBUG_MUTEX);
	std::printf(args...);
#endif
}

template <>
inline void THREAD_DEBUG<const char *>(const char * msg)
{
#ifdef IS_THREAD_DEBUGGING
	std::lock_guard<std::mutex> lock(DEBUG_MUTEX);
	std::printf("%s", msg);
#endif
}

class Worker
{
public:
	Worker();
	~Worker();

private:
	Worker(const Worker & that);
	Worker & operator=(const Worker & that);

public:
	void dispatch(std::function<void()> task);
	void clear_all_tasks();
	std::size_t n_loading();

private:
	void run();

private:
	bool _running;
	std::deque<std::function<void()>> _task_queue;
	std::mutex _resource_mutex;
	std::condition_variable _cv;
	std::thread _worker_thread;;
};

Worker::Worker()
	: _running(true)
	, _task_queue()
	, _worker_thread(&Worker::run, this)
{
	THREAD_DEBUG("worker up...\n");
}

Worker::~Worker()
{
	THREAD_DEBUG("worker down...\n");
	{
		std::lock_guard<std::mutex> lock(_resource_mutex);
		_running = false;
	}
	THREAD_DEBUG("notify_all\n");
	_cv.notify_all();
	_worker_thread.join();
}

void Worker::dispatch(std::function<void()> task)
{
	{
		std::lock_guard<std::mutex> lock(_resource_mutex);
		_task_queue.push_back(task);
		THREAD_DEBUG("dispatch a task\n");
	}
	_cv.notify_one();
}

void Worker::clear_all_tasks()
{
	std::lock_guard<std::mutex> lock(_resource_mutex);
	_task_queue.clear();
	THREAD_DEBUG("clear all tasks\n");
}

std::size_t Worker::n_loading()
{
	std::lock_guard<std::mutex> lock(_resource_mutex);
	return _task_queue.size();
}

void Worker::run()
{
	THREAD_DEBUG("running...\n");
	std::function<void()> task;
	while (true)
	{
		{
			std::unique_lock<std::mutex> cv_lock(_resource_mutex);
			THREAD_DEBUG("about to check condition... sizes:%d running:%d\n", _task_queue.size(), _running);
			while (_task_queue.empty() && _running)
			{
				THREAD_DEBUG("start waiting... tasks:%d running:%d\n", _task_queue.size(), _running);
				_cv.wait(cv_lock);
				THREAD_DEBUG("stop waiting... tasks:%d running:%d\n", _task_queue.size(), _running);
			}
			THREAD_DEBUG("leave wait scope... tasks:%d running:%d\n", _task_queue.size(), _running);
			if (!_running)
			{
				break;
			}

			task = std::move(_task_queue.front());
			_task_queue.pop_front();
			THREAD_DEBUG("pop a task\n");
		}

		THREAD_DEBUG("begin task\n");
		task();
		THREAD_DEBUG("finish task\n");
	}
	THREAD_DEBUG("stop running...\n");
}

#endif
