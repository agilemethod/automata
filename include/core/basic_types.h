#ifndef BASIC_TYPES_H
#define BASIC_TYPES_H

template <class Type>
struct Point
{
	Point()
		: x(0), y(0)
	{
	}

	Point(Type ix, Type iy)
		: x(ix), y(iy)
	{
	}

	bool operator==(const Point & that) const
	{
		return x == that.x && y == that.y;
	}

	Type x;
	Type y;
};

template <class Type>
struct Rect
{
	Rect()
		: x(0), y(0), width(0), height(0)
	{
	}

	Rect(Type ix, Type iy, Type iwidth, Type iheight)
		: x(ix), y(iy), width(iwidth), height(iheight)
	{
	}

	Type x;
	Type y;
	Type width;
	Type height;
};

template <class Type>
struct Size
{
	Size()
		: width(0), height(0)
	{
	}

	Size(Type iw, Type ih)
		: width(iw), height(ih)
	{
	}

	Type width;
	Type height;
};

#endif
