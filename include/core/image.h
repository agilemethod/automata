#ifndef IMAGE_H
#define IMAGE_H

#include <string>
#include <memory>
#include "color.h"

enum PixelFormat
{
	PF_8U_1C,
	PF_8U_2C,
	PF_8U_3C,
	PF_8U_4C,
	PF_RGBA8888,
	PF_RGB888,
	PF_RGB565,
	PixelFormatEnd,
};

struct ImagePimpl;

class Image
{
public:
	Image();
	Image(const std::string & path);
	Image(const uint8_t * data, std::size_t width, std::size_t height, PixelFormat pixel_format);

public:
	Image(const Image & that);
	Image & operator=(const Image & that);

public:
	~Image();

public:
	const uint8_t * data() const;
	std::size_t width() const;
	std::size_t height() const;
	PixelFormat pixel_format() const;
	std::size_t byte_count() const;
	Image crop(std::size_t x, std::size_t y, std::size_t width, std::size_t height) const;
	Image flip(int mode) const;
	void load(const std::string & path);
	void save(const std::string & path) const;
	void resize(std::size_t width, std::size_t height);
	Image & gray();

public:
	ImagePimpl * _pimpl;
};


#endif
