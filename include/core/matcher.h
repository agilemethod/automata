#ifndef MATCHER_H
#define MATCHER_H

#include <memory>
#include <tuple>
#include <vector>
#include <core/image.h>

struct MatchMethod
{
	static MatchMethod Sqdiff();
	static MatchMethod SqdiffNormed();
	static MatchMethod Ccorr();
	static MatchMethod CcorrNormed();
	static MatchMethod Ccoeff();
	static MatchMethod CcoeffNormed();

	int method;
	bool prefer_higher_value;
};


class MatchResult
{
public:
	MatchResult();
	~MatchResult() = default;

public:
	std::vector<float> score() const;
	float max() const;
	float min() const;
	std::tuple<int, int> max_location() const;
	std::tuple<int, int> min_location() const;
	std::size_t count() const;

	void normalize();

private:
	void update();

private:
	friend class Matcher;

private:
	struct PIMPL;
	std::shared_ptr<PIMPL> member;

#ifndef NDEBUG
public:
	void setup(uint8_t version);
#endif
};


class Matcher
{
public:
	MatchMethod match_method() const { return _match_method; }

	void set_match_method(const MatchMethod & match_method) { _match_method = match_method; }
	MatchResult match(const Image & source, const Image & templ);
	
	double mean_hsv_distance(const Image &source, uint32_t h, uint32_t s, uint32_t v);

private:
	MatchMethod _match_method;
};

#endif
