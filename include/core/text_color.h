#ifndef TEXT_COLOR_H
#define TEXT_COLOR_H
#include <string>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

namespace text
{

static inline std::string red(const std::string & str)
{
	return std::string(ANSI_COLOR_RED + str + ANSI_COLOR_RESET);
}

static inline std::string green(const std::string & str)
{
	return std::string(ANSI_COLOR_GREEN + str + ANSI_COLOR_RESET);
}

static inline std::string yellow(const std::string & str)
{
	return std::string(ANSI_COLOR_YELLOW + str + ANSI_COLOR_RESET);
}

static inline std::string blue(const std::string & str)
{
	return std::string(ANSI_COLOR_BLUE + str + ANSI_COLOR_RESET);
}

static inline std::string magenta(const std::string & str)
{
	return std::string(ANSI_COLOR_MAGENTA + str + ANSI_COLOR_RESET);
}

static inline std::string cyan(const std::string & str)
{
	return std::string(ANSI_COLOR_CYAN + str + ANSI_COLOR_RESET);
}

} // namespace

#endif
