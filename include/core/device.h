#ifndef CORE_DEVICE_H
#define CORE_DEVICE_H

struct Device
{
public:
	enum PlatformType
	{
		AndroidMobile,
		AndroidBluestacks1,
		AndroidBluestacks2,
	};


private:
	Device()
		: platform(AndroidMobile)
	{
	}

public:
	static Device & instance()
	{
		static Device instance;
		return instance;
	}

public:
	int platform;
};

#endif
