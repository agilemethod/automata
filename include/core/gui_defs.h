#ifndef gui_defs_h
#define gui_defs_h

enum ActionEvent
{
	ActionEventBegin,
	Down,
	Up,
	Move,
	ActionEventEnd
};

typedef const std::string & WidgetKey;

#endif
