#ifndef gesture_h
#define gesture_h

#include <vector>
#include "basic_types.h"

class Gesture
{
public:
	void tap(std::size_t x, std::size_t y, float interval_ms);
	void swipe(std::size_t start_x, std::size_t start_y, std::size_t end_x, std::size_t end_y, float interval_ms);
	void navigate(const std::vector<Point<std::size_t>> & paths, float interval_ms);
    void navigate(const std::vector<Point<std::size_t>> & paths, float interval_ms, bool press, bool release);
};

#endif
