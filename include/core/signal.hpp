#ifndef SIGNAL_HPP
#define SIGNAL_HPP

#include <map>
#include <functional>

template <class Func>
class Signal
{
public:
	Signal();

public:
	size_t connect(std::function<Func> && func);
	bool disconnect(size_t slot_id);
	void disconnect_all();
	size_t slot_counts() const;

	template<class ...Args>
	void emit(Args ...args)
	{
		for (auto & slot : _slots)
		{
			slot.second(args...);
		}
	}

private:
	std::map<size_t, std::function<Func>> _slots;
	size_t _slots_counter;
};

template <class Func>
Signal<Func>::Signal()
	: _slots_counter(0)
{
}

template <class Func>
size_t Signal<Func>::connect(std::function<Func> && func)
{
	size_t slot_id = _slots_counter++;
	_slots.insert(std::pair<size_t, std::function<Func>>(slot_id, func));
	return slot_id;
}

template <class Func>
bool Signal<Func>::disconnect(size_t slot_id)
{
	auto it = _slots.find(slot_id);
	if (it != _slots.end())
	{
		_slots.erase(it);
		return true;
	}
	return false;
}

template <class Func>
void Signal<Func>::disconnect_all()
{
	_slots.clear();
	_slots_counter = 0;
}

template <class Func>
size_t Signal<Func>::slot_counts() const
{
	return _slots.size();
}

#endif
