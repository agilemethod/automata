#ifndef wiget_h
#define wiget_h

#include <functional>

class Widget
{
public:
	virtual void handle_on_action_up(float x, float y)=0;
	virtual void handle_on_action_down(float x, float y)=0;
	virtual void handle_on_action_move(float x, float y)=0;
};

class Button : public Widget
{
private:
	typedef std::function<void(float, float)> action_callback_t;

public:
	virtual void on_clicked(action_callback_t cb)=0;
};

class Switch: public Widget
{
private:
	typedef std::function<void(float, float, bool)> action_callback_t;

public:
	virtual void on_changed(action_callback_t cb)=0;
};

class Touch: public Widget
{
private:
	typedef std::function<void(float, float)> action_callback_t;

public:
	virtual void on_pressed(action_callback_t cb)=0;
	virtual void on_moved(action_callback_t cb)=0;
	virtual void on_released(action_callback_t cb)=0;
};

#endif
