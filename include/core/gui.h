#ifndef gui_h
#define gui_h

#include "gui_def_h"

//*** example
//Button play(BTN_PLAY);
//play.on_clicked(start_game);

class Gui
{
public:
	virtual void action_up(WidgetKey widget_key, float x, float y)=0;
	virtual void action_down(WidgetKey widget_key, float x, float y)=0;
	virtual void action_move(WidgetKey widget_key, float x, float y)=0;
};

#endif
