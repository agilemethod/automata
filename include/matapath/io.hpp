#ifndef MATAPATH_IO_HPP
#define MATAPATH_IO_HPP

#include <thirdparty/json.hpp>
#include <matapath/basic_types.h>
#include <matapath/match_result.h>

// nlohmann::json
static nlohmann::json & operator<<(nlohmann::json & result, const MatchResult & mr)
{
	result["combo_count"] = mr.combo_count;
	result["first_combo_count"] = mr.first_combo_count;
	result["column_rune_count"] = nlohmann::json::array();
	for (size_t i = 0; i < 6; i++)
	{
		result["column_rune_count"].push_back(mr.column_rune_count[i]);
	}
	result["lowest_rune_count"] = mr.lowest_rune_count;
	result["total_puzzle_count"] = mr.total_puzzle_count;

	result["rune_results"] = nlohmann::json::array();
	for (size_t i = 1; i < TosRune::RuneTypeEnd; i++)
	{
		nlohmann::json rune_result;
		rune_result["total_count"] = mr.rune_results[i].total_count;
		rune_result["count"] = mr.rune_results[i].count;
		rune_result["times"] = mr.rune_results[i].times;;
		rune_result["enhanced_count"] = mr.rune_results[i].enhanced_count;
		rune_result["puzzle_count"] = mr.rune_results[i].puzzle_count;
		rune_result["column_count"].push_back(mr.rune_results[i].column_count[0]);
		rune_result["column_count"].push_back(mr.rune_results[i].column_count[1]);
		rune_result["column_count"].push_back(mr.rune_results[i].column_count[2]);
		rune_result["column_count"].push_back(mr.rune_results[i].column_count[3]);
		rune_result["column_count"].push_back(mr.rune_results[i].column_count[4]);
		rune_result["column_count"].push_back(mr.rune_results[i].column_count[5]);
		rune_result["has_first_round"] = mr.rune_results[i].has_first_round;
		rune_result["max_link_count"] = mr.rune_results[i].max_link_count;
		rune_result["has_full_row"] = mr.rune_results[i].has_full_row;

		result["rune_results"].push_back(rune_result);
	}

	return result;
}

static std::ostream & operator<<(std::ostream & os, const MatchResult & mr)
{
	nlohmann::json tmp;
	tmp << mr;
	return os << tmp.dump();
}

static nlohmann::json & operator<<(nlohmann::json & result, const Match & match)
{
	result["type"] = match.type;
	result["count"] = match.count;
	result["round"] = match.round;
	result["enhanced_count"] = match.enhanced_count;
	result["row_count"] = nlohmann::json::array();
	result["col_count"] = nlohmann::json::array();
	for (size_t i = 0; i < 5; i++)
	{
		result["row_count"].push_back(match.row_count[i]);
	}
	for (size_t i = 0; i < 6; i++)
	{
		result["col_count"].push_back(match.col_count[i]);
	}

	return result;
}

static std::ostream & operator<<(std::ostream & os, const Match & match)
{
	nlohmann::json tmp;
	tmp << match;
	return os << tmp.dump();
}

template <class T>
static nlohmann::json & operator<<(nlohmann::json & result, const Rect<T> & rect)
{
	result["x"] = rect.x;
	result["y"] = rect.y;
	result["width"] = rect.width;
	result["height"] = rect.height;

	return result;
}

template <class T>
std::ostream & operator<<(std::ostream & os, const Rect<T> & rect)
{
	nlohmann::json tmp;
	tmp << rect;
	return os << tmp.dump();
}

#endif
