#ifndef MATCH_RESULT_H
#define MATCH_RESULT_H

#include <matapath/basic_types.h>

struct MatchResult
{
	MatchResult(const std::vector<Match> & matches, const Board & board);
	float calculate_score();

	uint8_t combo_count = 0;
	uint8_t first_combo_count = 0;
	uint8_t column_rune_count[6] = {0, 0, 0, 0, 0, 0};
	uint8_t lowest_rune_count = 0;
	uint8_t total_puzzle_count = 0;
	uint8_t high_match_count = 0;
	uint8_t low_match_count = 0;
	float lowest_rune_bonus = 0;
	
	struct RuneResult
	{
		uint8_t total_count = 0;
		uint8_t count = 0;
		uint8_t times = 0;
		uint8_t enhanced_count = 0;
		uint8_t puzzle_count = 0;
		uint8_t column_count[6] = {0, 0, 0, 0, 0, 0};
		bool has_first_round = false;
		bool has_second_round = false;
		uint8_t max_link_count = 0;
		bool has_full_row = false;
	};

	RuneResult rune_results[TosRune::RuneTypeEnd];
};

#endif
