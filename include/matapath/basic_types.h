#ifndef MP_BASIC_TYPES_H
#define MP_BASIC_TYPES_H

#include <stdint.h>
#include <vector>
#include <sstream>
#include <iostream>
#include <core/basic_types.h>
#include "rune.h"
#include "enums.h"
#include <thirdparty/json.hpp>
#include <core/device.h>

struct BoardCell
{
	bool operator==(const BoardCell & that) const
	{ return rune.type == that.rune.type; }

	bool operator!=(const BoardCell & that) const
	{ return rune.type != that.rune.type; }

	Rune rune;
	
	bool has_puzzle = false;
};


template <class ElementType, std::size_t Rows, std::size_t Cols>
struct Vec2d
{
	enum { rows = Rows };
	enum { cols = Cols };
	ElementType data[Rows][Cols];
	
	bool operator==(const Vec2d & that) const
	{
		for (std::size_t i = 0; i < Rows; i++)
			for (std::size_t j = 0; j < Cols; j++)
				if (data[i][j] != that.data[i][j])
					return false;
		return true;
	}
};

template <class Vec2dType>
struct BoardBase : public Vec2dType
{
// methods
	uint8_t total_puzzle_count() const
	{
		if (!has_been_counted) count_rune();
		return puzzle_count;
	}

	uint8_t total_count(uint8_t type) const
	{
		if (!has_been_counted) count_rune();
		return rune_count[type];
	}

	void count_rune() const
	{
		has_been_counted = true;
		for (size_t i = 0; i < this->rows; i++)
		{
			for (size_t j = 0; j < this->cols; j++)
			{
				rune_count[this->data[i][j].rune.type]++;
				if (this->data[i][j].has_puzzle) puzzle_count++;
			}
		}
	}

// data	
private:
	mutable bool has_been_counted = false;
	mutable uint8_t rune_count[TosRune::RuneTypeEnd] = {0,};
	mutable uint8_t puzzle_count = 0;
};

typedef BoardBase<Vec2d<BoardCell, 5, 6>> Board;
typedef BoardBase<Vec2d<BoardCell, 8, 6>> BigBoard;
typedef Vec2d<uint8_t, 5, 6> StepLookUpBoard;

// helper
static void show_board(const Board & board)
{
	static std::vector<std::string> lookup;
	if (lookup.empty())
	{
		lookup.push_back("未");
		lookup.push_back("水");
		lookup.push_back("火");
		lookup.push_back("木");
		lookup.push_back("光");
		lookup.push_back("暗");
		lookup.push_back("心");
		lookup.push_back("問");
	}

	for (size_t i = 0; i < board.rows; i++)
	{
		for (size_t j = 0; j < board.cols; j++)
		{
			std::cout << lookup[board.data[i][j].rune.type] << " ";
		}
		std::cout << "\n";
	}
}

// helper
static Board make_board(const uint8_t input[], size_t rows, size_t cols)
{
	Board result;
	for (size_t i = 0; i < rows; i++)
	{
		for (size_t j = 0; j < cols; j++)
		{
			result.data[i][j].rune.type = input[i * cols + j];
		}
	}
	return result;
}

// helper
namespace {

std::vector<std::string> split(const std::string &s, char delim) {
	std::stringstream ss(s);
	std::string item;
	std::vector<std::string> elems;
	while (std::getline(ss, item, delim)) {
		elems.push_back(std::move(item));
	}
	return elems;
}

} // namespace
static Board make_board(const std::string & input, size_t rows, size_t cols)
{
	std::vector<uint8_t> input_vec;
	input_vec.reserve(rows * cols);
	auto values = split(input, ',');
	for (auto val : values)
	{
		uint8_t type(8);
		if (val == "b") type = 1;
		if (val == "r") type = 2;
		if (val == "g") type = 3;
		if (val == "y") type = 4;
		if (val == "p") type = 5;
		if (val == "h") type = 6;
		input_vec.push_back(static_cast<uint8_t>(type));
	}

	return make_board(input_vec.data(), rows, cols);
}


enum Direction
{
	RR=0,
	RB,
	BB,
	LB,
	LL,
	LU,
	UU,
	RU,
};

struct Match
{
	Match(uint8_t type, uint8_t count=0)
		: type(type)
		, count(count)
	{
	}

	uint8_t type;
	uint8_t count = 0;
	uint8_t round = 0;
	uint8_t enhanced_count = 0;
	uint8_t row_count[5] = {0, 0, 0, 0, 0};
	uint8_t col_count[6] = {0, 0 ,0, 0, 0, 0};
};


struct Solution
{
	Solution()
		: is_done(false)
		, weight(0)
	{
		for (std::size_t i = 0; i < StepLookUpBoard::rows; i++)
			for (std::size_t j = 0; j < StepLookUpBoard::cols; j++)
				step_lookup_board.data[i][j] = 0;
		matches.reserve(10);
	}

	bool operator<(const Solution & that) const
	{
		return weight < that.weight;
	}

	bool operator>(const Solution & that) const
	{
		return weight > that.weight;
	}

	Board board;
	Point<uint8_t> cursor;
	Point<uint8_t> init_cursor;
	std::vector<uint8_t> paths;
	bool is_done;
	float weight;
	std::vector<Match> matches;
	StepLookUpBoard step_lookup_board;
};

struct GamingConfig
{
	static GamingConfig & inst()
	{
		static GamingConfig inst;
		return inst;
	}
	
	GamingConfig() {
		for(std::size_t i = 0; i < MaxRuneSize; i++)
		{
			rune_priority[i] = Auto;
			rune_round_type[i] = Auto;
			rune_attack_type[i] = Auto;
			link_count[i] = 3;
			keep_enhance[i] = 0;
		}
	}

	enum { MaxRuneSize = TosRune::RuneTypeEnd };
	enum { MaxRuneLength = TosRune::RuneTypeEnd - 1 };

	int navigate_time = 2000;
	uint32_t width = 0;
	uint32_t height = 0;
	std::size_t max_solutions_size = 30 * 8 * 2 * 3;
	
	float rune_priority[MaxRuneSize]; // Low, Auto , High
	uint8_t rune_round_type[MaxRuneSize]; // Auto, First, Second
	uint8_t rune_attack_type[MaxRuneSize]; // Auto, Single, Multiple, Entire, FullRow
	uint8_t link_count[MaxRuneSize]; // 2 or 3
	uint8_t keep_enhance[MaxRuneSize]; // 0 or 1
	uint8_t puzzle_count = 0;
	uint8_t column_attack_type[6] = {1, 1, 1, 1, 1, 1}; // 4, 5
	uint8_t combo_count = 0; // 0 or n
	uint8_t first_combo_count = 0; // 0 or n
	uint8_t start_position = Auto;
	uint8_t max_steps = 48;
	uint8_t ignited_path = Auto;
	uint8_t device_type = Device::AndroidMobile;
	bool has_lowest_row = false;
	bool eight_direction_allowed = false;
	bool has_legency_greece = false;
	uint8_t babylon_type = TosRune::RuneNotFound;
};

enum Type{ Mass = 0, Normal };
struct Weight
{
	float value[2];
};

typedef Weight Weights[8]; // FIXME

struct SolveState
{
	SolveState(
		uint8_t imax_length, uint8_t idir_step, uint8_t ip,
		std::vector<Solution> & isolutions)
		: max_length(imax_length)
		, dir_step(idir_step)
		, p(ip)
		, solutions(isolutions)
	{
	}

	uint8_t max_length;
	uint8_t dir_step;
	uint8_t p;
	std::vector<Solution> & solutions;
};

// ============================================================
struct RuneRequirement
{
	enum RuneRequirementConfig
	{
		Nolimit,
		Link2,
		Link3,
		Link4,
		Link5,
		Link6,
		LinkAll,
		LinkNone
	};

	uint8_t type;
	uint8_t config;
	bool is_highorder;
};


struct Requirement
{
	uint8_t max_steps;
	bool eight_direction_allowed;
	std::vector<RuneRequirement> rune_requirements;
};


typedef std::vector<Point<uint8_t>> PathMap;

const uint8_t ROWS = 5;
const uint8_t COLS = 6;
const float MULTI_ORB_BONUS = 0.25;
const float COMBO_BONUS = 0.25;

// TODO: move this code
static nlohmann::json & operator>>(nlohmann::json & result, GamingConfig & gc)
{
	for (size_t i = 1; i < TosRune::RuneTypeEnd - 1; i++) // no hidden rune
	{
		gc.rune_priority[i] = result["rune_priority"][i];
		gc.rune_round_type[i] = result["rune_round_type"][i];
		gc.rune_attack_type[i] = result["rune_attack_type"][i];
		gc.keep_enhance[i] = result["keep_enhance"][i];
	}
	for (size_t i = 0; i < Board::cols; i++)
	{
		gc.column_attack_type[i] = result["column_attack_type"][i];
	}
	gc.width = result["width"];
	gc.height = result["height"];
	gc.navigate_time = result["navigate_time"];
	gc.combo_count = result["combo_count"];
	gc.first_combo_count = result["first_combo_count"];
	gc.start_position = result["start_position"];
	gc.max_steps = result["max_steps"];
	gc.ignited_path = result["ignited_path"];
	gc.max_solutions_size = result["max_solutions_size"];
	gc.has_lowest_row = result["has_lowest_row"];
	gc.eight_direction_allowed = result["eight_direction_allowed"];
	gc.has_legency_greece = result["has_legency_greece"];
	gc.babylon_type = result["babylon_type"];
	gc.device_type = result["device_type"];
	return result;
}


static nlohmann::json & operator<<(nlohmann::json & result, const Rune & rune)
{
	result["type"] = rune.type;
	result["property"] = rune.property;
	result["priority"] = rune.priority;
	result["moveable"] = rune.moveable;
	result["enhanced"] = rune.enhanced;
	result["matched"] = rune.matched;
	
	return result;
}

static nlohmann::json & operator>>(nlohmann::json & result, Rune & rune)
{
	rune.type = result["type"];
	rune.priority = result["priority"];
	rune.moveable = result["moveable"];
	
	return result;
}

static nlohmann::json & operator<<(nlohmann::json & result, const BoardCell & bc)
{
	result["puzzle"] = bc.has_puzzle;
	result["rune"] << bc.rune;
	
	return result;
}

static nlohmann::json & operator>>(nlohmann::json & result, BoardCell & bc)
{
	bc.has_puzzle = result["puzzle"];
	result["rune"] >> bc.rune;
	
	return result;
}

static nlohmann::json & operator<<(nlohmann::json & result, const Board & board)
{
	for (size_t i = 0; i < board.rows; i++)
	{
		result["cells"].push_back(nlohmann::json());
		for (size_t j = 0; j < board.cols; j++)
		{
			nlohmann::json tmp;
			tmp << board.data[i][j];
			result["cells"][i].push_back(tmp);
		}
	}
	
	return result;
}

static nlohmann::json & operator>>(nlohmann::json & result, Board & board)
{
	for (size_t i = 0; i < board.rows; i++)
	{
		for (size_t j = 0; j < board.cols; j++)
		{
			result["cells"][i][j] >> board.data[i][j];
		}
	}
	
	return result;
}


static std::ostream & operator<<(std::ostream & os, const Board & board)
{
	nlohmann::json tmp;
	tmp << board;
	return os << tmp.dump();
}


#endif
