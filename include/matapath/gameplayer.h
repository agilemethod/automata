#ifndef GAMEPLAYER_H
#define GAMEPLAYER_H

#include <functional>
#include <memory>
#include <string>
#include <core/signal.hpp>
#include <matapath/basic_types.h>
#include <matapath/tos_database.h>
#include <cstdlib>
#include <core/gesture.h>

class RuneProvider;

class GamePlayer
{
public:
	GamePlayer();
	
    void register_respond(std::function<void(const std::string &)> respond) { _respond = respond; }
    std::string execute(const std::string & command);
	Board load_board(const std::string & filepath=std::string());
	PathMap calculate_path(Board board, const std::vector<Point<std::size_t>> & paths);
	PathMap calculate_path(Board board, const std::vector<Point<std::size_t>> & paths, std::vector<Solution> & solutions);
    void peek_and_play(const std::string & filepath=std::string());
	bool board_is_stable() const;
    void request_administrator();

// signals
public:
	Signal<void(const std::string &)> on_execute_done;
	
private:
    std::function<void(const std::string &)> _respond;
	std::vector<std::string> ids;
	TosDatabase _tos_database;
	Gesture _gesture;

#ifdef __APPLE__
public:
	bool debug_is_android_image = false;
	size_t debug_android_width = 1080;
	size_t debug_android_height = 1776;
#endif
};

#endif
