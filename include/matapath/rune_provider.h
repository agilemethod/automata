#ifndef RUNE_PROVIDER_H
#define RUNE_PROVIDER_H

#include <memory>
#include <vector>
#include <core/image.h>
#include <core/basic_types.h>
#include <matapath/rune.h>

struct RuneSample
{
	Image image;
	float threshold;
	uint8_t type;
	uint8_t prop;
};


class RuneProvider
{
public:
	RuneProvider(const std::string & data);
	
public:
	std::vector<RuneSample> samples;
};

#endif
