#ifndef ga_traker_h
#define ga_traker_h

#include <string>
#include <functional>

#ifndef __APPLE__
#include "android/log.h"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)
#else
#define LOGD(...) ;
#endif

class GATracker
{
public:
	
	typedef std::function<void(std::string)> CALLER_SCREEN_TYPE;
	typedef std::function<void(std::string, std::string, std::string)> CALLER_EVENT_TYPE;
	
	static GATracker & getInstance()
	{
		static GATracker inst;
		return inst;
	}
	
	void sendTrackerScreen(const std::string & name)
	{
		screen_caller(name);
	}
	
	void sendTrackerEvent(const std::string &action, const std::string & category, const std::string & label)
	{
		event_caller(action, category, label);
	}
	
	void setJavaScreenCaller(std::function<void(std::string)> func)
	{
		screen_caller = func;
	}
	
	void setJavaEventCaller(std::function<void(std::string, std::string, std::string)> func)
	{
		event_caller = func;
	}
	
private:
	
	GATracker()
	{
	}
	
	CALLER_SCREEN_TYPE screen_caller = [](std::string name){};
	CALLER_EVENT_TYPE event_caller = [](std::string action, std::string category, std::string label){};
};


#endif
