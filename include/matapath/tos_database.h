#ifndef TOS_DATABASE_H
#define TOS_DATABASE_H

#include <string>

class TosDatabase
{
public:
	static std::string getTosPath(const std::string & prefix);
	static std::string getSavePath(const std::string & path);
	std::string read(const std::string & filepath);
	void copy(const std::string & from, const std::string & to);
	void backup(const std::string & path);
	void recovery(const std::string & path);
	void remove(const std::string & path);
};


#endif
