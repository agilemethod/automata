#ifndef LAYOUT_H
#define LAYOUT_H

#include <memory>
#include <core/basic_types.h>

class GameLayout
{
public:
	virtual float game_screen_ratio() const=0;
	virtual float board_ratio() const=0;
	virtual float board_x() const=0;
	virtual float board_y() const=0;
};

class TosGameLayout : public GameLayout
{
public:
	virtual float game_screen_ratio() const { return 1.5; }
	virtual float board_ratio() const { return 5.0 / 6; }
	virtual float board_x() const { return 0; }
	virtual float board_y() const { return 342.313 / 768; } // 430 // 960  // 342 / 768
};

class LayoutManager
{
public:
	LayoutManager(std::shared_ptr<GameLayout> game_layout)
		: _game_layout(game_layout)
	{
	}

public:
	std::size_t n_rows() const;
	std::size_t n_cols() const;
	Rect<std::size_t> board() const;
	Rect<std::size_t> screen() const;
	Rect<std::size_t> cell(std::size_t row, std::size_t col) const;
	Rect<std::size_t> vborder(std::size_t row, std::size_t col) const;
	Rect<std::size_t> hborder(std::size_t row, std::size_t col) const;

	void set_rows_count(std::size_t count) { _n_rows = count; }
	void set_cols_count(std::size_t count) { _n_cols = count; }
	void set_game_layout(std::shared_ptr<GameLayout> game_layout) { _game_layout = game_layout; }
	void set_resolution(float width, float height);
	void reset_board_size(float width, float height); // only effect border

private:
	std::shared_ptr<GameLayout> _game_layout;
	Rect<std::size_t> _screen;
	Rect<std::size_t> _board;
	std::size_t _n_rows;
	std::size_t _n_cols;
	float _cell_width;
	float _cell_height;
	float _border_width;
	float _border_height;
};

#endif
