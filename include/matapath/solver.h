#ifndef SOLVER_H
#define SOLVER_H

#include <vector>
#include "basic_types.h"

namespace Solver
{

typedef Point<uint8_t> Point;

void make_weights(Weights & weights);
void make_weights2(Weights & weights);
float compute_weight(const std::vector<Match> & matches, const Weights & weights);
void find_matches(std::vector<Match> & matches, Board & board, Board & match_board, Board * origin_board=NULL);
void remove_matches(Board & board, const Board & match_board);
void drop_empty_spaces(Board & board, uint8_t round=0);
void evaluate_solution(Solution & solution);
bool can_move_orb(Point rc, uint8_t dir);
void move_rc(Point & rc, uint8_t dir);
bool can_move_orb_in_solution(const Solution & solution, uint8_t dir);
Point swap_orb(Board & board, Point rc, uint8_t dir);
void swap_orb_in_solution(Solution & solution, uint8_t dir);
void evolve_solutions(std::vector<Solution> & solutions, uint8_t dir_step);
void solve_board_step(SolveState & solve_state);

extern Solution best_done_solution;
}

#endif
