#ifndef RUNE_H
#define RUNE_H

#include "enums.h"

struct Rune
{
	Rune() : type(0), property(0)
	{
	}

	Rune(uint8_t itype, uint8_t iproperty)
		: type(itype), property(iproperty)
	{
	}

	uint8_t type;
	uint8_t property;
	int8_t priority = Auto;
	bool moveable = true;
	bool enhanced = false;
	bool matched = false; // used by solver
};

namespace TosRune
{
	enum RuneType
	{
		RuneNotFound=0,
		RuneWater=1,
		RuneFire,
		RuneWood,
		RuneLight,
		RuneDark,
		RuneHeart,
		RuneHidden,
		RuneTypeEnd,
	};

	enum RuneProp
	{
		PropNormal=1,
		PropEnhanced,
		PropIgnited,
		PropWild,
		PropFreezedInit,
		PropFreezed,
		PropLightning,
		PropEtched,
		PropLocked,
		PropLaser,
		RunePropEnd,
	};

	static void update(Rune & rune, uint8_t type, uint8_t property)
	{
		rune.type = type;
		rune.property = property;

		switch (property)
		{
		case PropIgnited:
			rune.moveable = false;
			break;
		case PropWild:
			rune.moveable = false;
			rune.priority = High;
			break;
		case PropFreezedInit:
			rune.priority = High;
			break;
		case PropFreezed:
			//rune.type = RuneHidden;
			rune.priority = Low;
			break;
		case PropLightning:
			rune.moveable = false;
			rune.priority = High;
			break;
		case PropEtched:
			rune.moveable = false;
			break;
		case PropLocked:
			rune.priority = High;
			break;
		case PropLaser:
			rune.moveable = false;
		defalut:
			break;
		}
	}
}


#endif
