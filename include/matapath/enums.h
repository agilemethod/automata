#ifndef ENUMS_H
#define ENUMS_H

enum CommonType { Auto=0 };
enum PriorityType { Low=-1, High=1 };
enum RoundType { First=1, Second };
enum AttackType { Single=1, Multiple, TwoLink, Entire, FullRow };
enum StartPositionType {LeftBottom=1, RightBottom=2};
enum IgnitedPathType {Short=6, Long=30};

#endif
