#ifndef EYE_H
#define EYE_H

#include <automata/core/image.h>

class Eye
{
private:
	Eye();

public:
	Image take();

public:
	static Eye & instance();

public:
	struct Pimpl;
	Pimpl * _pimpl;
};

#endif
