#ifndef systemcall_h
#define systemcall_h
#include <automata/matapath/ga_tracker.hpp>
#include <automata/core/image.h>

#include <stdio.h>
#include <string>
#include <unistd.h>
#include <signal.h>
#include <unistd.h>
#include <sstream>

#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifndef __APPLE__
#include "android/log.h"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)
#else
#define LOGD(...) ;
#endif

enum {
    HAL_PIXEL_FORMAT_RGBA_8888    = 1,
    HAL_PIXEL_FORMAT_RGBX_8888    = 2,
    HAL_PIXEL_FORMAT_RGB_888      = 3,
    HAL_PIXEL_FORMAT_RGB_565      = 4,
    HAL_PIXEL_FORMAT_BGRA_8888    = 5,
    HAL_PIXEL_FORMAT_RGBA_5551    = 6,
    HAL_PIXEL_FORMAT_RGBA_4444    = 7,
    HAL_PIXEL_FORMAT_YCbCr_422_SP = 0x10,
    HAL_PIXEL_FORMAT_YCbCr_420_SP = 0x11,
    HAL_PIXEL_FORMAT_YCbCr_422_P  = 0x12,
    HAL_PIXEL_FORMAT_YCbCr_420_P  = 0x13,
    HAL_PIXEL_FORMAT_YCbCr_422_I  = 0x14,
    HAL_PIXEL_FORMAT_YCbCr_420_I  = 0x15,
    HAL_PIXEL_FORMAT_CbYCrY_422_I = 0x16,
    HAL_PIXEL_FORMAT_CbYCrY_420_I = 0x17
};

extern char **environ;


class SystemCall
{
private:
	SystemCall()
	{
		LOGD("Matapath.device: start process");
		
		// call it in gameplayer
		//setenv("PATH", "/sbin:/vendor/bin:/system/sbin:/system/bin:/system/xbin:/su/bin", 1);
		
		char* pPath;
		pPath = getenv("PATH");
		LOGD("Matapath systemcall env path %s", pPath);

		// system call
		pid = getpid();
		fp = popen("su", "w");
		if (fp == NULL)
		{
			GATracker::getInstance().sendTrackerEvent("cError", "SystemCall", "Can not popen su");
			LOGD("Matapath.device: Can not popen su. errno: %d", errno);
		}
		// custom popen r/w
		pipe(iofds);
		pipe(oifds);
		cid = fork();
		if (cid == -1)
		{
			GATracker::getInstance().sendTrackerEvent("cError", "SystemCall", "Can not Fork");
			LOGD("Matapath.device: Can not Fork cid1: %d errno: %d", cid, errno);
		}
		if (cid == 0)
		{
			char *argp[] = {"sh", "-c", "su", NULL};
			// type w, parent->w, child->r
			close(oifds[1]);
			dup2(oifds[0], STDIN_FILENO);
			close(oifds[0]);
			// type r, parent->r, child->w
			close(iofds[0]);
			dup2(iofds[1], STDOUT_FILENO);
			close(iofds[1]);
			//execve("/system/bin/sh", argp, environ);
			execvp("sh", argp);
			exit(1);
		}
		close(iofds[1]);
		close(oifds[0]);
		in = fdopen(iofds[0], "r");
		out = fdopen(oifds[1], "w");
		if (in == NULL || out == NULL) {
			GATracker::getInstance().sendTrackerEvent("cError", "SystemCall", "Can not fdopen in/out");
			LOGD("Matapath.device: Can not fdopen in or out. errno: %d", errno);
		}
	}

	~SystemCall()
	{
		close(iofds[0]);
		close(oifds[1]);
		pclose(fp);
	}
	
public:
	static bool file_exists(const std::string & filename) {
	    if( access( filename.c_str(), F_OK ) != -1 ) {
		    return false;
		} else {
		    return true;
		}
    }
    
	static void execute(const std::string & cmd)
	{
		getInstance().execute_impl(cmd);
	}
	
	static int refresh()
    {
	    return getInstance()._refresh();
    }

    struct TimerGuard
    {
    	TimerGuard(const char * msg = 0)
    		: msg(msg)
    	{
    		start_time = timestamp();
    	}

    	~TimerGuard()
    	{
    		if (msg)
    		{
				end_time = timestamp();
				LOGD("%s %lld ms", msg, elasped(start_time, end_time));
			}
    	}

    	long long timestamp()
    	{
			struct timeval te; 
			gettimeofday(&te, NULL); // get current time
			long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // caculate milliseconds
			return milliseconds;
    	}

    	long long elasped(long long start, long long end)
    	{
    		return end - start;
    	}

		const char * msg;
    	long long start_time, end_time;
    };
    
    static Image screenshot() {
  	   //TimerGuard timer("Matapath screenshot");
	   return getInstance().screenshot_impl();
	}

	uint32_t w_ = 0, h_ = 0, format_ = 0, bpp_ = 0;
	uint32_t total_ = 0;
	PixelFormat pf;
	void * screenshot_buf_ = NULL;
	short buf_offset_ = 0;

	// not show using su every time
	Image screenshot_impl() {
		if (in == NULL || out == NULL)
		{
			return Image();
		}
		fputs("screencap\n", out);
		fflush(out);
		
		std::size_t length = 0;
		if (feof(in))
		{
			GATracker::getInstance().sendTrackerEvent("cError", "SystemCall", "File in is feof");
			LOGD("Matapath.device: File in is feof");
			return Image();
		}
		if (ferror(in))
		{
			GATracker::getInstance().sendTrackerEvent("cError", "SystemCall", "File in is error");
			LOGD("Matapath.device: File in is error");
			return Image();
		}

		if (total_ == 0)
		{
			char buffer[64];
			fread(buffer, 1, 4, in);
			w_ = *((uint32_t *)buffer);
			fread(buffer, 1, 4, in);
			h_ = *((uint32_t *)buffer);
			fread(buffer, 1, 4, in);
			format_ = *((uint32_t *)buffer);
			if (format_ == HAL_PIXEL_FORMAT_RGB_565)
			{
				bpp_ = 2;
				pf = PF_RGB565;
			}
			else if (format_ == HAL_PIXEL_FORMAT_RGB_888)
			{
				bpp_ = 3;
				pf = PF_RGB888;
			}
			else
			{
				bpp_ = 4;
				pf = PF_RGBA8888;
			}
			total_ = w_ * h_ * bpp_;


			if (total_ > 0 && total_ <= 2400 * 2400 * 4)
			{
				screenshot_buf_ = malloc(total_);
			}
			buf_offset_ = 0;
		}
		
		
		if (total_ <= 0 || total_ > 2400 * 2400 * 4)
		{
			GATracker::getInstance().sendTrackerEvent("cError", "SystemCall", "Can not screenshot total_ <= 0");
			LOGD("Matapath.device: Can not screenshot_impl total_: %d", total_);
			return Image();
		}

		{
			//TimerGuard timer("Matapath screenshot fread");
			length = fread(screenshot_buf_, 1, total_ + buf_offset_, in);
			//read(iofds[0], screenshot_buf_, total_ + buf_offset_);
			//length = 1;
		}
		if (length <= 0)
		{
			GATracker::getInstance().sendTrackerEvent("cError", "SystemCall", "Can not screenshot length <= 0");
			LOGD("Matapath.device: Can not screenshot_impl length: %d", length);
			return Image();
		}
	    
	    Image img = Image((const uint8_t*)screenshot_buf_ + buf_offset_, w_, h_, pf);
	    if (buf_offset_ == 0) buf_offset_ = 12;
		//img.save("/storage/emulated/legacy/tmp/screen.png");
		_screen_width = img.width();
		_screen_height = img.height();
	    return img;
	}
	
	static std::size_t screen_width()
	{
		return getInstance()._screen_width;
	}
	
	static std::size_t screen_height()
	{
		return getInstance()._screen_height;
	}
	
    // not show using su every time <== old method
    static Image screenshot2() {

	    FILE* pfile = popen("su -c screencap", "r");
	    //FILE* pfile = popen("/system/bin/screencap", "r");
	    
	    uint32_t w, h, bpp;
	    char buffer[1024];
	    
	    fread(buffer, 1, 4, pfile);
	    w = buffer[0]|buffer[1]<<8|buffer[2]<<16|buffer[3]<<24;
	    fread(buffer, 1, 4, pfile);
	    h = buffer[0]|buffer[1]<<8|buffer[2]<<16|buffer[3]<<24;
	    fread(buffer, 1, 4, pfile);
	    bpp = buffer[0]|buffer[1]<<8|buffer[2]<<16|buffer[3]<<24;
	    
	    std::size_t length = 0;
	    std::size_t total = 0;
	    std::stringstream data;
	    int k = 0;
	    //data.seekp(0, std::ios::end);
	    while (!feof(pfile)) {
		    length = fread(buffer, 1, 1024, pfile);
	        data.write(buffer, length);
	        total += length;
	        k++;
	    }
	    pclose(pfile);
	    
	    bpp = total / w / h;

	    LOGD("===> %u %u %u %u %u", data.str().length(), total, bpp, data.tellg(), k); // 8294400 8294400 4 0 0
	    
	    
	    void* tmp = malloc(total);
		memcpy(tmp, data.str().c_str(), total);
		
		Image img = Image((const uint8_t*)tmp, w, h, PF_RGBA8888);
		//img.save("/storage/emulated/legacy/tmp/screen.png");
	    return img;
	   
	    //
	    //return ;
	}

protected:
	static SystemCall & getInstance() {
		static SystemCall instance;
		return instance;
	}
	
	void execute_impl(const std::string & cmd)
	{
		fputs(cmd.c_str(), fp);
		fflush(fp);
	}
	
	int _refresh() {
		//int status = pclose(fp); // wait for doing commands
        //fp = popen("/system/xbin/su", "w");
        //return status;
        
        char kill_cmd [30];
        sprintf (kill_cmd, "kill -%d %d\n", SIGCONT, pid);
		execute_impl("sleep 0.1\n");
        execute_impl(kill_cmd);
        // wait for continue signal
        kill(pid, SIGSTOP);
        return 1;
	}
	
private:
	FILE *fp;
	int pid;
	int iofds[2];
	int oifds[2];
	int cid;
	FILE *in;
	FILE *out;
	std::size_t _screen_width = 0;
	std::size_t _screen_height = 0;
};

#endif
