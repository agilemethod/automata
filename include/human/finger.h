#ifndef FINGER_H
#define FINGER_H

#include <cstddef>

class Finger
{
private:
	Finger();

public:
	void press(std::size_t x, std::size_t y);
	void move(std::size_t x, std::size_t y);
	void release();
	void fast_tap(std::size_t x, std::size_t y);

public:
	static Finger & instance();

public:
	struct Pimpl;
	Pimpl * _pimpl;
};

#endif
