#ifndef MATASHFIT_BASIC_TYPES_H
#define MATASHFIT_BASIC_TYPES_H

struct Box
{
	enum Type
	{
		T_BLUE,
		T_RED,
		T_GREEN,
		T_YELLOW,
		T_PINK,
		T_COPPER,
		T_SLIVER,
		T_GOLD,
		T_DIAMOND,
		T_GRAY,
		T_NOFOUND,
	};

	enum Status
	{
		S_NORMAL,
		S_BLINK,
		S_FALL,
	};

	uint8_t type;
	uint8_t status;
	bool remove;
	int y;
};

#endif
