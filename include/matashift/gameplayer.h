#ifndef MATASHIFT_GAMEPLAYER_H
#define MATASHIFT_GAMEPLAYER_H

#include <string>
#include <thirdparty/json.hpp>
#include <core/image.h>
#include <core/matcher.h>
#include <core/gesture.h>
#include <matashift/basic_types.h>

// forward declaration
class Image;

class GamePlayer
{
public:
	virtual std::string execute(const std::string & command)=0;
};

class MataShiftGamePlayer: public GamePlayer
{
public:
	MataShiftGamePlayer();

public:
	virtual std::string execute(const std::string & command);
	void request_administrator();

private:
	std::string play_tutorial(const nlohmann::json & request);
	std::string renew_game(const nlohmann::json & request);
	std::string play(const nlohmann::json & request);
private:
	void determine_screen_status(const Image & screenshot);
	void close_chronosgate_app();
	std::string get_pid();
	void write_ps_into(const std::string & filepath);
	std::string get_app_name();
	void change_ownship(const std::string & filepath);
	void delete_chronosgate_data();
	std::vector<std::string> ids;
	int calculateScore(const std::vector<Box> & ori_board, uint8_t lowest_crystal);
	uint32_t exhaustive(const std::vector<Box> & ori_board, int keep_type, uint8_t lowest_crystal);

private:
	enum ScreenStatus
	{
		Unknown,
		WaitToPlay,
		WaitToTalk,
	};

	enum PlayStatus
	{
		SwipeRight,
		SwipeLeft,
	};

private:
	Gesture gesture;
	Matcher matcher;

	int _screen_status;
	int _play_status;
	int _target_stone_position_x;
	int _target_stone_position_y;
	float _threshold;
	std::string _folder_path;
	std::string _user_folder;
	float _swipe_right_score;
	float _swipe_left_score;
	bool _auto_crystal = false;
	bool _pull_copper = true;
	bool _pull_sliver = true;
	bool _pull_gold = true;
	bool _pull_diamond = true;
	int _normal_bouns_time_counter = 0;
	int _diamond_bouns_time_counter = 0;
	bool _is_playing = false;

	Image _arrow_right;
	Image _arrow_left;
};

#endif
