#ifndef IO_IMAGE_HPP
#define IO_IMAGE_HPP

#include <thirdparty/json.hpp>
#include <thirdparty/base64.hpp>
#include <core/image.h>

// nlohmann::json
static nlohmann::json & operator<<(nlohmann::json & result, const Image & img)
{
	result["width"] = img.width();
	result["height"] = img.height();
	result["pixel_format"] = static_cast<uint8_t>(img.pixel_format());
	std::string data = base64_encode(img.data(), img.byte_count());
	result["data"] = data;

	return result;
}

static nlohmann::json & operator>>(nlohmann::json & json, Image & img)
{
	std::string data = base64_decode(json["data"]);
	img = Image((uint8_t *)data.data(), json["width"], json["height"], (PixelFormat)(uint8_t)json["pixel_format"]);

	return json;
}

#endif
