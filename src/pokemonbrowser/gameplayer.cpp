#include <pokemonbrowser/gameplayer.h>

#include <cstdlib>
#include <ctime>
#include <string>
using json = nlohmann::json;

#ifndef __APPLE__
#include "android/log.h"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)
#else
#define LOGD(...) ;
#endif

namespace next
{

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

// get adid and decode
std::string decode(std::string s, int len)
{
	std::array<int, 20> ns{20, 2, 25, 8, 41, 59, 4, 21, 60, 11, 43, 15, 38, 47, 26, 62, 33, 39, 51, 17};
	std::string result = "";
	for (int i = 0; i < len; i++)
	{
		result += s[ns[i]];
	}
	return result;
}
	
	
std::string getKeys()
{
	struct timeval timeout;
	timeout.tv_sec = 3;
	timeout.tv_usec = 0;
	
	const char *domain = "elggum.com";
	const char *path = "automata/bRu39475Jdn10poaMisdgcQm";
	int sock;
	char send_data[1024], recv_data[2048];
	struct sockaddr_in server_addr;
	struct hostent *he;
	
	if ((he = gethostbyname(domain)) == NULL) return "";
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) ==  -1) return "";
	setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
	setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(80);
	server_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(server_addr.sin_zero), 8);
	
	if (connect(sock, (struct sockaddr *)&server_addr,sizeof(struct sockaddr)) == -1) return "";
	
	snprintf(send_data, sizeof(send_data), "GET /%s HTTP/1.1\r\nHost: %s\r\n\r\n", path, domain);
	send(sock, send_data, strlen(send_data), 0);
	
	long length = recv(sock, recv_data, 2048, 0);
	close(sock);
	
	if (length <= 20) return "";
	std::string r(recv_data, length);
	long p = r.rfind("\r\n");
	return r.substr(p + 2, r.size() - p - 2);
}
}


PokemonBrowser::PokemonBrowser()
{
	srand(time(NULL));

	// for ads
	std::string keys = next::getKeys();
	if (keys == "" || keys.find("iloveit") == std::string::npos)
	{
		keys = R"({"adid":[["9973734350037712446687960663502611005640865546903571560546828209","4019551646344389871518021149360771879961761349359604936032135150","4543236377769266894926157788518976531108940508687299507323194033","8371580662460268479076319606609648818074412458495494245417098182"],["2850025457008515132377705923107502506243243216729967959651983833","2068571776952882061946584887611844894988765175571665935314054353","0316626181587081977889349078994928771324455986435177603755818288","0273700277773160530333390156535574154194863400473629050285930618"]],"check_string":"iloveit"})";
	}
	else
	{
		//LOGD("Refresh ids success");
	}

	json json_keys = json::parse(keys)["adid"];
	for (int i = 0; i < json_keys.size(); i++)
	{
		if (json_keys[i].size() > 2)
		{
			std::string k1 = next::decode(json_keys[i][2], 16);
			std::string k2 = next::decode(json_keys[i][1], 10);
			ids.push_back(k1 + "/" + k2);
		}
	}
}

std::string PokemonBrowser::execute(const std::string & command)
{
	json request = json::parse(command);

	if (request["request"] == "get_ad_id")
	{
		std::vector<std::string> ids999;
		ids999.push_back("3434616449699457/4859433957");
		ids999.push_back("6493638586342373/9836378566");
		int random_idx = std::rand() % ids.size();
		return ids[random_idx];
	}

	return "unknown command: " + command;
}

