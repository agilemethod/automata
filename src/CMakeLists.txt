cmake_minimum_required (VERSION 2.6)

#file(GLOB HEADER_FILES ${CMAKE_SOURCE_DIR}/include/io/*.h*)
#add_library(automata_io ${HEADER_FILES})

add_subdirectory(core)
add_subdirectory(human)
add_subdirectory(matapath)
add_subdirectory(matashift)
add_subdirectory(pokemonbrowser)
