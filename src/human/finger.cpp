#include <human/finger.h>

#include <core/device.h>
#include <human/systemcall.hpp>
#include "matapath/ga_tracker.hpp"

#include <algorithm>
#include <vector>
#include <string>
#include <cstdio> // debug
#include <sstream>

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef __APPLE__
#include "android/log.h"
#include <linux/input.h>
struct input_event event;

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)

#else
#define LOGD(...) ;
#endif

namespace {

class InputDeviceManager
{
public:
	virtual int max_x() const=0;
	virtual int max_y() const=0;
	virtual std::string device_path() const=0;
};
	
class TextInputDeviceManager : public InputDeviceManager
{
public:
	TextInputDeviceManager(){}
	
public:
	virtual int max_x() const { return _max_x; }
	virtual int max_y() const { return _max_y; }
	virtual std::string device_path() const { return _device_path; }
	
	void test(const std::string &msg)
	{
		std::string name;
		std::string event_path;
		int min_x(0);
		int min_y(0);
		std::size_t max_x(0);
		std::size_t max_y(0);
		
		std::size_t true_max_x(0);
		std::size_t true_max_y(0);
		std::string true_path;
		
		bool hasKEY001 = false;
		bool isABS = false;
		std::vector<std::string> lines = split(msg, '\n');
		for(std::size_t i = 0; i < lines.size(); i++)
		{
			std::string line = trim(lines[i]);
			int p = 0;
			if ((p = line.find("/dev/input/event")) != std::string::npos)
			{
				event_path = line.substr(p);
				isABS = false;
				hasKEY001 = false;
				max_x = 0;
				max_y = 0;
			}
			if ((p = line.find("KEY")) != std::string::npos)
			{
				if (line.find("001") != std::string::npos)
				{
					hasKEY001 = true;
				}
			}
			if ((p = line.find("ABS")) != std::string::npos)
			{
				isABS = true;
				if ((p = line.find(":")) != std::string::npos)
				{
					line = line.substr(line.find(":") + 1);
					line = trim(line);
				}
			}
			if ((p = line.find("name")) != std::string::npos)
			{
				name = line;
			}
			if (!isABS)
			{
				continue;
			}
			
			std::vector<std::string> fields = split(line, ' ');
			if (fields.size() > 6)
			{
				int key = 0;
				try
				{
					key = strtol(fields[0].c_str(), 0, 16);
				} catch(...) {}
				if (key != 0x35 && key != 0x36)
				{
					continue;
				}
				bool is_min = false;
				bool is_max = false;
				for (auto & v : fields)
				{
					int value = 0;
					if (is_min)
					{
						try
						{
							value = strtol(v.c_str(), 0, 10);
						} catch(...) {}
						if (key == 0x35)
						{
							min_x = value;
						}
						else if (key == 0x36)
						{
							min_y = value;
						}
						is_min = false;
					}
					if (is_max)
					{
						try
						{
							value = strtol(v.c_str(), 0, 10);
						} catch(...) {}
						if (key == 0x35)
						{
							max_x = value;
						}
						else if (key == 0x36)
						{
							max_y = value;
						}
						is_max = false;
						break;
					}
					if (v == "min")
					{
						is_min = true;
					}
					if (v == "max")
					{
						is_max = true;
					}
				}
				if (min_x < 0 || min_y < 0)
				{
					max_x = 0;
					max_y = 0;
				}
				if (max_x != 0 && max_y != 0)
				{
					true_max_x = max_x;
					true_max_y = max_y;
					true_path = event_path;
					if (name.find("pen") == std::string::npos && hasKEY001)
					{
						break;
					}
				}
			}
		}
		_max_x = true_max_x;
		_max_y = true_max_y;
		_device_path = true_path;
	}
	
private:
	std::vector<std::string> split(const std::string &s, char d)
	{
		std::stringstream ss(s);
		std::string item;
		std::vector<std::string> rs;
		while (std::getline(ss, item, d)) {
			rs.push_back(item);
		}
		return rs;
	}
	
	std::string &ltrim(std::string &s) {
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
		return s;
	}
	
	std::string &rtrim(std::string &s) {
		s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
		return s;
	}
	
	std::string &trim(std::string &s) {
		return ltrim(rtrim(s));
	}
	
	
private:
	int _max_x = 0;
	int _max_y = 0;
	std::string _device_path;
};


class BinaryInputDeviceManager : public InputDeviceManager
{
public:
	BinaryInputDeviceManager();

public:
	virtual int max_x() const { return _max_x; }
	virtual int max_y() const { return _max_y; }
	virtual std::string device_path() const { return _device_path; }

private:
	std::vector<std::string> scan_dir(const std::string & path);
	int parse_deivce(const std::string & device);

private:
	int _max_x; 
	int _max_y;
	std::string _device_path;
};

BinaryInputDeviceManager::BinaryInputDeviceManager()
	:_device_path("/dev/input/event3"), _max_x(0), _max_y(0)
{
	if (_max_y == 0 || _max_x == 0)
	{
		FILE *fp = popen("su -c \"getevent -p\"", "r");
		if (fp == NULL)
		{
			GATracker::getInstance().sendTrackerEvent("cError", "Finger", "Can not popen getevent -p");
			LOGD("Matapath.device: Can not GE -p");
			fp = popen("su -c \"getevent -i\"", "r");
			if (fp == NULL)
			{
				GATracker::getInstance().sendTrackerEvent("cError", "Finger", "Can not popen getevent -i");
				LOGD("Matapath.device: Can not GE -i");
			}
		}
		if (fp != NULL)
		{
			std::string msg;
			char buf[256];
			while(!feof(fp))
			{
				std::size_t len = fread(buf, 1, 256, fp);
				msg.append(buf, len);
			}
			pclose(fp);
			if (msg.length() == 0)
			{
				GATracker::getInstance().sendTrackerEvent("cError", "Finger", "popen getevent read length = 0");
				LOGD("Matapath.device read msg from GE is empty");
			}
			
			static TextInputDeviceManager text_input_device_manager;
			text_input_device_manager.test(msg);
			_max_x = text_input_device_manager.max_x();
			_max_y = text_input_device_manager.max_y();
			_device_path = text_input_device_manager.device_path();
			LOGD("Matapath.device: Parse text to get x %d, %d, %s", _max_x, _max_y, _device_path.c_str());
		}
	}
	
	if (_max_y == 0 || _max_x == 0)
	{
		GATracker::getInstance().sendTrackerEvent("cError", "Finger", "Can not parse text screen max xy");
		LOGD("Matapath.device: Can not get screen max x and y using parse text");
		
	}
	
	if (_max_y == 0 || _max_x == 0)
	{
		std::vector<std::string> files = scan_dir("/dev/input");
		if (files.size() == 0)
		{
			GATracker::getInstance().sendTrackerEvent("cError", "Finger", "Open dir /dev/input failed");
			LOGD("Matapath.device scandir is empty");
		}
		
		for (size_t i = 0; i < files.size(); i++)
		{
			if (files[i].find("event") != std::string::npos)
			{
				parse_deivce(files[i]);
				if (_max_x > 0 && _max_y > 0)
				{
					_device_path = files[i];
					break;
				}
				else
				{
					_max_x = 0;
					_max_y = 0;
				}
			}
		}
	}
	
	if (_max_y == 0 || _max_x == 0)
	{
		GATracker::getInstance().sendTrackerEvent("cError", "Finger", "Can not open screen max xy");
		LOGD("Matapath.device: Can not get screen max x and y using open");
	}
	else
	{
		GATracker::getInstance().sendTrackerEvent("cError", "Finger", "Screen max xy success");
		LOGD("Matapath.device: Screen max xy success");
	}
}
	
std::vector<std::string> BinaryInputDeviceManager::scan_dir(const std::string & path)
{
	std::vector<std::string> file_list;
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir (path.c_str())) != NULL)
	{
		while ((ent = readdir (dir)) != NULL)
		{
			std::string filename(ent->d_name);
			if (filename != "." && filename != "..")
			{
				file_list.push_back(path + "/" + filename);
			}
		}
		closedir (dir);
	}
	return file_list;
}

int BinaryInputDeviceManager::parse_deivce(const std::string & device)
{
#ifndef __APPLE__
	uint8_t *bits = NULL;
	ssize_t bits_size = 0;
	int res;
	bool hasKEY = false;

	int fd = open(device.c_str(), O_RDWR);
	
	if(fd < 0)
	{
		GATracker::getInstance().sendTrackerEvent("cError", "Finger", "Can not open device fd");
		LOGD("Matapath.device: Can not open device fd: %d name: %s", errno, device.c_str());
		return -1;
	}
	for(int i = 0; i <= EV_MAX; i++) {
		
		while(1) {
			res = ioctl(fd, EVIOCGBIT(i, bits_size), bits);
			if(res < bits_size)
				break;
			bits_size = res + 16;
			bits = (uint8_t *)realloc(bits, bits_size * 2);
			if(bits == NULL) {
				return 1;
			}
		}
		if(i == EV_KEY)
		{
			hasKEY = true;
		}
		if(i != EV_ABS || !hasKEY) {
			continue;
		}
		for(int j = 0; j < res; j++) {
			for(int k = 0; k < 8; k++) {
				if(bits[j] & 1 << k) {
					struct input_absinfo abs;
					if(ioctl(fd, EVIOCGABS(j * 8 + k), &abs) == 0) {
						int abs_event_number = j * 8 + k;
						if (abs_event_number == 0x35)
						{
							_max_x = abs.maximum;
						}
						else if (abs_event_number == 0x36)
						{
							_max_y = abs.maximum;
						}
					}
				}
			}
		}
	}
	free(bits);
	close(fd);
#endif
}
	
struct FingerHelper
{
	std::size_t screen_width_in_pixel()
	{
		return _screen_width_in_pixel; // 1080; // FIXME
	}
	
	std::size_t screen_height_in_pixel()
	{
		return _screen_height_in_pixel; //1920; // FIXME
	}
	
	std::size_t toucharea_width_in_pixel()
	{
		return _toucharea_width_in_pixel;
	}
	
	std::size_t toucharea_height_in_pixel()
	{
		return _toucharea_height_in_pixel;
	}
	
	std::size_t screen_x(float pixel)
	{
		return static_cast<std::size_t>(pixel * toucharea_width_in_pixel() / screen_width_in_pixel());
	}
	
	std::size_t screen_y(float pixel)
	{
		return static_cast<std::size_t>(pixel * toucharea_height_in_pixel() / screen_height_in_pixel());
	}
	
	void system(const char * cmd)
	{
		SystemCall::execute(cmd);
	}
	
	bool sendevent2(uint32_t cmd, uint32_t type, std::size_t value)
	{
#ifndef __APPLE__
		if (_device_fd > 0)
		{
			memset(&event, 0, sizeof(event));
			event.type = cmd;
			event.code = type;
			event.value = value;
			int ret = write(_device_fd, &event, sizeof(event));
			if(ret < (ssize_t) sizeof(event))
			{
				return false;
			}
			return true;
		}
#endif
		return false;
	}
	
	void sendevent(uint32_t cmd, uint32_t type, std::size_t value)
	{
		if(sendevent2(cmd, type, value))
		{
			return;
		}
		char event[256];
		std::sprintf(event, "sendevent %s %u %u %u\n", _device_path.c_str(), cmd, type, value);
		system(event);
	}
	
	void refresh() {
		if (_device_fd <= 0)
		{
			int s = SystemCall::refresh();
		}
	}

	void init()
	{
		// input event
		_device_fd = open(_device_path.c_str(), O_WRONLY);
		if(_device_fd < 0) {
			close(_device_fd);
			LOGD("Matapath.device: Can not open input event %d", errno);
		} else {
			LOGD("Matapath.device: Open input event success %d", _device_fd);
		}
	}
	
	std::size_t _screen_width_in_pixel;
	std::size_t _screen_height_in_pixel;
	std::size_t _toucharea_width_in_pixel;
	std::size_t _toucharea_height_in_pixel;
	std::string _device_path;
	int _device_fd;
	
};

} // namespace

struct Finger::Pimpl
{
public:
	virtual void press(std::size_t x, std::size_t y)=0;
	virtual void move(std::size_t x, std::size_t y)=0;
	virtual void release()=0;
	virtual void fast_tap(std::size_t x, std::size_t y)=0;
};

namespace
{

struct AndroidMobileStrategy : public Finger::Pimpl
{
public:
	AndroidMobileStrategy();

public:
	virtual void press(std::size_t x, std::size_t y);
	virtual void move(std::size_t x, std::size_t y);
	virtual void release();
	virtual void fast_tap(std::size_t x, std::size_t y);

private:
	FingerHelper finger_helper;
};

AndroidMobileStrategy * android_mobile_strategy;

AndroidMobileStrategy::AndroidMobileStrategy()
{
	finger_helper.system("chmod 666 /dev/input/event*\n");
	finger_helper.refresh();
	static BinaryInputDeviceManager input_device_manager;
	finger_helper._screen_width_in_pixel = SystemCall::screen_width();
	finger_helper._screen_height_in_pixel = SystemCall::screen_height();
	finger_helper._toucharea_width_in_pixel = input_device_manager.max_x();
	finger_helper._toucharea_height_in_pixel = input_device_manager.max_y();
	finger_helper._device_path = input_device_manager.device_path();
	finger_helper.init();
	
	LOGD("Matapath.device: screen w/h %d/%d, toucharea w/h %d/%d",
		 finger_helper._screen_width_in_pixel,
		 finger_helper._screen_height_in_pixel,
		 finger_helper._toucharea_width_in_pixel,
		 finger_helper._toucharea_height_in_pixel
	);
	
	LOGD("Matapath.device: Finger device num: %s, max_x: %d, max_y: %d",
		input_device_manager.device_path().c_str(),
		input_device_manager.max_x(),
		input_device_manager.max_y()
	);
}

void AndroidMobileStrategy::press(std::size_t x, std::size_t y)
{
	finger_helper.sendevent(3, 0x39, 0x24f);
    finger_helper.sendevent(1, 330, 1);
    // note: finger width and pressure
    finger_helper.sendevent(3, 0x30, 0x4d); //ABS_MT_TOUCH_MAJOR
    finger_helper.sendevent(3, 0x32, 0x9);  //ABS_MT_WIDTH_MAJOR
    finger_helper.sendevent(3, 0x3a, 0x9);  //ABS_MT_PRESSURE
    // note: x, y
    finger_helper.sendevent(3, 53, finger_helper.screen_x(x));
    finger_helper.sendevent(3, 54, finger_helper.screen_y(y));
    finger_helper.sendevent(0, 0, 0);
    finger_helper.refresh();
}

void AndroidMobileStrategy::move(std::size_t x, std::size_t y)
{
	finger_helper.sendevent(3, 0x3a, 0xc);
    finger_helper.sendevent(3, 53, finger_helper.screen_x(x));
    finger_helper.sendevent(3, 54, finger_helper.screen_y(y));
    finger_helper.sendevent(0, 0, 0);
}

void AndroidMobileStrategy::release()
{
	finger_helper.sendevent(3, 0x39, 0xffffffff);
	finger_helper.sendevent(1, 330, 0);
    finger_helper.sendevent(0, 0, 0);
    finger_helper.refresh();
}

void AndroidMobileStrategy::fast_tap(std::size_t x, std::size_t y)
{
	finger_helper.sendevent(3, 0x39, 0x24f);
    finger_helper.sendevent(1, 330, 1);
    // note: finger width and pressure
    finger_helper.sendevent(3, 0x30, 0x4d); //ABS_MT_TOUCH_MAJOR
    finger_helper.sendevent(3, 0x32, 0x9);  //ABS_MT_WIDTH_MAJOR
    finger_helper.sendevent(3, 0x3a, 0x9);  //ABS_MT_PRESSURE
    // note: x, y
    finger_helper.sendevent(3, 53, finger_helper.screen_x(x));
    finger_helper.sendevent(3, 54, finger_helper.screen_y(y));
    finger_helper.sendevent(0, 0, 0);
	finger_helper.sendevent(3, 0x39, 0xffffffff);
	finger_helper.sendevent(1, 330, 0);
    finger_helper.sendevent(0, 0, 0);
    finger_helper.refresh();
}

struct AndroidBs2Strategy : public Finger::Pimpl
{
public:
	AndroidBs2Strategy();

public:
	virtual void press(std::size_t x, std::size_t y);
	virtual void move(std::size_t x, std::size_t y);
	virtual void release();
	virtual void fast_tap(std::size_t x, std::size_t y);

private:
	std::size_t adjust_x(std::size_t user_y)
	{
		return finger_helper._screen_width_in_pixel - user_y;
	}

	std::size_t adjust_y(std::size_t user_x)
	{
		return user_x;
	}

private:
	FingerHelper finger_helper;
};

AndroidBs2Strategy * android_bs2_strategy;

AndroidBs2Strategy::AndroidBs2Strategy()
{
	finger_helper._screen_width_in_pixel = SystemCall::screen_width();
	finger_helper._screen_height_in_pixel = SystemCall::screen_height();
	finger_helper._toucharea_width_in_pixel = 32767;
	finger_helper._toucharea_height_in_pixel = 32767;
	finger_helper._device_path = "/dev/input/event2";
	finger_helper.init();
}

void AndroidBs2Strategy::press(std::size_t x, std::size_t y)
{
	std::size_t nx = adjust_x(y);
	std::size_t ny = adjust_y(x);
	
	finger_helper.sendevent(3, 0x35, finger_helper.screen_x(nx));
	finger_helper.sendevent(3, 0x36, finger_helper.screen_y(ny));
	finger_helper.sendevent(0, 2, 0);
	finger_helper.sendevent(0, 0, 0);
	finger_helper.refresh();
}

void AndroidBs2Strategy::move(std::size_t x, std::size_t y)
{
	std::size_t nx = adjust_x(y);
	std::size_t ny = adjust_y(x);

	finger_helper.sendevent(3, 0x35, finger_helper.screen_x(nx));
	finger_helper.sendevent(3, 0x36, finger_helper.screen_y(ny));
	finger_helper.sendevent(0, 2, 0);
	finger_helper.sendevent(0, 0, 0);
}

void AndroidBs2Strategy::release()
{
	finger_helper.sendevent(0, 2, 0);
	finger_helper.sendevent(0, 0, 0);
	finger_helper.refresh();
}

void AndroidBs2Strategy::fast_tap(std::size_t x, std::size_t y)
{
	std::size_t nx = adjust_x(y);
	std::size_t ny = adjust_y(x);
	
	finger_helper.sendevent(3, 0x35, finger_helper.screen_x(nx));
	finger_helper.sendevent(3, 0x36, finger_helper.screen_y(ny));
	finger_helper.sendevent(0, 2, 0);
	finger_helper.sendevent(0, 0, 0);
	finger_helper.refresh();

	finger_helper.sendevent(0, 2, 0);
	finger_helper.sendevent(0, 0, 0);
	finger_helper.refresh();
}

	
struct AndroidBs1Strategy : public Finger::Pimpl
{
public:
	AndroidBs1Strategy();
	
public:
	virtual void press(std::size_t x, std::size_t y);
	virtual void move(std::size_t x, std::size_t y);
	virtual void release();
	virtual void fast_tap(std::size_t x, std::size_t y);
	
private:
	std::size_t adjust_x(std::size_t user_y)
	{
		return finger_helper._screen_width_in_pixel - user_y;
	}
	
	std::size_t adjust_y(std::size_t user_x)
	{
		return user_x;
	}

private:
	FingerHelper finger_helper;
};

AndroidBs1Strategy * android_bs1_strategy;

AndroidBs1Strategy::AndroidBs1Strategy()
{
	finger_helper._screen_width_in_pixel = SystemCall::screen_width();
	finger_helper._screen_height_in_pixel = SystemCall::screen_height();
	finger_helper._toucharea_width_in_pixel = 32767;
	finger_helper._toucharea_height_in_pixel = 32767;
	finger_helper._device_path = "/dev/input/event6";
	finger_helper.init();
}

void AndroidBs1Strategy::press(std::size_t x, std::size_t y)
{
	std::size_t nx = adjust_x(y);
	std::size_t ny = adjust_y(x);
	
	// note: finger width and pressure
	// note: x, y
	finger_helper.sendevent(3, 0, finger_helper.screen_x(nx));
	finger_helper.sendevent(3, 1, finger_helper.screen_y(ny));
	finger_helper.sendevent(1, 0x110, 1);
	finger_helper.sendevent(0, 0, 0);
	finger_helper.refresh();
}

void AndroidBs1Strategy::move(std::size_t x, std::size_t y)
{
	std::size_t nx = adjust_x(y);
	std::size_t ny = adjust_y(x);
	
	finger_helper.sendevent(3, 0, finger_helper.screen_x(nx));
	finger_helper.sendevent(3, 1, finger_helper.screen_y(ny));
	finger_helper.sendevent(0, 0, 0);
}

void AndroidBs1Strategy::release()
{
	finger_helper.sendevent(1, 0x110, 0);
	finger_helper.sendevent(0, 0, 0);
	finger_helper.refresh();
}

void AndroidBs1Strategy::fast_tap(std::size_t x, std::size_t y)
{
	std::size_t nx = adjust_x(y);
	std::size_t ny = adjust_y(x);
	
	// note: finger width and pressure
	// note: x, y
	finger_helper.sendevent(3, 0, finger_helper.screen_x(nx));
	finger_helper.sendevent(3, 1, finger_helper.screen_y(ny));
	finger_helper.sendevent(1, 0x110, 1);
	finger_helper.sendevent(0, 0, 0);
	finger_helper.refresh();

	finger_helper.sendevent(1, 0x110, 0);
	finger_helper.sendevent(0, 0, 0);
	finger_helper.refresh();
}

} // end of namespace


// FIXME
Finger::Finger()
{
	android_mobile_strategy = new AndroidMobileStrategy();
	_pimpl = android_mobile_strategy;
	android_bs1_strategy = new AndroidBs1Strategy();
	android_bs2_strategy = new AndroidBs2Strategy();
}

void Finger::press(std::size_t x, std::size_t y)
{
	_pimpl->press(x, y);
}

void Finger::move(std::size_t x, std::size_t y)
{
	_pimpl->move(x, y);
}

void Finger::release()
{
	_pimpl->release();
}

void Finger::fast_tap(std::size_t x, std::size_t y)
{
	_pimpl->fast_tap(x, y);
}

Finger & Finger::instance()
{
	static Finger finger;

	if (Device::instance().platform == Device::AndroidBluestacks1)
	{
		finger._pimpl = android_bs1_strategy;
	}
	else if (Device::instance().platform == Device::AndroidBluestacks2)
	{
		finger._pimpl = android_bs2_strategy;
	}
	else
	{
		finger._pimpl = android_mobile_strategy;
	}

	return finger;
}
