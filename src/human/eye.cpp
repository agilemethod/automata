#include "human/eye.h"
#include "matapath/ga_tracker.hpp"
#include <core/device.h>
#include <human/systemcall.hpp>
#include <unistd.h>
#if !defined(__APPLE__)
#include "android/log.h"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)
#else
#define LOGD(...) ;
#endif


struct Eye::Pimpl
{
public:
	virtual Image take()=0;
};

#if defined(__APPLE__)

static struct MockStrategy : public Eye::Pimpl
{
	virtual Image take()
	{
		return Image();
	}
} mock_strategy;

Eye::Eye()
	: _pimpl(&mock_strategy)
{
}

Image Eye::take()
{
	return _pimpl->take();
}

Eye & Eye::instance()
{
	static Eye eye;
	return eye;
}

#else

namespace {

void system(const char * cmd)
{
	SystemCall::execute(cmd);
	int s = SystemCall::refresh();
	LOGD("Refresh1 status %d", s);
}

void snapshot_in_android()
{
}

}


namespace
{

static struct AndroidMobileStrategy : public Eye::Pimpl
{
	virtual Image take()
	{
		return SystemCall::screenshot();
	}
} android_mobile_strategy;

struct AndroidBs2Strategy : public Eye::Pimpl
{
	virtual Image take()
	{
		Image result = SystemCall::screenshot();
		result = result.flip(0);
		return result;
	}
} android_bs2_strategy;

}


Eye::Eye()
	: _pimpl(&android_mobile_strategy)
{
	Image result = SystemCall::screenshot();
	if (result.width() == 0 || result.height() == 0)
	{
		GATracker::getInstance().sendTrackerEvent("cError", "Eye", "Screenshot failure");
		LOGD("Matapath.device: Can not get screenshot");
	}
	else
	{
		GATracker::getInstance().sendTrackerEvent("cError", "Eye", "Screenshot success");
		LOGD("Matapath.device: Get screenshot success");
	}
}

Image Eye::take()
{
	return _pimpl->take();
}

Eye & Eye::instance()
{
	static Eye eye;

	if (Device::instance().platform == Device::AndroidMobile)
	{
		eye._pimpl = &android_mobile_strategy;
	}
	else
	{
		eye._pimpl = &android_bs2_strategy;
	}

	return eye;
}

#endif
