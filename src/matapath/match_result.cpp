#include "matapath/match_result.h"
#include "matapath/rune.h"
#include "matapath/basic_types.h"

namespace {
std::ostream & operator<<(std::ostream & os, const Board & board)
{
	const size_t total = board.rows * board.cols;
	os << "[ ";
	for (size_t r = 0; r < board.rows; r++)
	{
		if (r != 0) os << ", ";
		for (size_t c = 0; c < board.cols; c++)
		{
			if (c == 0) os << (int)board.data[r][c].rune.type;
			else os << ", " << (int)board.data[r][c].rune.type;
		}
	}
	os << " ]";
	return os;
}
}

MatchResult::MatchResult(const std::vector<Match> & matches, const Board & board)
{
	combo_count = matches.size();
	for (size_t i = 0; i < matches.size(); i++)
	{
		const Match & match = matches[i];
		if (match.round == 1)
		{
			first_combo_count++;

			rune_results[match.type].has_first_round = true;

			lowest_rune_count += match.row_count[4];
			
			for (size_t c = 0; c < 6; c++)
			{
				if (match.col_count[c] > column_rune_count[c])
					column_rune_count[c] = match.col_count[c];
			}
		}
		else
		{
			rune_results[match.type].has_second_round = true;
		}
		
		rune_results[match.type].max_link_count = std::max(
			rune_results[match.type].max_link_count,
			match.count);

		for (size_t r = 0; r < 5; r++)
		{
			if (match.row_count[r] == 6)
			{
				rune_results[match.type].has_full_row = true;
			}
		}

		rune_results[match.type].count += match.count;
		rune_results[match.type].times++;
		rune_results[match.type].enhanced_count += match.enhanced_count;
	}
	
	for (int r = 0; r < 5; r++)
	{
		for (int c = 0; c < 6; c++)
		{
			rune_results[board.data[r][c].rune.type].column_count[c]++;

			if (board.data[r][c].has_puzzle)
			{
				rune_results[board.data[r][c].rune.type].puzzle_count++;
			}
			if (board.data[r][c].rune.matched) {
				if (board.data[r][c].rune.priority == High)
				{
					high_match_count++;
				}
				else if (board.data[r][c].rune.priority == Low)
				{
					low_match_count++;
				}
			}
		}
	}
	
	total_puzzle_count = board.total_puzzle_count();
	for (size_t i = 1; i < TosRune::RuneTypeEnd; i++)
	{
		rune_results[i].total_count = board.total_count(i);
	}
    
	if ((board.data[4][0].rune.type == board.data[4][1].rune.type) || (board.data[4][4].rune.type == board.data[4][5].rune.type)) {
		if (rune_results[board.data[4][0].rune.type].total_count > 2)
			lowest_rune_bonus += 1;
	}
	if ((board.data[4][1].rune.type == board.data[4][2].rune.type) || (board.data[4][3].rune.type == board.data[4][4].rune.type)) {
		if (rune_results[board.data[4][0].rune.type].total_count > 2)
			lowest_rune_bonus += 0.5;
	}
};

float MatchResult::calculate_score()
{
	float result(0);

	const float combo_bouns = 1.25;
	float basic_score = 0;
	for (size_t i = 1; i < TosRune::RuneTypeEnd; i++)
	{
		const RuneResult & rune_result = rune_results[i];
		float basic_score_foreach = (rune_result.count / 3.0f) * combo_bouns;
		//float basic_score_foreach = rune_result.count * rune_result.times * 0.25 + (rune_result.enhanced_count * 0.15);
		basic_score += basic_score_foreach;
	}
	basic_score += combo_count * combo_bouns;
	//basic_score *= (1 + (combo_count - 1) * 0.25);
	
	uint8_t max_puzzle_count = 0;
	uint8_t perfer_puzzle_rune = 0;
	typedef decltype(GamingConfig::inst().rune_round_type) rune_round_type_t;
	const rune_round_type_t & rune_round_type = GamingConfig::inst().rune_round_type;
	
	typedef decltype(GamingConfig::inst().rune_attack_type) rune_attack_type_t;
	const rune_attack_type_t & rune_attack_type = GamingConfig::inst().rune_attack_type;
	
	typedef decltype(GamingConfig::inst().column_attack_type) column_attack_type_t;
	const column_attack_type_t & column_attack_type = GamingConfig::inst().column_attack_type;

	typedef decltype(GamingConfig::inst().rune_priority) rune_priority_t;
	const rune_priority_t & rune_priority = GamingConfig::inst().rune_priority;


	
	for (size_t i = 1; i < TosRune::RuneTypeEnd; i++)
	{
		const RuneResult & rune_result = rune_results[i];
		
        if (max_puzzle_count < rune_result.puzzle_count)
        {
            perfer_puzzle_rune = i;
            max_puzzle_count = rune_result.puzzle_count;
        }
        else if (max_puzzle_count == rune_result.puzzle_count)
        {
            if (rune_results[perfer_puzzle_rune].total_count < rune_result.total_count)
            {
                perfer_puzzle_rune = i;
                max_puzzle_count = rune_result.puzzle_count;
            }
        }
        
		if (rune_result.total_count == 0) continue;

		if (rune_round_type[i] == First && rune_results[i].has_first_round)
		{
			basic_score += 0.75 * rune_result.count * combo_bouns;
		}
		else if (rune_round_type[i] == First && rune_results[i].has_second_round)
		{
			basic_score -= 5 * combo_bouns;
		}
		else if (rune_round_type[i] == Second && rune_results[i].has_second_round)
		{
			basic_score += 0.75 * rune_result.count * combo_bouns;
		}
		else if (rune_round_type[i] == Second && rune_results[i].has_first_round)
		{
			basic_score -= 5 * combo_bouns;
		}

		if (rune_attack_type[i] == Single)
		{
			if (rune_result.max_link_count >= 5)
			{
				basic_score -= 5 * combo_bouns;
			}
		}
		else if (rune_attack_type[i] == Multiple)
		{
			if (rune_result.max_link_count >= 5)
			{
				basic_score += 10 * combo_bouns;
			}
			else if (rune_result.max_link_count == 4)
			{
				basic_score += 5 * combo_bouns;
			}
		}
		else if (rune_attack_type[i] == Entire)
		{
			basic_score += (rune_result.count / rune_result.total_count) * 6 * combo_bouns;
		}
		else if (rune_attack_type[i] == FullRow)
		{
#if 0 
			if (rune_result.has_full_row)
			{
				basic_score * attack_type_rate;
			}
#endif
		}
	}
	
	for (size_t c = 0; c < 6; c++)
	{
		// Babylon
		if (column_attack_type[c] == 4)
		{
			if (column_rune_count[c] == 3) basic_score += 2 * combo_bouns;
			if (column_rune_count[c] == 4) basic_score += 7 * combo_bouns;
		}
		else if (column_attack_type[c] == 5)
		{
			if (column_rune_count[c] == 3) basic_score += 2 * combo_bouns;
			if (column_rune_count[c] == 4) basic_score += 7 * combo_bouns;
			if (column_rune_count[c] == 5) basic_score += 12 * combo_bouns;
		}
	}

	if (GamingConfig::inst().has_lowest_row)
	{
		if (lowest_rune_count == 3)
		{
			basic_score += 1 * combo_bouns;
		}
		else if (lowest_rune_count == 4)
		{
			basic_score += 2 * combo_bouns;
		}
		else if (lowest_rune_count == 5)
		{
			basic_score += 6 * combo_bouns;
		}
		else if (lowest_rune_count == 6)
		{
			basic_score += 12 * combo_bouns;
		}
		basic_score += 10 * lowest_rune_bonus * combo_bouns;
	}


	// for group rune
	for (size_t i = 1; i < TosRune::RuneTypeEnd; i++)
	{
		const RuneResult & rune_result = rune_results[i];
		if (rune_result.total_count == 0) continue;

		if (rune_priority[i] == Low)
		{
			if (rune_result.count > 0)
				basic_score -= 2.5 * rune_result.count * combo_bouns;
		}
		else if (rune_priority[i] == High)
		{
			if (rune_result.count > 0)
				basic_score += 1 * rune_result.count * combo_bouns;
		}
	}

	// for single rune
	// rune priority low/high
	basic_score += high_match_count * 8 * combo_bouns;
	basic_score -= low_match_count * 6 * combo_bouns;
	
	// puzzle shield
	if (GamingConfig::inst().puzzle_count > 0)
	{
		const RuneResult & rune_result = rune_results[perfer_puzzle_rune];
		
		if (rune_result.puzzle_count < total_puzzle_count)
		{
			if (rune_result.puzzle_count == 3)
			{
				basic_score += 1 * combo_bouns;
			}
			else if (rune_result.puzzle_count == 4)
			{
				basic_score += 6 * combo_bouns;
			}
			else if (rune_result.puzzle_count == 5) // for puzzle count == 5
			{
				basic_score += 12 * combo_bouns;
			}
			else if (rune_result.puzzle_count == 6) // for puzzle count == 6
			{
				basic_score += 18 * combo_bouns;
			}
			else if (rune_result.puzzle_count == 7) // for puzzle count == 7
			{
				basic_score += 24 * combo_bouns;
			}
		}
		if (rune_result.puzzle_count == total_puzzle_count)
		{
			basic_score += 12 * combo_bouns;
		}
//		else if (rune_result.puzzle_count > total_puzzle_count)
//		{
//			basic_score -= 2 * combo_bouns;
//		}
	}
	
	
	// first combo count
	if (GamingConfig::inst().first_combo_count > 0)
	{
		basic_score += (first_combo_count / GamingConfig::inst().first_combo_count) * 5 * combo_bouns;
		if (first_combo_count > GamingConfig::inst().first_combo_count)
		{
			basic_score = 0;
		}
	}
	
	// combo count
	if (GamingConfig::inst().combo_count > 0 && combo_count > GamingConfig::inst().combo_count)
	{
		basic_score = 0;
	}
	
	result = basic_score;
	return result;
};
