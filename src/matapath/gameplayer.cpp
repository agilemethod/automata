#include "matapath/gameplayer.h"

#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <random>
#include <sstream>
#include <opencv2/opencv.hpp>
#include <thirdparty/json.hpp>
#include <core/image.h>
#include <core/image_p.h>
#include <core/matcher.h>
#include <core/gesture.h>
#include <core/device.h>
#include <human/eye.h>
#include <human/finger.h>
#include <human/systemcall.hpp>
#include <matapath/rune.h>
#include <matapath/rune_provider.h>
#include <matapath/layout.h>
#include <matapath/solver.h>
#include <matapath/rune_sample_basic.h>
#include <matapath/rune_sample_advance.h>
#include <matapath/tos_database.h>


#include <unistd.h> // FIXME: remove

#if !defined(__APPLE__) && !defined(NDEBUG)
#include "android/log.h"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)
#else
#define LOGD(...) ;
#endif

using json = nlohmann::json;

namespace 
{
//helpers

Image create_screenshot()
{
#ifdef __APPLE__
    return Image("tos_android.png");
#else
    return Eye::instance().take();
#endif
}

void weights_from_requirement(Weights & weights, const Requirement & requirement)
{
	for (auto & rune_requirement : requirement.rune_requirements)
	{
		if (rune_requirement.config == RuneRequirement::Link3)
		{
			weights[rune_requirement.type].value[Normal] = 3;
			weights[rune_requirement.type].value[Mass] = 1;
		}
		else
		{
			weights[rune_requirement.type].value[Normal] = 1;
			weights[rune_requirement.type].value[Mass] = 5;
		}
	}
}

// note: dir_from_point
uint8_t dir_from_point(const Point<std::size_t> & from, const Point<std::size_t> & to)
{
    uint8_t dir;
    std::size_t dx = to.x - from.x;
    std::size_t dy = to.y - from.y;

    if (dx == 0 && dy == 1)
    {
        dir = RR;
    }
    else if (dx == -1 && dy == 1)
    {
        dir = RU;
    }
    else if (dx == 1 && dy == 1)
    {
        dir = RB;
    }
    else if (dx == 0 && dy == -1)
    {
        dir = LL;
    }
    else if (dx == -1 && dy == -1)
    {
        dir = LU;
    }
    else if (dx == 1 && dy == -1)
    {
        dir = LB;
    }
    else if (dx == -1 && dy == 0)
    {
        dir = UU;
    }
    else if (dx == 1 && dy == 0)
    {
        dir = BB;
    }

    return dir;
}

} // namespace

namespace next
{
	
struct TosRuneMatcher
{
	TosRuneMatcher()
		: basic_rune_provider(RuneSampleArchive::BASIC)
		, advance_rune_provider(RuneSampleArchive::ADVANCE)
	{
		matcher.set_match_method(MatchMethod::CcoeffNormed());
		
		for (size_t i = 0; i < basic_rune_provider.samples.size(); i++)
		{
			RuneSample rs = basic_rune_provider.samples[i];
			rs.threshold = 0.8f;
			advance_rune_provider.samples.push_back(rs);
		}
	}
	
	Rune find_best(const Image & target_rune, const RuneProvider & rune_provider)
	{
		Rune result;
		
		float best_value = -1.0f;
		int best_index = -1;
		for (int i = 0; i < rune_provider.samples.size(); i++)
		{
			auto match_result = matcher.match(target_rune, rune_provider.samples[i].image);
			
			float value = match_result.max();
			if (best_value < value)
			{
				best_value = value;
				best_index = i;
			}
		}
		
		if (best_value > rune_provider.samples[best_index].threshold)
		{
			result.type = rune_provider.samples[best_index].type;
			result.property = rune_provider.samples[best_index].prop;
		}
		
		return result;
	}
	
	Rune operator()(const Image & target_rune)
	{
		Rune result;
		static int i=0;
		//target_rune.save(std::to_string(i++) + ".jpg");
		result = find_best(target_rune, basic_rune_provider);
		if (result.type == TosRune::RuneType::RuneNotFound)
		{
			result = find_best(target_rune, advance_rune_provider);
			if (result.type == TosRune::RuneType::RuneNotFound)
			{
				result.type = TosRune::RuneType::RuneHidden;
				//target_rune.save("unknown.jpg");
			}
		}
		
		return result;
	}
	
	
	Matcher matcher;
	RuneProvider basic_rune_provider;
	RuneProvider advance_rune_provider;
};
	
class PintooFinder
{
public:
	virtual std::vector<Point<uint8_t>> find(const Image & board, const LayoutManager & layout_manager)=0;
};

class TosPintooFinder : public PintooFinder
{
public:
	virtual std::vector<Point<uint8_t>> find(const Image & board, const LayoutManager & layout_manager)
	{
		std::vector<Point<uint8_t>> result;
		
		// horizontal-borders
		for (size_t j = 0; j < layout_manager.n_cols(); j++)
		{
			bool is_open = false;
			for (size_t i = 0; i < layout_manager.n_rows(); i++)
			{
				auto border = layout_manager.hborder(i, j);
				Image img_border = board.crop(border.x, border.y, border.width, border.height);
				//std::stringstream ss;
				//ss << "hborder_" << i << "_" << j << ".png";
				//img_border.save(ss.str());
				
				float value = gray_avg(img_border._pimpl->mat);
				//printf("%d %d %f\n", i, j, value);
				if (value > 80.0f)
				{
					is_open = !is_open;
				}
				
				if (is_open)
				{
					result.push_back(Point<uint8_t>(i, j));
				}
			}
		}
		if (result.size() < 5 || result.size() > 7)
		{
			result.clear();
		}
		
		if (!result.empty())
		{
			if (!all_chained(result))
			{
				result.clear();
			}
		}
		
		return result;
	}
	
private:
	bool all_chained(const std::vector<Point<uint8_t>> & pintoos)
	{
		size_t j;
		for (size_t i = 0; i < pintoos.size(); i++)
		{
			for (j = 0; j < pintoos.size(); j++)
			{
				if (i == j) continue;
				if (near(pintoos[i], pintoos[j]))
				{
					break;
				}
			}
			if (j == pintoos.size())
			{
				return false;
			}
		}
		return true;
	}
	
	template <class P>
	inline bool near(const P & a, const P & b)
	{
		int dx = a.x - b.x;
		int dy = a.y - b.y;
		return (dx * dx + dy * dy == 1);
	}
	
private:
	float gray_avg(cv::Mat image)
	{
		cv::cvtColor(image, image, CV_BGR2GRAY);
		cv::Scalar avg = cv::mean(image);
		return static_cast<float>(avg[0]);
	}
};
	
// get adid and decode
std::string decode(std::string s, int len)
{
    std::array<int, 20> ns{20, 2, 25, 8, 41, 59, 4, 21, 60, 11, 43, 15, 38, 47, 26, 62, 33, 39, 51, 17};
    std::string result = "";
    for (int i = 0; i < len; i++)
    {
        result += s[ns[i]];
    }
    return result;
}
    
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
    std::string getKeys()
    {
        struct timeval timeout;
        timeout.tv_sec = 3;
        timeout.tv_usec = 0;
        
        const char *domain = "elggum.com";
        const char *path = "automata/due93hdncksu3h4nfidh3jd9cp";
        int sock;
        char send_data[1024], recv_data[2048];
        struct sockaddr_in server_addr;
        struct hostent *he;
        
        if ((he = gethostbyname(domain)) == NULL) return "";
        if ((sock = socket(AF_INET, SOCK_STREAM, 0)) ==  -1) return "";
        setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
        setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout));
        server_addr.sin_family = AF_INET;
        server_addr.sin_port = htons(80);
        server_addr.sin_addr = *((struct in_addr *)he->h_addr);
        bzero(&(server_addr.sin_zero), 8);
        
        if (connect(sock, (struct sockaddr *)&server_addr,sizeof(struct sockaddr)) == -1) return "";
        
        snprintf(send_data, sizeof(send_data), "GET /%s HTTP/1.1\r\nHost: %s\r\n\r\n", path, domain);
        send(sock, send_data, strlen(send_data), 0);
        
        long length = recv(sock, recv_data, 2048, 0);
        close(sock);
        
        if (length <= 20) return "";
        std::string r(recv_data, length);
        long p = r.rfind("\r\n");
        return r.substr(p + 2, r.size() - p - 2);
    }
	
} // namespace

GamePlayer::GamePlayer()
{
	srand(time(0));
    std::string keys = next::getKeys();
    if (keys == "" || keys.find("iloveit") == std::string::npos)
    {
        keys = R"({"adid":[["9973734350037712446687960663502611005640865546903571560546828209","4019551646344389871518021149360771879961761349359604936032135150","4543236377769266894926157788518976531108940508687299507323194033","8371580662460268479076319606609648818074412458495494245417098182"],["2850025457008515132377705923107502506243243216729967959651983833","2068571776952882061946584887611844894988765175571665935314054353","0316626181587081977889349078994928771324455986435177603755818288","0273700277773160530333390156535574154194863400473629050285930618"]],"check_string":"iloveit"})";
    }
    else
    {
        LOGD("Refresh ids success");
    }
    json json_keys = json::parse(keys)["adid"];
    for (int i = 0; i < json_keys.size(); i++)
    {
        if (json_keys[i].size() > 2)
        {
            std::string k1 = next::decode(json_keys[i][2], 16);
            std::string k2 = next::decode(json_keys[i][1], 10);
            ids.push_back(k1 + "/" + k2);
        }
    }
}

Board GamePlayer::load_board(const std::string & filepath)
{
	Board result;

	LayoutManager layout_manager(std::make_shared<TosGameLayout>());
	layout_manager.set_rows_count(5);
	layout_manager.set_cols_count(6);

	Image img;

	if (filepath.empty())
	{
		LOGD("Matapath load_board has no filepath");
		img = create_screenshot();
		if (img.width () <= 0 || img.height() <= 0)
		{
			LOGD("img.width () <= 0 || img.height() <= 0");
			return result;
		}
		LOGD("Image Ok");
	}
	else
	{
		LOGD("Matapath load_board has filepath %s", filepath.c_str());
		img = Image(filepath);
		if (img.width () <= 0 || img.height() <= 0)
		{
			LOGD("img.width () <= 0 || img.height() <= 0");
			return result;
		}

#ifdef __APPLE__
		if (debug_is_android_image)
		{
			img = img.crop(0, 0, debug_android_width, debug_android_height);
		}

#endif
		
	}
#ifndef __APPLE__
	img = img.crop(0, 0, GamingConfig::inst().width, GamingConfig::inst().height);
#endif
	layout_manager.set_resolution(img.width(), img.height());
	
	auto board_rect = layout_manager.board();
	Image img_board = img.crop(board_rect.x, board_rect.y, board_rect.width, board_rect.height);
	img_board.resize(layout_manager.n_cols() * 32, layout_manager.n_rows() * 32);
	layout_manager.reset_board_size(layout_manager.n_cols() * 32, layout_manager.n_rows() * 32);
	
	static next::TosPintooFinder pintoo_finder;
	std::vector<Point<uint8_t>> pintoos = pintoo_finder.find(img_board, layout_manager);
	
	for (auto & pintoo : pintoos)
	{
		result.data[pintoo.x][pintoo.y].has_puzzle = true;
	}
	
	static next::TosRuneMatcher rune_matcher;
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		for (size_t j = 0; j < layout_manager.n_cols(); j++)
		{
			auto img_cell = img_board.crop(j * 32, i * 32, 32, 32);
			Rune rune = rune_matcher(img_cell);
			if (rune.property == TosRune::PropEnhanced && GamingConfig::inst().keep_enhance[rune.type])
			{
				result.data[i][j].rune.priority = Low;
			}
			
			TosRune::update(result.data[i][j].rune, rune.type, rune.property);
		}
	}

	return result;
}

template <class Type>
uint32_t remap_rune(Type our_type)
{
	// our order
	//                        水 火 木 光 暗 心
	// their order
	uint32_t rune_map[] = {6, 1, 0, 2, 3, 4, 5};

	return rune_map[our_type];
}

void show_board_log(const Board & board)
{
	std::stringstream result;
	for (size_t i = 0; i < ROWS; i++)
	{
		for (size_t j = 0; j < COLS; j++)
		{
			result << remap_rune(board.data[i][j].rune.type);
		}
	}
	LOGD("board: %s\n", result.str().c_str());
}

PathMap GamePlayer::calculate_path(Board board, const std::vector<Point<std::size_t>> & paths, std::vector<Solution> & solutions)
{
	GamingConfig::inst().puzzle_count = board.total_puzzle_count();

	// helper
	static struct ResetFreezedRune
	{
		void operator()(Board & board)
		{
			for (std::size_t i = 0; i < ROWS; i++)
			{
				for (size_t j = 0; j < COLS; j++)
				{
					if (board.data[i][j].rune.property == TosRune::PropFreezed)
					{
						board.data[i][j].rune.type = TosRune::RuneHidden;
					}
				}
			}
		}
	} reset_freezed_rune;
	reset_freezed_rune(board);

	//show_board_log(board);
	
	const std::size_t max_solutions_size = GamingConfig::inst().max_solutions_size;
	solutions.reserve(max_solutions_size * 3);
	
	PathMap result;

	Solution seed_solution;
	seed_solution.board = board;
	Solver::evaluate_solution(seed_solution);

    if (paths.empty())
    {
		bool run_all_runes = true;
		if (GamingConfig::inst().start_position == LeftBottom)
		{
			if (seed_solution.board.data[4][0].rune.moveable)
			{
				Solution s = seed_solution;
				s.init_cursor = Point<uint8_t>(4, 0);
				s.cursor = Point<uint8_t>(4, 0);
				solutions.push_back(s);
				run_all_runes = false;
			}
		}
		else if (GamingConfig::inst().start_position == RightBottom)
		{
			if (seed_solution.board.data[4][5].rune.moveable)
			{
				Solution s = seed_solution;
				s.init_cursor = Point<uint8_t>(4, 5);
				s.cursor = Point<uint8_t>(4, 5);
				solutions.push_back(s);
				run_all_runes = false;
			}
		}
		
		if (run_all_runes)
		{
			for (size_t i = 0; i < ROWS; i++)
			{
				for (size_t j = 0; j < COLS; j++)
				{
					if (seed_solution.board.data[i][j].rune.moveable)
					{
						Solution s = seed_solution;
						s.init_cursor = Point<uint8_t>(i, j);
						s.cursor = Point<uint8_t>(i, j);
						solutions.push_back(s);
					}
				}
			}
		}
    }
    else
    {
        LOGD("custom paths %d", paths.size());
        Solution s = seed_solution;
        s.init_cursor = Point<uint8_t>(paths[0].x, paths[0].y);
        s.cursor = Point<uint8_t>(paths[0].x, paths[0].y);

        for (std::size_t i = 0; i < paths.size() - 1; i++)
        {
            if (paths[i] == paths[i+1])
            {
                continue;
            }
            uint8_t dir = dir_from_point(paths[i], paths[i+1]);
            Solver::swap_orb_in_solution(s, dir);
            Solver::evaluate_solution(s);
        }
        s.is_done = false;
        solutions.push_back(s);
    }

	uint8_t direction = GamingConfig::inst().eight_direction_allowed ? 1 : 2;
	SolveState solve_state(GamingConfig::inst().max_steps, direction, paths.size(), solutions);

	Solver::solve_board_step(solve_state);
	
	const Solution & best_solution = solutions[0];

	result.push_back(best_solution.init_cursor);
	
	// note: board.cell(x, y) -> (row, col)
	for (auto dir : best_solution.paths)
	{
		Point<uint8_t> point = result.back();
		switch (dir)
		{
		case RR:
			point.y += 1;
			break;
		case RU:
			point.y += 1;
			point.x -= 1;
			break;
		case RB:
			point.y += 1;
			point.x += 1;
			break;
		case LL:
			point.y -= 1;
			break;
		case LU:
			point.y -= 1;
			point.x -= 1;
			break;
		case LB:
			point.y -= 1;
			point.x += 1;
			break;
		case UU:
			point.x -= 1;
			break;
		case BB:
			point.x += 1;
			break;
		}
		result.push_back(point);
	}

	return result;
}

namespace {

std::random_device rd;
std::mt19937 generator(rd());
int random_near_point(int px)
{
	std::uniform_int_distribution<int> dis(-20, 20);
	return px + dis(generator);
}

} // namespace

std::string GamePlayer::execute(const std::string & command)
{
	LOGD("Metapath execute!!!");

	json request = json::parse(command);

	if (request.find("config") != request.end()) {
		request["config"] >> GamingConfig::inst();
		Device::instance().platform = GamingConfig::inst().device_type;
	}

	if (request["request"] == "track_screen")
	{
		const std::string ui_status_str = request["ui_status"];
		json ui_status = json::parse(ui_status_str);
		const int hide = ui_status[2];
		const int checkcode = request["checkcode"];
		const int type = request["type"];
		const bool should_click = (!hide) && (rand() % 1000 < checkcode);
		if (should_click)
		{
			const int h = ui_status[1];
			const int x = 200 + (rand() % 101) - 50;
			const int y = h * 7.0f / 5.0f + (rand() % 20) + 10;
			const int t = type + (rand() % 10);
			Finger::instance().fast_tap(x, y);
			_gesture.tap(0, h + 10, 50);
			GATracker::getInstance().sendTrackerEvent("trigger", "track_screen", "trigger success");
			//LOGD("Matapath click %d %d %d", x, y, t);
		}
	}
	else if (request["request"] == "load_board")
	{
        LOGD("execute load_board");
		auto board = load_board();
		
        json respond;
        respond["respond"] = "load_board_done";
		respond["board"] << board;
		return respond.dump();
	}
    else if (request["request"] == "calculate_path")
    {
        LOGD("execute calculate_path");
        Board board; 
		request["board"] >> board;
		
        std::vector<Point<std::size_t>> paths;
        for (std::size_t i = 0; i < request["previous_path"].size(); i++)
        {
            json point = request["previous_path"][i];
            paths.emplace_back(point["row"], point["col"]);
        }
		
		std::vector<Solution> solutions;
        auto pathmap = calculate_path(board, paths, solutions);
		
        json respond;
        respond["respond"] = "calculate_path_done";
        respond["paths"] = json();
        for (auto & rune : pathmap)
        {
            json path = {{"row", rune.x}, {"col", rune.y}};
            respond["paths"].push_back(path);
        }
		
		const std::vector<Match> & best_matches = solutions[0].matches;
		respond["matches"] = json::array();
        for (std::size_t i = 0; i < best_matches.size(); i++)
        {
            json match = {{"type", best_matches[i].type}, {"count", best_matches[i].count}
                , {"round", best_matches[i].round}};
            respond["matches"].push_back(match);
        }

		//std::cout << board << std::endl;
		//std::cout << respond.dump() << std::endl;
        return respond.dump();
    }
	else if (request["request"] == "run_path")
    {
        LayoutManager layout_manager(std::make_shared<TosGameLayout>());
        layout_manager.set_rows_count(5);
        layout_manager.set_cols_count(6);

#ifndef __APPLE__
		layout_manager.set_resolution(GamingConfig::inst().width, GamingConfig::inst().height);
#else
        layout_manager.set_resolution(1080, 1920); // FIXME: don't hardcode here
#endif

        std::vector<Point<std::size_t>> paths;

        for (std::size_t i = 0, l = request["previous_path"].size(); i < l; i++)
        {
            auto cell = layout_manager.cell(request["previous_path"][i]["row"], request["previous_path"][i]["col"]);
			paths.emplace_back(
					random_near_point(cell.x + cell.width / 2),
					random_near_point(cell.y + cell.height / 2));
            LOGD("path %d %u %u\n", i, paths.back().x, paths.back().y);
        }
		_gesture.navigate(paths, GamingConfig::inst().navigate_time);
		
        json respond;
        respond["respond"] = "run_path_done";
        return respond.dump();
    }
	else if (request["request"] == "peek_and_play")
    {
    	peek_and_play();

        json respond;
        respond["respond"] = "peek_and_play_done";
        return respond.dump();
    }
	else if (request["request"] == "board_is_stable")
	{
		bool result = board_is_stable();
		
		json respond;
		respond["respond"] = "board_is_stable_done";
		respond["result"] = result;
		return respond.dump();
	}
	else if (request["request"] == "get_ad_id")
	{
		std::vector<std::string> ids999;
		ids999.push_back("3434616449699457/4859433957");
		ids999.push_back("6493638586342373/9836378566");
		int random_idx = std::rand() % ids.size();
		return ids[random_idx];
	}
    else if (request["request"] == "gesture")
    {
        std::string request_type = request["type"];
        if (request_type == "tap")
        {
            std::size_t x = request["x"];
            std::size_t y = request["y"];
            std::size_t interval = request["interval"];
            _gesture.tap(x, y, interval);
        }
        else if (request_type == "swipe")
        {
			std::size_t x1 = request["x1"];
			std::size_t y1 = request["y1"];
			std::size_t x2 = request["x2"];
			std::size_t y2 = request["y2"];
			std::size_t interval = request["interval"];
			_gesture.swipe(x1, y1, x2, y2, interval);
        }
        else if (request_type == "navigate")
        {
            // TODO
        }
        json respond;
        respond["respond"] = "gesture_done";
        return respond.dump();
    }
	else if (request["request"] == "read_level")
	{
		std::string tos_file = TosDatabase::getTosPath(request["tos_version"]);
		std::string save_file = TosDatabase::getSavePath(request["user_file_path"]);
		
		json respond;
		respond["respond"] = "read_level_done";
		_tos_database.copy(tos_file, save_file);
		respond["result"] = _tos_database.read(save_file);
		return respond.dump();
	}
	else if (request["request"] == "save_level")
	{
		std::string tos_file = TosDatabase::getTosPath(request["tos_version"]);
		std::string save_file = TosDatabase::getSavePath(request["user_file_path"]);
		
		json respond;
		
		respond["respond"] = "save_level_done";
		_tos_database.copy(tos_file, save_file);
		respond["result"] = _tos_database.read(save_file);
		_tos_database.backup(tos_file);
		
		return respond.dump();
	}
	else if (request["request"] == "load_level")
	{
		std::string tos_file = TosDatabase::getTosPath(request["tos_version"]);
		
		json respond;
		
		_tos_database.recovery(tos_file);
		respond["respond"] = "load_level_done";
	}
	else if (request["request"] == "logout")
	{
		std::string tos_file = TosDatabase::getTosPath(request["tos_version"]);
		
		json respond;
		
		_tos_database.remove(tos_file);
		respond["respond"] = "logout_done";
	}

    return "wrong command";
}

void GamePlayer::peek_and_play(const std::string & filepath)
{
    LayoutManager layout_manager(std::make_shared<TosGameLayout>());
    layout_manager.set_rows_count(5);
    layout_manager.set_cols_count(6);

#ifndef __APPLE__
	layout_manager.set_resolution(GamingConfig::inst().width, GamingConfig::inst().height);
#else
	layout_manager.set_resolution(1080, 1920); // FIXME: don't hardcode here
#endif
	LOGD("Matapath width: %d, height :%d", GamingConfig::inst().width, GamingConfig::inst().height);
    
    auto cell1 = layout_manager.cell(1, 0);
    auto cell2 = layout_manager.cell(0, 0);
    
    std::vector<Point<std::size_t>> navigate_paths;
    navigate_paths.emplace_back(
        random_near_point(cell1.x + cell1.width / 2),
        random_near_point(cell1.y + cell1.height / 2));
    navigate_paths.emplace_back(
        random_near_point(cell2.x + cell2.width / 2),
        random_near_point(cell2.y + cell2.height / 2));

	LOGD("Matapath peek");
#ifndef __APPLE__
    _gesture.navigate(navigate_paths, 80, true, false);
#endif
    navigate_paths.clear();
	
	usleep(100 * 1000);

    std::vector<Point<std::size_t>> previous_paths;
	previous_paths.emplace_back(1, 0);
	previous_paths.emplace_back(0, 0);

    auto board = load_board(filepath);
	std::vector<Solution> solutions;
    auto pathmap = calculate_path(board, previous_paths, solutions);
	LOGD("Matapath combo %d", solutions[0].matches.size());
    
    for (std::size_t i = 1; i < pathmap.size(); i++)
    {
        auto & rune = pathmap[i];
        auto cell = layout_manager.cell(rune.x, rune.y);
        navigate_paths.emplace_back(
            random_near_point(cell.x + cell.width / 2),
            random_near_point(cell.y + cell.height / 2));
    }
    
	LOGD("Matapath play");
#ifndef __APPLE__
    _gesture.navigate(navigate_paths, 2000, false, true);
#endif
}

bool GamePlayer::board_is_stable() const
{
	LayoutManager layout_manager(std::make_shared<TosGameLayout>());
	layout_manager.set_rows_count(5);
	layout_manager.set_cols_count(6);
	
#ifndef __APPLE__
	layout_manager.set_resolution(GamingConfig::inst().width, GamingConfig::inst().height);
#else
	layout_manager.set_resolution(1080, 1920); // FIXME: don't hardcode here
#endif
	
#ifndef __APPLE__
	auto test_cell = layout_manager.cell(4, 1);
	
	LOGD("Matapath board_is_stable img 1");
	Image before_test_img = Eye::instance().take();
	Image before_test_rune_img = before_test_img.crop(test_cell.x, test_cell.y, test_cell.width, test_cell.height);
	
	LOGD("Matapath board_is_stable tap");
	//_gesture.tap(test_cell.x + test_cell.width / 2, test_cell.y + test_cell.height / 2, 100);
	Finger::instance().press(test_cell.x + test_cell.width / 2, test_cell.y + test_cell.height / 2);
	
	usleep(100 * 1000);
	
	LOGD("Matapath board_is_stable img 2");
	Image after_test_img = Eye::instance().take();
	Image after_test_rune_img = after_test_img.crop(test_cell.x, test_cell.y, test_cell.width, test_cell.height);
	
	Finger::instance().release();
    
	Matcher matcher;
	matcher.set_match_method(MatchMethod::CcoeffNormed());
	
	LOGD("Matapath match on");
	MatchResult match_result = matcher.match(before_test_rune_img, after_test_rune_img);
	
	float diff = match_result.max();
	return diff < 0.7 && diff > 0.2;
#else
	return false;
#endif
}

void GamePlayer::request_administrator()
{
    Eye::instance().take();
}
