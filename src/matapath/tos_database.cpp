#include <thirdparty/pugixml.hpp>
#include <thirdparty/json.hpp>
#include <unistd.h>
#include <stdio.h>

#include "matapath/tos_database.h"
#include "human/systemcall.hpp"

#if !defined(__APPLE__) && !defined(NDEBUG)
#include "android/log.h"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)
#else
#define LOGD(...) ;
#endif

namespace {
	
std::string urlDecode(const std::string & SRC) {
	std::string ret;
	char ch;
	int i, ii;
	for (i=0; i<SRC.length(); i++) {
		if (int(SRC[i])==37) {
			sscanf(SRC.substr(i+1,2).c_str(), "%x", &ii);
			ch=static_cast<char>(ii);
			ret+=ch;
			i=i+2;
		} else {
			ret+=SRC[i];
		}
	}
	return (ret);
}
	
nlohmann::json extract(const std::string & input)
{
	nlohmann::json result;
	try
	{
		result = nlohmann::json::parse(input);
		LOGD("Matapath.db: json");
	}
	catch (...)
	{
		result = nlohmann::json(input);
		LOGD("Matapath.db: string");
	}
	return result;
}
	
}

std::string TosDatabase::getTosPath(const std::string & prefix)
{
	return std::string("/data/data/com.madhead.")
		+ prefix + "/shared_prefs/com.madhead."
		+ prefix + ".v2.playerprefs.xml";
}

std::string TosDatabase::getSavePath(const std::string & path)
{
	return path + "/cache.mp";
}

std::string TosDatabase::read(const std::string & filepath)
{
	nlohmann::json result;
	std::string not_endode_names("REPORT_COMMIT_KEY|MH_APP_NOTIFICATION_DATA|APPEARED_CHALLENGE_FLOORS");
	pugi::xml_document doc;
	pugi::xml_parse_result xml_result = doc.load_file(filepath.c_str());

	LOGD("Matapath.db: filepath %s", filepath.c_str());

	if (xml_result != NULL)
	{
		LOGD("Matapath.db: load_file ok");
		for (pugi::xml_node & map : doc.children("map"))
		{
			for (pugi::xml_node & string : map.children("string"))
			{
				nlohmann::json json_value;
				std::string name(string.attribute("name").value());
				std::string decode_value = urlDecode(std::string(string.child_value()));
				if (decode_value.length() > 32 && not_endode_names.find(name) == std::string::npos)
				{
					decode_value = decode_value.substr(32);
				}
				/*
				if (name == "MH_CACHE_RUNTIME_DATA_CURRENT_FLOOR_ENTER_DATA")
				{
					decode_value = decode_value.substr(449);
				}
				else if (name == "PRE_BATTLE_INFO_COMPAREAPI_DATA_JSON")
				{
					decode_value = decode_value.substr(50);
				}
				else if (name == "PRE_BATTLE_INFO_COMPAREAPI_DATA_STAGE_JSON")
				{
					decode_value = decode_value.substr(55);
				}
				else if (name == "PRE_BATTLE_INFO_COMPAREAPI_DATA_FLOOR_JSON")
				{
					decode_value = decode_value.substr(55);
				}
				else if (name == "MH_CACHE_API_DATA_JSON")
				{
					decode_value = decode_value.substr(1);
				}
				*/
				
				if (name == "MH_CACHE_RUNTIME_DATA_CURRENT_FLOOR_WAVES")
				{
					LOGD("Matapath.db: MH_CACHE_RUNTIME_DATA_CURRENT_FLOOR_WAVES");
					result["dz"] = extract(decode_value);
				}
				else if (name == "MH_CACHE_GAMEPLAY_DATA_CURRENT_WAVE")
				{
					LOGD("Matapath.db: MH_CACHE_GAMEPLAY_DATA_CURRENT_WAVE");
					result["dx"] = extract(decode_value);
				}
				else if (name == "MH_CACHE_API_DATA_STAGE_JSON")
				{
					//LOGD("Matapath.db: MH_CACHE_API_DATA_STAGE_JSON");
					//result["MH_CACHE_API_DATA_STAGE_JSON"] = extract(decode_value);
				}
				else if (name == "MH_CACHE_API_DATA_FLOOR_JSON")
				{
					//LOGD("Matapath.db: MH_CACHE_API_DATA_FLOOR_JSON");
					//result["MH_CACHE_API_DATA_FLOOR_JSON"] = extract(decode_value);
				}
				else if (name == "MH_CACHE_API_DATA_MONSTER_JSON")
				{
					LOGD("Matapath.db: MH_CACHE_API_DATA_MONSTER_JSON");
					result["dy"] = extract(decode_value);
				}
				
				/*
				try
				{
					json_value = nlohmann::json::parse(decode_value);
					std::cout << name << " : \n" << json_value.dump(4) << "\n" << std::endl;
				} catch (...)
				{
					std::cout << name << " : " << decode_value << "\n" << std::endl;
				}
				 */
			}
		}
	} else {
		LOGD("Matapath.db: can not open xml %d", errno);
	}
	LOGD("Matapath.db: DONE");
	return result.dump();
}

void TosDatabase::copy(const std::string & from, const std::string & to)
{
	std::string cmd = "cp ";
	cmd.append(from);
	cmd.append(" ");
	cmd.append(to);
	cmd.append("\n");
	int uid = getuid();
	LOGD("Matapath.db: cmd %s", cmd.c_str());
	LOGD("Matapath.db: cmd %s", (std::string("chown ") + std::to_string(uid) + "." + std::to_string(uid) + " " + to + "\n").c_str());
	SystemCall::execute("rm " + to + "\n");
	SystemCall::execute(cmd);
	SystemCall::execute(std::string("chown ") + std::to_string(uid) + "." + std::to_string(uid) + " " + to + "\n");
	SystemCall::refresh();
	usleep(200 * 1000);
}

void TosDatabase::backup(const std::string & path)
{
	// cp -p -f path path.backup
	std::string cmd = "cp -p -f ";
	cmd.append(path + " " + path + ".backup");
	cmd.append("\n");

	SystemCall::execute(cmd);
	SystemCall::refresh();
	usleep(200 * 1000);
}

void TosDatabase::recovery(const std::string & path)
{
	// cp -p path.backup path
	std::string cmd = "cp -p ";
	cmd.append(path + ".backup " + path);
	cmd.append("\n");

	SystemCall::execute(cmd);
	SystemCall::refresh();
	usleep(200 * 1000);
}

void TosDatabase::remove(const std::string & path)
{
	// rm -f path path.backup 
	std::string cmd = "rm -f ";
	cmd.append(path + " " + path + ".backup");
	cmd.append("\n");

	SystemCall::execute(cmd);
	SystemCall::refresh();
	usleep(200 * 1000);
}
