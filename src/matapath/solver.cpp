#include "matapath/solver.h"

#include <functional>
#include <algorithm>
#include "matapath/rune.h"
#include "matapath/basic_types.h"
#include "matapath/match_result.h"

#if !defined(__APPLE__) && !defined(NDEBUG)	
#include "android/log.h"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)
#else
#define LOGD(...) ;
#endif

namespace WorkInProgress
{

#if 0
class Solver
{
public:
	std::vector<Match> find_matches(Board & board, Board & match_board);
	void remove_matches(Board & board, const Board & match_board);
	void drop_empty_spaces(Board & board);
	void evaluate_solution(Solution & solution, const Weights & weights);
	bool can_move_orb(Point rc, uint8_t dir);
	void move_rc(Point & rc, uint8_t dir);
	bool can_move_orb_in_solution(const Solution & solution, uint8_t dir);
	Point swap_orb(Board & board, Point rc, uint8_t dir);
	void swap_orb_in_solution(Solution & solution, uint8_t dir);
	void evolve_solutions(std::vector<Solution> & solutions, const Weights & weights, uint8_t dir_step);
	void solve_board_step(SolveState & solve_state);
};
#endif

}

namespace Solver
{

Solution best_done_solution;

uint8_t babylon_achieve_col[6] = {0, 0, 0, 0, 0, 0};

void make_weights(Weights & weights)
{
	for (std::size_t i = 1; i < TosRune::RuneTypeEnd; i++)
	{
		weights[i].value[Normal] = 1;
		weights[i].value[Mass] = 3;
	}
}

void make_weights2(Weights & weights)
{
	for (std::size_t i = 1; i < TosRune::RuneTypeEnd; i++)
	{
		weights[i].value[Normal] = 1;
		weights[i].value[Mass] = 1;
	}
}

// FIXME: deprecated
float compute_weight(const std::vector<Match> & matches, const Weights & weights)
{
#if 0
	float total_weight = 0;
	for (auto & match : matches)
	{
		float base_weight = 1.5;
		// attack-type score
		if (GamingConfig::inst().rune_attack_type[match.type] == match.count)
			base_weight *= 2;

		float multi_orb_bonus = (match.count - 3) * MULTI_ORB_BONUS + 1;
		// priority score
		float priority_score = GamingConfig::inst().rune_priority[match.type];
		// round-type score
		float round_type_score = 1.0;
		if (GamingConfig::inst().rune_round_type[match.type] != Auto)
		{
			if (GamingConfig::inst().rune_round_type[match.type] == First)
			{
				if (match.round == 0) round_type_score = 2;
			}
			else if (GamingConfig::inst().rune_round_type[match.type] == NotFirst)
			{
				if (match.round == 0) round_type_score = -1;
			}
		}
		total_weight += multi_orb_bonus * base_weight * priority_score * round_type_score;
	}
	float combo_bonus = (matches.size() - 1) * COMBO_BONUS + 1;
	return total_weight * combo_bonus;
#else
	return 0;
#endif
}
	
void eliminate_marks(Board & board, const std::vector<Point> & rollback_map, Board * origin_board)
{
	uint8_t row, col;
	for (std::size_t i = 0; i < rollback_map.size(); i++)
	{
		row = rollback_map[i].x;
		col = rollback_map[i].y;

		board.data[row][col].rune.type = 0;
		if (origin_board != NULL)
		{
			origin_board->data[row][col].rune.matched = false;
		}
	}
}

void find_matches(std::vector<Match> & matches, Board & board, Board & match_board, Board * origin_board)
{
	if (GamingConfig::inst().babylon_type != TosRune::RuneNotFound)
	{
		// init 
		babylon_achieve_col[0] = 0;
		babylon_achieve_col[1] = 0;
		babylon_achieve_col[2] = 0;
		babylon_achieve_col[3] = 0;
		babylon_achieve_col[4] = 0;
		babylon_achieve_col[5] = 0;
	}

	const bool legency_greece_enabled = GamingConfig::inst().has_legency_greece;
	typedef decltype(GamingConfig::inst().rune_attack_type) rune_attack_type_t;
	const rune_attack_type_t & rune_attack_type = GamingConfig::inst().rune_attack_type;
	for (std::size_t i = 0; i < board.rows; i++)
	{
		uint8_t prev_1_orb = 0;
		uint8_t prev_2_orb = 0;
		for (std::size_t j = 0; j < board.cols; j++)
		{
			uint8_t cur_orb = board.data[i][j].rune.type;
			const bool two_link_enabled = rune_attack_type[cur_orb] == TwoLink;
			
			if (two_link_enabled || legency_greece_enabled)
			{
				if (prev_2_orb == cur_orb
					&& cur_orb != TosRune::RuneNotFound && cur_orb != TosRune::RuneHidden)
				{
					match_board.data[i][j].rune.type = cur_orb;
					match_board.data[i][j - 1].rune.type = cur_orb;
				}
			}
			else
			{
				if (prev_1_orb == prev_2_orb && prev_2_orb == cur_orb
					&& cur_orb != TosRune::RuneNotFound && cur_orb != TosRune::RuneHidden)
				{
					match_board.data[i][j].rune.type = cur_orb;
					match_board.data[i][j - 1].rune.type = cur_orb;
					match_board.data[i][j - 2].rune.type = cur_orb;
				}
			}
			prev_1_orb = prev_2_orb;
			prev_2_orb = cur_orb;
		}
	}

	
	
	for (std::size_t j = 0; j < board.cols; j++)
	{
		uint8_t prev_1_orb = 0;
		uint8_t prev_2_orb = 0;
		uint8_t col_link_count = 0;
		for (std::size_t i = 0; i < board.rows; i++)
		{
			uint8_t cur_orb = board.data[i][j].rune.type;
			const bool two_link_enabled = rune_attack_type[cur_orb] == TwoLink;

			if (two_link_enabled || legency_greece_enabled)
			{
				if (prev_2_orb == cur_orb
					&& cur_orb != TosRune::RuneNotFound && cur_orb != TosRune::RuneHidden)
				{
					match_board.data[i][j].rune.type = cur_orb;
					match_board.data[i - 1][j].rune.type = cur_orb;
				}
			}
			else
			{
				if (prev_1_orb == prev_2_orb && prev_2_orb == cur_orb
					&& cur_orb != TosRune::RuneNotFound && cur_orb != TosRune::RuneHidden)
				{
					match_board.data[i][j].rune.type = cur_orb;
					match_board.data[i - 1][j].rune.type = cur_orb;
					match_board.data[i - 2][j].rune.type = cur_orb;
					col_link_count++;
				}
			}
			prev_1_orb = prev_2_orb;
			prev_2_orb = cur_orb;

			// note: link greater than 4
			if (col_link_count >= 4)
			{
				babylon_achieve_col[j] = col_link_count == 5 ? 3 : 2;
			}
		}
	}
	
	
	static struct LocalVariableHolder
	{
		LocalVariableHolder()
		{
			stack.reserve(64);
			rollback_map.reserve(32);
		}
		
		std::vector<Point> stack;
		std::vector<Point> rollback_map;
	} local;
	
	Board scratch_board = match_board;
	std::vector<Point> & stack = local.stack;
	std::vector<Point> & rollback_map = local.rollback_map;
	
	stack.clear();
	rollback_map.clear();
	
	for (std::size_t i = 0; i < scratch_board.rows; i++)
	{
		for (std::size_t j = 0; j < scratch_board.cols; j++)
		{
			uint8_t cur_orb = scratch_board.data[i][j].rune.type;
			if (cur_orb == 0) continue;
			rollback_map.clear();
			stack.clear();
			stack.push_back(Point(i, j));
			Match match(cur_orb);
			while (stack.size())
			{
				Point n = stack.back();
				stack.pop_back();
				if (scratch_board.data[n.x][n.y].rune.type != cur_orb) continue;
				match.count++;
				match.row_count[n.x]++;
				match.col_count[n.y]++;

				if (origin_board != NULL)
				{
					origin_board->data[n.x][n.y].rune.matched = true;
				}
				if (board.data[n.x][n.y].rune.enhanced)
					match.enhanced_count++;
				scratch_board.data[n.x][n.y].rune.type = 0;
				rollback_map.push_back(Point(n.x, n.y));
				if (n.x > 0) stack.push_back(Point(n.x - 1, n.y));
				if (n.x < scratch_board.rows - 1) stack.push_back(Point(n.x + 1, n.y));
				if (n.y > 0) stack.push_back(Point(n.x, n.y - 1));
				if (n.y < scratch_board.cols - 1) stack.push_back(Point(n.x, n.y + 1));
			}
			if (legency_greece_enabled && rune_attack_type[cur_orb] != TwoLink)
			{
				if (match.count < 3)
				{
					eliminate_marks(match_board, rollback_map, origin_board);
					continue;
				}
			}
			matches.push_back(match);
		}
	}
}


void remove_matches(Board & board, const Board & match_board)
{
	for (std::size_t i = 0; i < board.rows; i++)
	{
		for (std::size_t j = 0; j < board.cols; j++)
		{
			if (match_board.data[i][j].rune.type != 0)
				board.data[i][j].rune.type = 0;
		}
	}
}

void drop_empty_spaces(Board & board, uint8_t round)
{
	uint8_t babylon_type = GamingConfig::inst().babylon_type;

	for (std::size_t j = 0; j < board.cols; j++)
	{
		int dest_i = board.rows - 1;
		for (int src_i = board.rows - 1; src_i >= 0; src_i--)
		{
			if (board.data[src_i][j].rune.type != 0)
			{
				board.data[dest_i][j].rune.type = board.data[src_i][j].rune.type;
				--dest_i;
			}
		}
		for (; dest_i >= 0; --dest_i)
		{
			if (babylon_type != TosRune::RuneNotFound && babylon_achieve_col[j] != 0 && round == 1)
			{
				//LOGD("Matapath has babylon drop");
				board.data[dest_i][j].rune.type = babylon_type;
				babylon_achieve_col[j]--;
				continue;
			}
			board.data[dest_i][j].rune.type = 0;
		}
	}
}


void evaluate_solution(Solution & solution)
{
	Board current_board = solution.board;
	solution.matches.clear();
	
	std::vector<Match> matches;
	matches.reserve(16);
	for (uint8_t round = 1; true; round++)
	{
		Board match_board;
		matches.clear();
		find_matches(matches, current_board, match_board, &solution.board);
		
		if (matches.empty())
		{
			break;
		}
		for (auto & match : matches)
		{
			match.round = round;
		}
		remove_matches(current_board, match_board);
		drop_empty_spaces(current_board, round);
		solution.matches.insert(solution.matches.end(), matches.begin(), matches.end());
	}
	MatchResult match_result = MatchResult(solution.matches, solution.board);
	solution.weight = match_result.calculate_score();

	//solution.weight = compute_weight(solution.matches, weights);
}

bool can_move_orb(Point rc, uint8_t dir)
{
	switch (dir)
	{
		case 0: return                  rc.y < COLS-1;
		case 1: return rc.x < ROWS-1 && rc.y < COLS-1;
		case 2: return rc.x < ROWS-1;
		case 3: return rc.x < ROWS-1 && rc.y > 0;
		case 4: return                  rc.y > 0;
		case 5: return rc.x > 0      && rc.y > 0;
		case 6: return rc.x > 0;
		case 7: return rc.x > 0      && rc.y < COLS-1;
		default: return false;
	}
	return false;
}

void move_rc(Point & rc, uint8_t dir)
{
    switch (dir) {
        case 0:            rc.y += 1; break;
        case 1: rc.x += 1; rc.y += 1; break;
        case 2: rc.x += 1;            break;
        case 3: rc.x += 1; rc.y -= 1; break;
        case 4:            rc.y -= 1; break;
        case 5: rc.x -= 1; rc.y -= 1; break;
        case 6: rc.x -= 1;            break;
        case 7: rc.x -= 1; rc.y += 1; break;
        default: break;
    }
}


bool can_move_orb_in_solution(const Solution & solution, uint8_t dir)
{
	if (!solution.paths.empty() && solution.paths.back() == (dir + 4) % 8) return false;
	if (!can_move_orb(solution.cursor, dir)) return false;
	
	Point next_rc = solution.cursor;
	move_rc(next_rc, dir);
	if (!solution.board.data[next_rc.x][next_rc.y].rune.moveable) return false;
	
	if (GamingConfig::inst().ignited_path != Auto)
	{
		if (solution.step_lookup_board.data[next_rc.x][next_rc.y] == 0)
		{
			return true;
		}
		if (solution.paths.size() + 1 - solution.step_lookup_board.data[next_rc.x][next_rc.y] <= GamingConfig::inst().ignited_path)
		{
			return false;
		}
	}
	return true;
	//return can_move_orb(solution.cursor, dir);
}


Point swap_orb(Board & board, Point rc, uint8_t dir)
{
	Point old_rc = rc;
	move_rc(rc, dir);
	std::swap(board.data[old_rc.x][old_rc.y].rune, board.data[rc.x][rc.y].rune);
	return rc;
}

void swap_orb_in_solution(Solution & solution, uint8_t dir)
{
	solution.step_lookup_board.data[solution.cursor.x][solution.cursor.y] = solution.paths.size() + 1;
	solution.cursor = swap_orb(solution.board, solution.cursor, dir);
	solution.paths.push_back(dir);
}


void evolve_solutions(std::vector<Solution> & solutions, uint8_t dir_step)
{
	const std::size_t max_solutions_size = GamingConfig::inst().max_solutions_size;
	
	static std::vector<Solution> new_solutions;
	if (new_solutions.capacity() < max_solutions_size * 4)
	{
		new_solutions.reserve(max_solutions_size * 4);
	}
	new_solutions.clear();
	
	uint8_t i = 0;
	for (auto & s : solutions)
	{
		if (s.is_done) continue;
		for (uint8_t dir = 0; dir < 8; dir += dir_step)
		{
			if (!can_move_orb_in_solution(s, dir)) continue;
			Solution solution = s;
			swap_orb_in_solution(solution, dir);
			evaluate_solution(solution);
			new_solutions.push_back(solution);
		}
		s.is_done = true;
	}
	solutions.insert(solutions.end(), new_solutions.begin(), new_solutions.end());
	std::sort(solutions.begin(), solutions.end(), std::greater<Solution>());
	if (solutions.size() > max_solutions_size)
		solutions.resize(max_solutions_size);
}



void solve_board_step(SolveState & solve_state)
{
	for (; solve_state.p < solve_state.max_length; ++solve_state.p)
	{
		evolve_solutions(
			solve_state.solutions,
			solve_state.dir_step);
	}
}

}
