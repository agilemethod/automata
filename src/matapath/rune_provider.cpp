#include "matapath/rune_provider.h"
#include <thirdparty/json.hpp>
#include <io/image.hpp>

using namespace nlohmann;

RuneProvider::RuneProvider(const std::string & data)
{
	json rune_samples_json = json::parse(data);
	samples.reserve(100);
	for (auto & each_rune : rune_samples_json["runes"])
	{
		RuneSample sample;
		each_rune["image"] >> sample.image;
		sample.threshold = each_rune["threshold"];
		sample.type = each_rune["type"];
		sample.prop = each_rune["prop"];
		samples.push_back(sample);
	}
}

