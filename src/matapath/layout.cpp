#include "matapath/layout.h"
#include <cassert>
#include <cmath>

void LayoutManager::set_resolution(float width, float height)
{
	auto & game_layout = *_game_layout;
	double ratio = height / width;
	Rect<std::size_t> & screen = _screen;
	Rect<std::size_t> & board = _board;

	if (ratio == game_layout.game_screen_ratio())
	{
		screen.x = 0;
		screen.y = 0;
		screen.width = width;
		screen.height = height;
		board.x = 0;
	}
	else if (ratio > game_layout.game_screen_ratio())
	{
		screen.height = std::ceil(width * game_layout.game_screen_ratio());
		screen.width = width;
		screen.x = 0;
		screen.y = (height - screen.height) / 2;
		board.x = 0;
	}
	else
	{ // width > height
		screen.height = height;
		screen.width = std::ceil(height / game_layout.game_screen_ratio());
		screen.x = (width - screen.width) / 2;
		screen.y = 0;
	}
	board.width = screen.width;
	board.height = screen.width * game_layout.board_ratio();
	board.x = screen.x;
	board.y = screen.y + screen.height - board.height;
	
	_cell_width = board.width / _n_cols;
	_cell_height = board.height / _n_rows;

	_border_width = _cell_width * 0.076;
	_border_height = _cell_height * 0.076;
}

void LayoutManager::reset_board_size(float width, float height)
{
	_cell_width = width / _n_cols;
	_cell_height = height / _n_rows;
	
	_border_width = _cell_width * 0.076;
	_border_height = _cell_height * 0.076;
}

std::size_t LayoutManager::n_rows() const
{
	return _n_rows;
}

std::size_t LayoutManager::n_cols() const
{
	return _n_cols;
}

Rect<std::size_t> LayoutManager::board() const
{
	return _board;
}

Rect<std::size_t> LayoutManager::screen() const
{
	return _screen;
}

Rect<std::size_t> LayoutManager::cell(std::size_t row, std::size_t col) const
{
	Rect<std::size_t> cell;
	cell.x = _board.x + _cell_width * col;
	cell.y = _board.y + _cell_height * row;
	cell.width = _cell_width;
	cell.height = _cell_height;

	return cell;
}

Rect<std::size_t> LayoutManager::vborder(std::size_t row, std::size_t col) const
{
	assert(row < n_rows());
	assert(col <= n_cols());

	Rect<std::size_t> border;

	border.x = col * _cell_width - std::ceil(_border_width / 2.0);
	border.y = row * _cell_height;
	border.width = _border_width;
	border.height = _cell_height;

	if (col == 0)
	{
		border.x = 0;
		border.width /= 2;
	}
	else if (col == n_cols())
	{
		border.width /= 2;
	}
	else
	{
		border.x += 1;
	}

	return border;
}

Rect<std::size_t> LayoutManager::hborder(std::size_t row, std::size_t col) const
{
	// note: hborder is different from vborder
	//       hborder will get same height for both outside-border and inside-border
	
	assert(row <= n_rows());
	assert(col < n_cols());

	Rect<std::size_t> border;

	auto half_hieght = round(_border_height / 2.0);
	border.x = col * _cell_width;
	border.y = row * _cell_height - half_hieght;
	border.width = _cell_width;
	border.height = half_hieght;

	if (row == 0)
	{
		border.y = 0;
	}
	else if (row == n_rows())
	{
	}
	else
	{
		border.y += 1;
	}

	return border;
}
