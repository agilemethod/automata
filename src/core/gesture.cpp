#include "core/gesture.h"

#include <chrono>
#include <thread>
#include <human/finger.h>
#include <unistd.h>

namespace {

void stay(float milliseconds)
{
    usleep(milliseconds * 1000);
	//std::this_thread::sleep_for(std::chrono::milliseconds(static_cast<long long>(milliseconds)));
}

void move_interpolately(int start_x, int start_y, int end_x, int end_y, float interval_ms)
{
	const int step = 1;
	const float dt = interval_ms / step;
	const int dx = (end_x - start_x) / step;
	const int dy = (end_y - start_y) / step;
	int x = start_x;
	int y = start_y;

	for (std::size_t i = 0; i < step; i++)
	{
		stay(dt);
		x += dx;
		y += dy;
		Finger::instance().move(static_cast<std::size_t>(x), static_cast<std::size_t>(y));
	}
}

}

void Gesture::tap(std::size_t x, std::size_t y, float interval_ms)
{
	Finger::instance().press(x, y);
	stay(interval_ms);
	Finger::instance().release();
}

void Gesture::swipe(std::size_t start_x, std::size_t start_y, std::size_t end_x, std::size_t end_y, float interval_ms)
{
	const float dt = interval_ms / 2;
	Finger::instance().press(start_x, start_y);
	move_interpolately(start_x, start_y, end_x, end_y, dt);
	stay(dt);
	Finger::instance().release();
}

void Gesture::navigate(const std::vector<Point<std::size_t>> & paths, float interval_ms)
{
	if (paths.size() == 0)
	{
		return;
	}
	const float dt = interval_ms / paths.size();
	std::size_t start_x = paths[0].x;
	std::size_t start_y = paths[0].y;
	std::size_t end_x;
	std::size_t end_y;

	Finger::instance().press(start_x, start_y);
	for (std::size_t i = 1; i < paths.size(); i++)
	{
		end_x = paths[i].x;
		end_y = paths[i].y;
		move_interpolately(start_x, start_y, end_x, end_y, dt);
		start_x = paths[i].x;
		start_y = paths[i].y;
	}
	stay(dt);
	Finger::instance().release();
}

void Gesture::navigate(const std::vector<Point<std::size_t>> & paths, float interval_ms, bool press, bool release)
{
    const float dt = interval_ms / paths.size();
    std::size_t start_x = paths[0].x;
    std::size_t start_y = paths[0].y;
    std::size_t end_x;
    std::size_t end_y;
    
    if (press) Finger::instance().press(start_x, start_y);
    for (std::size_t i = 1; i < paths.size(); i++)
    {
        end_x = paths[i].x;
        end_y = paths[i].y;
        move_interpolately(start_x, start_y, end_x, end_y, dt);
        start_x = paths[i].x;
        start_y = paths[i].y;
    }
    if (release) Finger::instance().release();
}
