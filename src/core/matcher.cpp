#include "core/matcher.h"

#include <opencv2/opencv.hpp>
#include "core/image_p.h"


#ifndef __APPLE__
#include "android/log.h"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)
#else
#define LOGD(...) ;
#endif

MatchMethod MatchMethod::Sqdiff()
{
	static const MatchMethod result = { CV_TM_SQDIFF, false };
	return result;
}

MatchMethod MatchMethod::SqdiffNormed()
{
	static const MatchMethod result = { CV_TM_SQDIFF_NORMED, false };
	return result;
}

MatchMethod MatchMethod::Ccorr()
{
	static const MatchMethod result = { CV_TM_CCORR, true };
	return result;
}

MatchMethod MatchMethod::CcorrNormed()
{
	static const MatchMethod result = { CV_TM_CCORR_NORMED, true };
	return result;
}

MatchMethod MatchMethod::Ccoeff()
{
	static const MatchMethod result = { CV_TM_CCOEFF, true };
	return result;
}

MatchMethod MatchMethod::CcoeffNormed()
{
	static const MatchMethod result = { CV_TM_CCOEFF_NORMED, true };
	return result;
}


//================================================================================
struct MatchResult::PIMPL
{
	cv::Mat data;
	double min_value;
	double max_value;
	cv::Point min_location;
	cv::Point max_location;
};


//================================================================================
MatchResult::MatchResult()
	: member(new PIMPL())
{
}

#ifndef NDEBUG
void MatchResult::setup(uint8_t version)
{
	cv::Mat result(2, 3, CV_32FC1);

	switch (version)
	{
	case 0:
		result.at<float>(0, 0) = 0.1;
		result.at<float>(0, 1) = 0.2;
		result.at<float>(0, 2) = 0.3;
		result.at<float>(1, 0) = 0.4;
		result.at<float>(1, 1) = 0.5;
		result.at<float>(1, 2) = 0.6;
		member->data = result;
		break;
	case 1:
		result.at<float>(0, 1) = 0.1;
		member->data = result;
		break;
	default:
		break;
	}
	update();
}
#endif

std::vector<float> MatchResult::score() const
{
	std::vector<float> result(count());

	for (std::size_t i = 0; i < count(); i++)
	{
		result[i] = member->data.at<float>(i);
	}

	return result;
}

float MatchResult::max() const
{
	return static_cast<float>(member->max_value);
}

float MatchResult::min() const
{
	return static_cast<float>(member->min_value);
}

std::tuple<int, int> MatchResult::max_location() const
{
	return std::make_tuple(member->max_location.x, member->max_location.y);
}

std::tuple<int, int> MatchResult::min_location() const
{
	return std::make_tuple(member->min_location.x, member->min_location.y);
}

std::size_t MatchResult::count() const
{
	return member->data.total();
}

void MatchResult::normalize()
{
	cv::normalize(member->data, member->data, 0.0, 1.0,  cv::NORM_MINMAX);
	update();
}

void MatchResult::update()
{
	cv::minMaxLoc(
		member->data,
		&(member->min_value),
		&(member->max_value),
		&(member->min_location),
		&(member->max_location));
}


//================================================================================
namespace {

// helper
int PixelFormatToCvType(PixelFormat pixel_format)
{
	switch (pixel_format)
	{
	case PF_8U_1C:
		return CV_8UC1;
	case PF_8U_2C:
		return CV_8UC2;
	case PF_8U_3C:
		return CV_8UC3;
	case PF_8U_4C:
		return CV_8UC4;
	default:
		throw std::runtime_error("Bad pixel format!");

	}
}

} // namespace

double Matcher::mean_hsv_distance(const Image &source, uint32_t h, uint32_t s, uint32_t v)
{
	cv::Scalar scalar = cv::mean(source._pimpl->mat);
	cv::Mat a1(1, 1, CV_8UC3, scalar);
	cv::Mat a2(1, 1, CV_8UC3);
	a2.at<cv::Vec3b>(0, 0) = cv::Vec3b(h, s, v);
	return cv::norm(a1, a2);
}

MatchResult Matcher::match(const Image & source, const Image & templ)
{
	cv::Mat ref = source._pimpl->mat;
	cv::Mat tpl = templ._pimpl->mat;

	cv::Mat result(ref.rows - tpl.rows + 1, ref.cols - tpl.cols + 1, CV_32FC1);
	cv::matchTemplate(ref, tpl, result, _match_method.method);

	MatchResult match_result;
	match_result.member->data = result;
	match_result.update();

	return match_result;
}
