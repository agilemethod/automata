#include "core/image.h"
#include "core/image_p.h"

#include <string>

Image::Image()
	: _pimpl(new ImagePimpl())
{
}

Image::Image(const std::string & path)
	: _pimpl(new ImagePimpl())
{
	_pimpl->mat = cv::imread(path.c_str(), CV_LOAD_IMAGE_COLOR);
}

Image::Image(const uint8_t * data, std::size_t width, std::size_t height, PixelFormat pixel_format)
	: _pimpl(new ImagePimpl())
{
	switch (pixel_format)
	{
	case PF_8U_1C:
		_pimpl->mat = cv::Mat(height, width, CV_8UC1, (void *)data).clone();
		break;
	case PF_8U_2C:
		_pimpl->mat = cv::Mat(height, width, CV_8UC2, (void *)data).clone();
		break;
	case PF_8U_3C:
		_pimpl->mat = cv::Mat(height, width, CV_8UC3, (void *)data).clone();
		break;
	case PF_8U_4C:
		_pimpl->mat = cv::Mat(height, width, CV_8UC4, (void *)data).clone();
		break;
	case PF_RGBA8888:
		_pimpl->mat = cv::Mat(height, width, CV_8UC4, (void *)data).clone();
		try {
			cvtColor(_pimpl->mat, _pimpl->mat, CV_RGBA2BGR);
		} catch (...) {}
		break;
	case PF_RGB888:
		_pimpl->mat = cv::Mat(height, width, CV_8UC3, (void *)data).clone();
		try {
			cvtColor(_pimpl->mat, _pimpl->mat, CV_RGB2BGR);
		} catch (...) {}
		break;
	case PF_RGB565:
		_pimpl->mat = cv::Mat(height, width, CV_8UC2, (void *)data).clone();
		try {
			cvtColor(_pimpl->mat, _pimpl->mat, CV_BGR5652RGB);
		} catch (...) {}
		break;
	default:
		throw std::runtime_error("Bad pixel format!");
	}

}

Image::Image(const Image & that)
	: _pimpl(new ImagePimpl())
{
	_pimpl->mat = that._pimpl->mat;
}

Image & Image::operator=(const Image & that)
{
	_pimpl->mat = that._pimpl->mat;
	return *this;
}

Image::~Image()
{
	delete _pimpl;
}

const uint8_t * Image::data() const
{
	return static_cast<const unsigned char *>(_pimpl->mat.data);
}

std::size_t Image::width() const
{
	return static_cast<std::size_t>(_pimpl->mat.cols);
}

std::size_t Image::height() const
{
	return static_cast<std::size_t>(_pimpl->mat.rows);
}

PixelFormat Image::pixel_format() const
{
	switch (_pimpl->mat.type())
	{
	case CV_8UC1:
		return PF_8U_1C;
	case CV_8UC2:
		return PF_8U_2C;
	case CV_8UC3:
		return PF_8U_3C;
	case CV_8UC4:
		return PF_8U_4C;
	default:
		throw std::runtime_error("Bad pixel format!");
	}
	return PixelFormatEnd;
}

std::size_t Image::byte_count() const
{
	return _pimpl->mat.elemSize() * _pimpl->mat.rows * _pimpl->mat.cols;
}

void Image::load(const std::string & path)
{
	_pimpl->mat = cv::imread(path.c_str(), CV_LOAD_IMAGE_COLOR);
}

void Image::save(const std::string & path) const
{
	cv::imwrite(path.c_str(), _pimpl->mat);
}

Image Image::crop(std::size_t x, std::size_t y, std::size_t width, std::size_t height) const
{
	Image result;
	result._pimpl->mat = _pimpl->mat(cv::Rect(x, y, width, height));
	return result;
}

Image Image::flip(int mode) const
{
	Image result;
	result._pimpl->mat = cv::Mat(height(), width(), _pimpl->mat.type());
	cv::transpose(_pimpl->mat, result._pimpl->mat);
	cv::flip(result._pimpl->mat, result._pimpl->mat, mode);
	return result;
}

void Image::resize(std::size_t width, std::size_t height)
{
	cv::resize(_pimpl->mat, _pimpl->mat, cv::Size(width, height));
}

Image & Image::gray()
{
	if (_pimpl->mat.type() != PF_8U_1C)
	{
		cv::cvtColor(_pimpl->mat, _pimpl->mat, CV_BGR2GRAY);
	}
	return *this;
}
