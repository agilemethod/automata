cmake_minimum_required (VERSION 2.6)

file(GLOB HEADER_FILES ${CMAKE_SOURCE_DIR}/include/matashift/*.h*)
add_library(matashift gameplayer.cpp ${HEADER_FILES})
