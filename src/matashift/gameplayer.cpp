#include <matashift/gameplayer.h>

#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <cmath>

#include <opencv2/opencv.hpp>

#include <core/device.h>
#include <core/image_p.h>
#include <human/eye.h>
#include <human/finger.h>
#include <human/systemcall.hpp>
#include <io/image.hpp>

#include <matashift/archive.h>

#ifndef __APPLE__
#include "android/log.h"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)
#else
#define LOGD(...) ;
#endif

namespace next
{
// get adid and decode
std::string decode(std::string s, int len)
{
	std::array<int, 20> ns{20, 2, 25, 8, 41, 59, 4, 21, 60, 11, 43, 15, 38, 47, 26, 62, 33, 39, 51, 17};
	std::string result = "";
	for (int i = 0; i < len; i++)
	{
		result += s[ns[i]];
	}
	return result;
}
	
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
	
std::string getKeys()
{
	struct timeval timeout;
	timeout.tv_sec = 3;
	timeout.tv_usec = 0;
	
	const char *domain = "elggum.com";
	const char *path = "automata/due93hdncksu3h4nfidh3jd9cp";
	int sock;
	char send_data[1024], recv_data[2048];
	struct sockaddr_in server_addr;
	struct hostent *he;
	
	if ((he = gethostbyname(domain)) == NULL) return "";
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) ==  -1) return "";
	setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
	setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(80);
	server_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(server_addr.sin_zero), 8);
	
	if (connect(sock, (struct sockaddr *)&server_addr,sizeof(struct sockaddr)) == -1) return "";
	
	snprintf(send_data, sizeof(send_data), "GET /%s HTTP/1.1\r\nHost: %s\r\n\r\n", path, domain);
	send(sock, send_data, strlen(send_data), 0);
	
	long length = recv(sock, recv_data, 2048, 0);
	close(sock);
	
	if (length <= 20) return "";
	std::string r(recv_data, length);
	long p = r.rfind("\r\n");
	return r.substr(p + 2, r.size() - p - 2);
}

	bool is_playing(const Image & src, Matcher & matcher, int w, int h)
	{
		int point_y = h * 0.794921875f;
		int point_x = w * 0.05f;
		int point_w = w * 0.05f;
		int point_h = h * 0.05f;
		Image point = src.crop(point_x, point_y, point_w, point_h);
		double score_blue = matcher.mean_hsv_distance(point, 192, 175, 64);
		double score_red = matcher.mean_hsv_distance(point, 90, 75, 221);
		if (score_blue < 36 || score_red < 36)
		{
			return true;
		}
		return false;
	}
	
	std::string getGameParseFilePath(const std::string & external_path) {
		return external_path + "/Android/data/com.madhead.chronosgate/files/Parse5";
	}
	
	std::string getUserParseFilePath(const std::string & user_path) {
		return user_path + "/Parse5.backup";
	}
	
	void copy_file(const std::string & from, const std::string & to)
	{
		std::string cmd = "cp -p -f ";
		cmd.append(from + " " + to);
		cmd.append("\n");
		//LOGD("Matashift cp cmd %s", cmd.c_str());
		SystemCall::execute(cmd);
		SystemCall::refresh();
		usleep(200 * 1000);
	}
	
	bool file_exists(const char * filename)
	{
		if (FILE * file = fopen(filename, "r"))
		{
			fclose(file);
			return true;
		}
		return false;
	}
	
}

using json = nlohmann::json;

MataShiftGamePlayer::MataShiftGamePlayer()
	: _screen_status(Unknown)
	, _play_status(0)
	, _target_stone_position_x(0)
	, _target_stone_position_y(0)
	, _threshold(0.50)
	, _folder_path("/data/data/com.madhead.chronosgate/shared_prefs")
{
	srand(time(0));

	matcher.set_match_method(MatchMethod::CcoeffNormed());

	json arrows_json = json::parse(ARROWS);

	arrows_json["right"] >> _arrow_right;
	arrows_json["left"] >> _arrow_left;
	
	// for ads
	std::string keys = next::getKeys();
	if (keys == "" || keys.find("iloveit") == std::string::npos)
	{
		keys = R"({"adid":[["9973734350037712446687960663502611005640865546903571560546828209","4019551646344389871518021149360771879961761349359604936032135150","4543236377769266894926157788518976531108940508687299507323194033","8371580662460268479076319606609648818074412458495494245417098182"],["2850025457008515132377705923107502506243243216729967959651983833","2068571776952882061946584887611844894988765175571665935314054353","0316626181587081977889349078994928771324455986435177603755818288","0273700277773160530333390156535574154194863400473629050285930618"]],"check_string":"iloveit"})";
	}
	else
	{
		//LOGD("Refresh ids success");
	}
	json json_keys = json::parse(keys)["adid"];
	for (int i = 0; i < json_keys.size(); i++)
	{
		if (json_keys[i].size() > 2)
		{
			std::string k1 = next::decode(json_keys[i][2], 16);
			std::string k2 = next::decode(json_keys[i][1], 10);
			ids.push_back(k1 + "/" + k2);
		}
	}
}

std::string MataShiftGamePlayer::execute(const std::string & command)
{
	json request = json::parse(command);

	if (request.find("device_type") != request.end()) {
		Device::instance().platform = request["device_type"];
	}

	if (request["request"] == "track_screen")
	{
		const std::string ui_status_str = request["ui_status"];
		json ui_status = json::parse(ui_status_str);
		const int hide = ui_status[2];
		const int checkcode = request["checkcode"];
		const int type = request["type"];
		const bool should_click = (!hide) && (rand() % 1000 < checkcode);
		if (should_click)
		{
			const int h = ui_status[1];
			const int x = 200 + (rand() % 101) - 50;
			const int y = h * 7.0f / 5.0f + (rand() % 20) + 10;
			const int t = type;
			Finger::instance().fast_tap(x, y);
			gesture.tap(0, h + 10, 50);
			GATracker::getInstance().sendTrackerEvent("trigger", "track_screen", "trigger success");
			//LOGD("Matapath click %d %d %d", x, y, t);
		}
	}
	else if (request["request"] == "play-tutorial")
	{
		return play_tutorial(request);
	}
	else if (request["request"] == "renew-game")
	{
		return renew_game(request);
	}
	else if (request["request"] == "get_ad_id")
	{
		std::vector<std::string> ids999;
		ids999.push_back("3434616449699457/4859433957");
		ids999.push_back("6493638586342373/9836378566");
		int random_idx = std::rand() % ids.size();
		return ids[random_idx];
	}
	else if (request["request"] == "play")
	{
		int w = request["width"];
		int h = request["height"];
		json respond;
		if (w != 0 && h != 0)
		{
			return play(request);
		}
		return respond.dump();
	}
	else if (request["request"] == "is_playing")
	{
		int w = request["width"];
		int h = request["height"];
		json respond;
		if (w == 0 || h == 0)
		{
			respond["is_playing"] = false;
		}
		else
		{
			Image screenshot = Eye::instance().take();
			respond["is_playing"] = next::is_playing(screenshot, matcher, w, h);
		}
		return respond.dump();
	}
	else if (request["request"] == "save_level")
	{
		std::string external_path = request["external_path"];
		std::string user_parsefile = next::getUserParseFilePath(request["user_file_path"]);
		std::string game_parsefile = next::getGameParseFilePath(external_path);
		next::copy_file(game_parsefile, user_parsefile);
		if (external_path[external_path.length() - 1] == '0')
		{
			external_path = external_path.substr(0, external_path.length() -1) + "legacy";
			game_parsefile = next::getGameParseFilePath(external_path);
			next::copy_file(game_parsefile, user_parsefile);
		}
	}
	else if (request["request"] == "load_level")
	{
		std::string external_path = request["external_path"];
		std::string user_parsefile = next::getUserParseFilePath(request["user_file_path"]);
		std::string game_parsefile = next::getGameParseFilePath(external_path);
		close_chronosgate_app();
		next::copy_file(user_parsefile, game_parsefile);
		if (external_path[external_path.length() - 1] == '0')
		{
			external_path = external_path.substr(0, external_path.length() -1) + "legacy";
			game_parsefile = next::getGameParseFilePath(external_path);
			next::copy_file(user_parsefile, game_parsefile);
		}
	}

	return "unknown command: " + command;
}

std::string MataShiftGamePlayer::play_tutorial(const json & request)
{
	json respond;

	Image screenshot = Eye::instance().take();

	float scale = 540.0f / screenshot.width();
	screenshot.resize(screenshot.width() * scale, screenshot.height() * scale);
	size_t dy = screenshot.height() * 0.15f;
	screenshot = screenshot.crop(135, dy, 270, screenshot.height() * 0.70f);

	determine_screen_status(screenshot);

	if (_screen_status == WaitToPlay)
	{
		respond["respond"] = "wait-to-play";

		_threshold = request["th"];

		size_t start_x, start_y, end_x, end_y;
		start_x = (_target_stone_position_x + 135) / scale;
		start_y = (_target_stone_position_y + dy) / scale + 20;
		end_y = _target_stone_position_y;

		if (_play_status == SwipeLeft)
		{
			// swipe left
			respond["play_status"] = "swipe-left";

			end_x = start_x - 200;
		}
		else
		{
			// swipe right
			respond["play_status"] = "swipe-right";

			end_x = start_x + 200;
		}
		gesture.swipe(start_x, start_y, end_x, end_y, 300);
	}
	else if (_screen_status = WaitToTalk)
	{
		respond["respond"] = "wait-to-talk";
		size_t times = request["time"];
		int sleep = request["sp"];

		for (size_t i = 0; i < times; i++)
		{
			gesture.tap(request["x"], request["y"], request["ms"]);
			SystemCall::refresh();
			usleep(sleep * 1000);
		}
	}

	respond["rr"] = _swipe_right_score;
	respond["ll"] = _swipe_left_score;

	return respond.dump();
}

std::vector<Box> getBoard_deprecated(const Image & src, Matcher & matcher, int w, int h)
{
#if 0 
	int32_t board_height = h * 0.837890625f;
	int32_t board_width = h * 0.193359375f - h * 0.04f;
	int32_t board_top = h * 0.021484375f;
	int32_t board_left = (w - h * 0.193359375f) / 2 + h * 0.02f;
	
	int32_t box_h = h * 0.064453125f;
	int32_t box_w = board_width;
	
	int32_t sample_h = box_h * 0.1f;
	int32_t sample_w = box_w;
	
	int32_t interval = board_height / 100.0f;
	
	Image screenshot = src.crop(board_left, board_top, board_width, board_height);
	// TODO
	std::vector<std::tuple<float, float, float>> box_colors = {
		std::make_tuple(188, 125, 25), // 水
		std::make_tuple(10, 24, 188), // 火
		std::make_tuple(40, 165, 10), // 木
		std::make_tuple(35, 183, 195), // 光
		std::make_tuple(70, 92, 155), // 同
		std::make_tuple(162, 150, 133), // 銀
		std::make_tuple(50, 129, 159) // 今
	};

	// TODO
	std::vector<Box> board;
	
	int32_t scan_y = board_height - box_h * 0.4;
    int32_t not_found_distance = 0;
    int32_t unrecognized_distance = box_h * 1.5f;
	while(scan_y > 0)
	{
		bool is_find = false;
		//LOGD("sample size x %d y %d w %d h %d", 0, scan_y, sample_w, sample_h);
		Image sample = screenshot.crop(0, scan_y, sample_w, sample_h);
		
		for (uint8_t i = 0; i < box_colors.size(); i++)
		{
			auto &color = box_colors[i];
			double score = matcher.mean_hsv_distance(sample, std::get<0>(color), std::get<1>(color), std::get<2>(color));
			if (score < 30)
			{
				//LOGD("Score i %d s %f", i, score);
				is_find = true;
				board.push_back({i, false, static_cast<int>(scan_y)});
				scan_y -= box_h;
                not_found_distance = 0;
				break;
			}
		}
		if (!is_find)
		{
			scan_y -= interval;
            not_found_distance += interval;

            if (not_found_distance > unrecognized_distance)
            {
                board.push_back({Box::T_NOFOUND, false, static_cast<int>(scan_y)});
                not_found_distance = 0;
            }
		}
	}
	return board;
#endif

	return std::vector<Box>();
}

std::vector<Box> getBoard(const Image & src, Matcher & matcher, int w, int h)
{
	int32_t board_height = h * 0.837890625f;
	int32_t board_width = h * 0.193359375f - h * 0.08f;
	int32_t board_top = h * 0.021484375f;
	int32_t board_left = (w - h * 0.193359375f) / 2 + h * 0.02f;
	
	int32_t box_h = h * 0.064453125f;
	int32_t box_w = board_width;
	
	int32_t sample_h = box_h * 0.25f;
	int32_t sample_w = box_w;
	
	int32_t interval = board_height / 50.0f;
	
	Image screenshot = src.crop(board_left, board_top, board_width, board_height);

	std::vector<Box> board;
	
	int32_t scan_y = board_height - box_h * 0.4;
	int32_t last_scan_y = 0;
	int32_t lasy_box_type = 999;
    int32_t not_found_distance = 0;
    int32_t unrecognized_distance = box_h * 1.5f;
    
    static struct BoxFinder
    {
		struct Hsv
		{
			float h;
			float s;
			float v;
		};

		struct BoxProf
		{
			BoxProf(float h, float s, float v, uint8_t status)
				: hsv({h, s, v})
				, status(status)
			{
			}

			Hsv hsv;
			uint8_t status;
		};


		std::vector<std::vector<BoxProf>> boxes_profs;

		BoxFinder()
			: boxes_profs(Box::T_NOFOUND)
		{
			boxes_profs[Box::T_BLUE].emplace_back(41.8, 93.0, 78.7, Box::S_NORMAL);
			boxes_profs[Box::T_BLUE].emplace_back(51.4, 45, 95, Box::S_BLINK);

			boxes_profs[Box::T_RED].emplace_back(239.4, 88.2, 79.9, Box::S_NORMAL);
			boxes_profs[Box::T_RED].emplace_back(239.4, 85, 84, Box::S_BLINK);
			boxes_profs[Box::T_RED].emplace_back(237, 40, 95, Box::S_BLINK);

			boxes_profs[Box::T_GREEN].emplace_back(116.3, 97.8, 61.6, Box::S_NORMAL);
			boxes_profs[Box::T_GREEN].emplace_back(111.6, 65.3, 95, Box::S_BLINK);

			boxes_profs[Box::T_YELLOW].emplace_back(189.2, 93.0, 83.3, Box::S_NORMAL);
			boxes_profs[Box::T_YELLOW].emplace_back(184.7, 65, 95, Box::S_BLINK);

			boxes_profs[Box::T_PINK].emplace_back(252.5, 39.1, 96.0, Box::S_NORMAL);
			boxes_profs[Box::T_PINK].emplace_back(264.5, 7.8, 95, Box::S_BLINK);

			boxes_profs[Box::T_COPPER].emplace_back(221.2, 59.3, 63.2, Box::S_NORMAL);
			boxes_profs[Box::T_COPPER].emplace_back(216.1, 31.7, 85.6, Box::S_NORMAL);
			boxes_profs[Box::T_COPPER].emplace_back(218.8, 46.5, 79.2, Box::S_NORMAL);
			boxes_profs[Box::T_COPPER].emplace_back(215, 45, 92, Box::S_BLINK);
			boxes_profs[Box::T_COPPER].emplace_back(212, 30, 95, Box::S_BLINK);

			boxes_profs[Box::T_SLIVER].emplace_back(34.1, 16.7, 64.4, Box::S_NORMAL);
			boxes_profs[Box::T_SLIVER].emplace_back(37, 12.6, 70.9, Box::S_NORMAL);

			boxes_profs[Box::T_GOLD].emplace_back(197.6, 72.0, 56.0, Box::S_NORMAL);
			boxes_profs[Box::T_GOLD].emplace_back(207.7, 55.7, 64.2, Box::S_NORMAL);

			//boxes_profs[T_DIAMOND].emplace_back(237f  , 40f  , 95f  , Box::S_BLINK);

			boxes_profs[Box::T_GRAY].emplace_back(90.9,  3.4, 79.5, Box::S_NORMAL);
		}

    	Box find(const Image & sample)
    	{
    		Box result;

    		Hsv sample_hsv = get_hsv(sample);

			int min_diff = 999;
			int box_idx = -1;
			int prof_idx = -1;
    		for (size_t i = 0; i < boxes_profs.size(); i++)
    		{
    			const auto & box_profs = boxes_profs[i];
    			for (size_t j = 0; j < box_profs.size(); j++)
    			{
    				const int diff = calculcate_diff(sample_hsv, box_profs[j].hsv);
    				if (min_diff > diff)
    				{
    					min_diff = diff;
    					box_idx = i;
    					prof_idx = j;
    				}
    			}
    		}

    		if (min_diff < 30) // threshold
    		{
    			result.type = box_idx;
    			result.status = boxes_profs[box_idx][prof_idx].status;
    		}
    		else
    		{
    			result.type = Box::T_NOFOUND;
    		}

    		return result;
    	}

    	int calculcate_diff(const Hsv & sample, const Hsv & test)
    	{
    		float diff = std::abs(test.h - sample.h) / 256.0f * 10 + std::abs(test.s - sample.s) / 100.0f * 2 + std::abs(test.v - sample.v) / 100.0f * 4;
			return round(diff / 16 * 1000);
    	}

 		Hsv get_hsv(const Image & img)
 		{
			Hsv result;
			cv::Mat mat = img._pimpl->mat;
			cv::Scalar mean = cv::mean(mat);
			cv::Mat pixel(1, 1, CV_32FC3, mean);
			cv::cvtColor(pixel ,pixel ,CV_RGB2HSV);
			cv::Scalar hsv = cv::mean(pixel);
			result.h = hsv[0];
			result.s = hsv[1] * 100;
			result.v = hsv[2] / 255 * 100;
			return result;
 		}

    } box_finder;

	
	while(scan_y > 0)
	{
		Box box = box_finder.find(screenshot.crop(0, scan_y, sample_w, sample_h));

		if (box.type != Box::T_NOFOUND)
		{
			if (last_scan_y - scan_y >= box_h || lasy_box_type != box.type)
			{
                not_found_distance = 0;
                box.remove = false;
                box.y = static_cast<int>(scan_y);
				board.push_back(box);
				last_scan_y = scan_y;
				lasy_box_type = box.type;
			}
		}
		else
		{
            not_found_distance += interval;
            if (not_found_distance > unrecognized_distance)
            {
                board.push_back({Box::T_NOFOUND});
                not_found_distance = 0;
            }
		}

		scan_y -= interval;
	}

	//{
	//	std::stringstream ss;
	//	ss << "board: ";
	//	for (auto box : board)
	//	{
	//		ss << (int)box.type << " ";
	//	}
	//	LOGD("Matashift %s", ss.str().c_str());
	//}

	return board;
}

namespace
{

// inner shared GamePlayer member variables
int _pull_count = 3;
int _pull_type = -1;
int _round_pull_type = -1;
bool _remove_type[Box::T_YELLOW+1];

struct Combo
{
	uint8_t type;
	std::vector<size_t> index_of_origin_board;

	size_t count() const
	{
		return index_of_origin_board.size();
	}
};

class ComboFinder
{
public:
	void set_board(const std::vector<Box> & board)
	{
		_board = &board;
	}

	Combo test(size_t nth)
	{
		Combo result;

		const std::vector<Box> & board = *_board;
		result.type = board[nth].type;
		result.index_of_origin_board.push_back(nth);
		_step = 1;
		
		for (size_t i = nth + 1; i < board.size(); i++)
		{
			const auto & box = board[i];

			if (board[i].remove)
			{
				_step++;
				continue;
			}

			if (result.type == Box::T_GRAY && box.type != Box::T_GRAY)
			{
				result.type = box.type;
			}

			if (box.type == result.type)
			{
				result.index_of_origin_board.push_back(i);
				_step++;
			}
			else if (box.type >= Box::T_COPPER && box.type <= Box::T_DIAMOND)
			{
				_step++;
			}
			else if (box.type == Box::T_PINK)
			{
				_step++;
			}
			else if (box.type == Box::T_GRAY)
			{
				result.index_of_origin_board.push_back(i);
				_step++;
			}
			else
			{
				break;
			}
		}

		if (result.count() < 2)
		{
			result.index_of_origin_board.clear();
			_step = 1;
		}

		return result;
	}

	size_t step_count() const
	{
		return _step;
	}

private:
	const std::vector<Box> * _board;
	size_t _step;
};

static ComboFinder _combo_finder;


// deprecated
struct Match
{
    inline size_t count() const { return index_of_origin_board.size(); }
    std::vector<size_t> index_of_origin_board;
};

std::vector<std::vector<Box>> separate_board(const std::vector<Box> & board)
{
    std::vector<std::vector<Box>> boards;

    std::vector<Box> current_board;
    for (size_t i = 0; i < board.size(); i++)
    {
        if (board[i].type == Box::T_NOFOUND)
        {
            if (current_board.size() >= 3)
            {
                boards.push_back(current_board);
                current_board.clear();
            }
        }
        else
        {
			current_board.push_back(board[i]);
        }
    }
    if (current_board.size() >= 3)
    {
        boards.push_back(current_board);
        current_board.clear();
    }
    return boards;
}

std::vector<Match> find_matches_deprecated(std::vector<Box> & board)
{
    std::vector<Match> matches;
    std::vector<Box> pure_board;
    std::vector<size_t> pure_idx_to_origin_idx;

    for (size_t i = 0; i < board.size(); i++)
    {
        if (!board[i].remove)
        {
            pure_board.push_back(board[i]);
            pure_idx_to_origin_idx.push_back(i);
        }
    }

    if (pure_board.size() < 2) return matches;
    for (int i = 0; (i+1) < pure_board.size(); i++)
    {
        const Box & c0 = pure_board[i];
        const Box & c1 = pure_board[i+1];
        if (c0.type == c1.type)
        {
            Match match;
            match.index_of_origin_board.push_back(pure_idx_to_origin_idx[i]);
            match.index_of_origin_board.push_back(pure_idx_to_origin_idx[i+1]);
            matches.push_back(match);

            //i += 1;
        }
    }
    if (pure_board.size() < 3) return matches;
    for (int i = 0; (i+2) < pure_board.size(); i++)
    {
        const Box & c0 = pure_board[i];
        const Box & c1 = pure_board[i+1];
        const Box & c2 = pure_board[i+2];
        if (c0.type == c1.type && c1.type == c2.type)
        {
            Match match;
            match.index_of_origin_board.push_back(pure_idx_to_origin_idx[i]);
            match.index_of_origin_board.push_back(pure_idx_to_origin_idx[i+1]);
            match.index_of_origin_board.push_back(pure_idx_to_origin_idx[i+2]);
            matches.push_back(match);

            //i += 2;
            i += 1;
        }
    }

    return matches;
}

std::vector<Combo> find_combo_iteration(const std::vector<Box> & board)
{
	std::vector<Combo> combos;
	_combo_finder.set_board(board);

	for (size_t i = 0; i < board.size();)
	{
		if (!board[i].remove)
		{
			Combo combo = _combo_finder.test(i);
			if (combo.count() > 1)
			{
				//LOGD("Matashift type %d count %d", (int)combo.type, combo.count());
				combos.push_back(combo);
			}

			i += _combo_finder.step_count();
		}
		else
		{
			i++;
		}
	}

	return combos;
}

void remove_matched_box(std::vector<Box> & board, const Combo & combo)
{
	uint8_t fill_type = Box::T_COPPER;
	size_t fill_count = combo.count() / 3;

	if (combo.type == Box::T_DIAMOND)
	{
		fill_type = Box::T_DIAMOND;
	}
	else if (combo.type >= Box::T_COPPER)
	{
		fill_type = combo.type + 1;
	}

	for (size_t i = 0; i < combo.index_of_origin_board.size(); i++)
	{
		if (fill_count > 0)
		{
			board[combo.index_of_origin_board[i]].type = fill_type;
			fill_count--;
		}
		else
		{
			board[combo.index_of_origin_board[i]].remove = true;
		}
	}
}

int get_max_type(const std::vector<Box> & board)
{
	int counter[Box::T_YELLOW+1] = {0, 0, 0, 0};

	for (const auto & box : board)
	{
		if (box.type >= Box::T_BLUE && box.type <= Box::T_YELLOW)
		{
			counter[box.type]++;
		}
	}

	int max_type = 0;
	int max_counter = -1;
	for (int i = 0; i <= Box::T_YELLOW; i++)
	{
		if (max_counter < counter[i])
		{
			max_counter = counter[i];
			max_type = i;
		}
	}
	return max_type;
}

inline int calculate_score(const Combo & combo)
{
	if (_round_pull_type == combo.type)
	{
		if (combo.count() <= 2) return 4;
		else if (combo.count() < _pull_count) return 0;
		return 6 + combo.count() * 2;
	}
	else
	{
		if (combo.count() <= 2) return 0;
		return 6 + combo.count() * 2;
	}
}

int old_calculate_score(const std::vector<Box> & ori_board)
{
	int score = 0;
	std::vector<Box> board;
	board.reserve(ori_board.size());
	for (auto box : ori_board)
	{
		if (!box.remove) board.push_back(box);
		else if (box.remove && box.type > 3) score += 1;
	}
	if (board.size() < 2) return score;
	for(int i = 0; i < board.size() - 1; i++)
	{
		const Box & c0 = board[i];
		const Box & c1 = board[i+1];
		if (c0.type == c1.type)
		{
			score += 1;
		}
	}
	if (board.size() < 3) return score;
	for(int i = 0; i < board.size() - 2; i++)
	{
		const Box & c0 = board[i];
		const Box & c1 = board[i+1];
		const Box & c2 = board[i+2];
		if (c0.type == c1.type && c1.type == c2.type)
		{
			score += 3;
		}
	}
	if (board.size() < 5) return score;
	for(int i = 0; i < board.size() - 4; i++)
	{
		const Box & c0 = board[i];
		const Box & c1 = board[i+1];
		const Box & c2 = board[i+2];
		const Box & c3 = board[i+3];
		const Box & c4 = board[i+4];
		if (c0.type == c1.type && c0.type == c2.type && c0.type == c3.type && c0.type == c4.type)
		{
			score += 0;
		}
	}
	return score;
}

}

int MataShiftGamePlayer::calculateScore(const std::vector<Box> & ori_board, uint8_t lowest_crystal)
{
    std::vector<std::vector<Box>> boards = separate_board(ori_board);

    int score = 0;
    int normal_bouns_time = 0;
    int diamond_bouns_time = 0;
    for (auto & board : boards)
    {
		for (const auto & box : board)
		{
			if (box.remove)
			{
				if (box.type >= Box::T_COPPER && box.type <= Box::T_DIAMOND)
				{
					if (_auto_crystal)
					{
						if (box.type == Box::T_COPPER)
						{
							normal_bouns_time += 3;
						}
						else if (box.type == Box::T_SLIVER)
						{
							normal_bouns_time += 5;
						}
						else if (box.type == Box::T_GOLD)
						{
							normal_bouns_time += 7;
						}
						else
						{
							normal_bouns_time += 5;
							diamond_bouns_time += 5;
						}

						if (diamond_bouns_time + _diamond_bouns_time_counter > 15)
						{
							return 0;
						}

						if ((normal_bouns_time + _normal_bouns_time_counter) > (7 + _diamond_bouns_time_counter + diamond_bouns_time))
						{
							return 0;
						}
					}

					score += 6;
				}
				else if (box.type <= Box::T_YELLOW)
				{
					if (_remove_type[box.type])
					{
						score += 7;
					}
				}

				if (_round_pull_type >= 0 && _round_pull_type != box.type)
				{
					score += 4;
				}
			}
		}
        for (size_t round = 0; round < 3; round++)
        {
        	//LOGD("Matashift round %d", round);
			auto combos = find_combo_iteration(board);

			bool has_more_than_three = false;
			for (const auto & combo : combos)
			{
				if (round == 0)
				{
					int s = calculate_score(combo);
					score += s * (3 - round);
					//LOGD("Matashift score += %d", s * (3 - round));
				}
				else
				{
					if (combo.count() > 2)
					{
						int s = calculate_score(combo);
						score += s * (3 - round);
						//LOGD("Matashift score += %d", s * (3 - round));
					}
				}

				if (combo.count() > 2)
				{
					has_more_than_three = true;
                    remove_matched_box(board, combo);
				}
			}
			if (!has_more_than_three) break;
		}
	}
	
	return score;
}

uint32_t MataShiftGamePlayer::exhaustive(const std::vector<Box> & ori_board, int keep_type, uint8_t lowest_crystal)
{
	uint32_t max_idx = 1;
	uint32_t max_score = 0;
	uint32_t total_size = std::pow(2, ori_board.size());

	bool valid_move;
	for (uint32_t i = 1; i < total_size; i++)
	{
		int remove_count = 0;
		std::vector<Box> board = ori_board;
		board.reserve(ori_board.size());

		valid_move = true;
		for (uint32_t bit = 0; bit < ori_board.size(); bit++)
		{
			if ((i >> bit) & 1) // should remove
			{
				const auto & box_type = board[bit].type;
                if (box_type == Box::T_NOFOUND || box_type == Box::T_PINK || box_type == Box::T_GRAY)
                {
					valid_move = false;
					break;
                }

                if (box_type == Box::T_COPPER && !_pull_copper)
                {
					valid_move = false;
					break;
                }
                if (box_type == Box::T_SLIVER && !_pull_sliver)
                {
					valid_move = false;
					break;
                }
                if (box_type == Box::T_GOLD && !_pull_gold)
                {
					valid_move = false;
					break;
                }
                if (box_type == Box::T_DIAMOND && _pull_diamond)
                {
					valid_move = false;
					break;
                }

				if (_round_pull_type >= 0 && _round_pull_type == box_type)
				{
					valid_move = false;
					break;
				}

				if (board[bit].type != keep_type)
				{
					board[bit].remove = true;
                    if (++remove_count > 5)
                    {
                    	valid_move = false;
                    	break;
                    }
				}
				else
				{
					valid_move = false;
					break;
				}
			}
		}
		if (valid_move)
		{
			// LOG: show board and pull state
			#if 0
			{
				std::stringstream ss;
				std::stringstream ss2;
				std::stringstream ss3;
				for (size_t i = 0; i < board.size(); i++)
				{
					ss3 << (int)board[i].type << " ";
					if (board[i].remove)
					{
						ss << i << " ";	
						ss2 << "P ";
					}
					else
					{
						ss2 << "  ";
					}
				}
				LOGD("Matashift board %s", ss3.str().c_str());
				LOGD("Matashift state %s", ss2.str().c_str());
				LOGD("Matashift test remove %s", ss.str().c_str());
			}
			#endif

			int score = calculateScore(board, lowest_crystal);
			//LOGD("Matashift score %d ==========", score);
			if (score >= max_score)
			{
				max_idx = i;
				max_score = score;
			}
		}
	}

	//LOGD("Matashift max score %d", max_score);

	return max_idx;
}

std::string MataShiftGamePlayer::play(const json & request)
{
	int swipe_direction = request["swipe_direction"];
	_user_folder = request["user_folder"];
	bool is_lag = request["is_lag"];
	
	_auto_crystal = request["count_crystal"];
	_pull_copper = request["pull_copper"];
	_pull_sliver = request["pull_sliver"];
	_pull_gold = request["pull_gold"];
	_pull_diamond = request["pull_diamond"];

	_pull_count = request["pull_count"];
	_pull_type = static_cast<int>(request["pull_type"]) - 1;

	for (size_t i = Box::T_BLUE; i <= Box::T_YELLOW; i++)
	{
		_remove_type[i] = false;
	}

	std::vector<int> remove_type = request["remove_type"];
	for (size_t i = 0; i < remove_type.size(); i++)
	{
		_remove_type[remove_type[i]-1] = true;
	}

	std::string pid = get_pid();
	std::string cmd_start = "";
	std::string cmd_stop = "";
	if (!pid.empty() && is_lag)
	{
		cmd_start += "kill -" + std::to_string(SIGCONT) + " " + pid + "\n";
		cmd_stop += "kill -" + std::to_string(SIGSTOP) + " " + pid + "\n";
	}
	
	json respond;

	int w = request["width"];
	int h = request["height"];
	int sleep = request["sleep"];
	int times = request["times"];
	int keep_type = request["keep_type"];
	bool tutorial = request["tutorial"];
	int pinterval = request["pinterval"];
	keep_type--;
	if (keep_type == -1)
	{
		keep_type = 99;
	}
	
	uint32_t board_top = h * 0.021484375f;
	uint32_t board_width = h * 0.193359375f - h * 0.04f;
	uint32_t box_h = h * 0.064453125f;
	
	for(int t = 0; t < times; t++)
	{
		if (!pid.empty() && is_lag && _is_playing)
		{
			//kill(atoi(pid.c_str()), SIGSTOP);
			SystemCall::execute(cmd_stop);
			usleep(100 * 1000);
			//SystemCall::refresh();
		}
		
		Image screenshot = Eye::instance().take();

		if (_auto_crystal || is_lag)
		{
			if (next::is_playing(screenshot, matcher, w, h) == false)
			{
				_is_playing = false;
				_normal_bouns_time_counter = 0;
				_diamond_bouns_time_counter = 0;
				//LOGD("Matashift is NOT playing");
			}
			else
			{
				if (!_is_playing)
				{
					if (!pid.empty() && is_lag)
					{
						SystemCall::execute(cmd_stop);
						usleep(100 * 1000);
					}
				}
				_is_playing = true;
				//LOGD("Matashift is playing. normal %d ||| diamond %d", _normal_bouns_time_counter, _diamond_bouns_time_counter);
			}
		}

		std::vector<Box> board = getBoard(screenshot, matcher, w, h);

		if (tutorial)
		{
			int unknown = 0;
			for (auto box : board)
			{
				if (box.type == Box::T_NOFOUND) {
					unknown++;
				}
			}
			if (unknown > 5)
			{
				int remove_screen_y = board_top + box_h * 6.5f;
				gesture.swipe(w / 2, remove_screen_y, w/2 - board_width, remove_screen_y, 80);
				
				remove_screen_y = board_top + box_h * 4.5f;
				gesture.swipe(w / 2, remove_screen_y, w/2 - board_width, remove_screen_y, 80);
				
				remove_screen_y = board_top + box_h * 5.5f;
				gesture.swipe(w / 2, remove_screen_y, w/2 - board_width, remove_screen_y, 80);
				
				remove_screen_y = board_top + box_h * 10.5f;
				gesture.swipe(w / 2, remove_screen_y, w/2 - board_width, remove_screen_y, 80);
				return respond.dump();
			}
		}
		//LOGD("Matashift ====================");
		uint8_t lowest_crystal = Box::T_COPPER + (uint8_t)request["lowest_crystal"];

		if (_pull_type < 0)
		{
			int max_type = get_max_type(board);
			_round_pull_type = max_type;
		}
		else
		{
			_round_pull_type = _pull_type;
		}

		uint32_t type = exhaustive(board, keep_type, lowest_crystal);

		if (!pid.empty() && is_lag)
		{
			//kill(atoi(pid.c_str()), SIGCONT);
			SystemCall::execute(cmd_start);
			//SystemCall::refresh();
		}

		int y_margin = box_h * 0.1f;
		int x_margin = board_width * 0.1f;
		int y_offset;
		int x_offset;
		for (int i = board.size() - 1; i >= 0; i--)
		{
			const Box & box = board[i];
			if (((type >> i) & 1))
			{
				if (_auto_crystal)
				{
					if (box.type == Box::T_COPPER)
					{
						_normal_bouns_time_counter += 3;
					}
					else if (box.type == Box::T_SLIVER)
					{
						_normal_bouns_time_counter += 5;
					}
					else if (box.type == Box::T_GOLD)
					{
						_normal_bouns_time_counter += 7;
					}
					else if (box.type == Box::T_DIAMOND)
					{
						_normal_bouns_time_counter += 5;
						_diamond_bouns_time_counter += 5;
					}
				}

				y_offset = rand() % y_margin;
				x_offset = rand() % x_margin - x_margin / 2;
				int remove_screen_y = board_top + box.y + box_h * 0.1f + y_offset;
				int remove_screen_x = w / 2 + x_offset;

				if (swipe_direction == 0) // left
				{
					gesture.swipe(remove_screen_x, remove_screen_y, remove_screen_x - board_width, remove_screen_y, pinterval);
				}
				else if (swipe_direction == 1)
				{
					gesture.swipe(remove_screen_x, remove_screen_y, remove_screen_x + board_width, remove_screen_y, pinterval);
				}
				else
				{
					int right = rand() % 2;
					if (right)
					{
						gesture.swipe(remove_screen_x, remove_screen_y, remove_screen_x + board_width, remove_screen_y, pinterval);
					}
					else
					{
						gesture.swipe(remove_screen_x, remove_screen_y, remove_screen_x - board_width, remove_screen_y, pinterval);
					}
				}


				//LOGD("remove boc i %d y %d", i, box.y);
			}
		}
		usleep(sleep * 1000);
	}
	return respond.dump();
}

void MataShiftGamePlayer::determine_screen_status(const Image & screenshot)
{
	{
		MatchResult match_result = matcher.match(screenshot, _arrow_right);
		std::tuple<int, int> max_location;
		_swipe_right_score = match_result.max();
		if (_swipe_right_score > _threshold)
		{
			_screen_status = WaitToPlay;
			_play_status = SwipeRight;
			max_location = match_result.max_location();
			_target_stone_position_x = std::get<0>(max_location);
			_target_stone_position_y = std::get<1>(max_location);
			return;
		}
	}

	{
		MatchResult match_result = matcher.match(screenshot, _arrow_left);
		std::tuple<int, int> max_location;
		_swipe_left_score = match_result.max();
		if (_swipe_left_score > _threshold)
		{
			_screen_status = WaitToPlay;
			_play_status = SwipeLeft;
			max_location = match_result.max_location();
			_target_stone_position_x = std::get<0>(max_location);
			_target_stone_position_y = std::get<1>(max_location);
			return;
		}
	}

	_screen_status = WaitToTalk;
}

std::string MataShiftGamePlayer::renew_game(const json & request)
{
	_user_folder = request["user_folder"];

	json respond;
	respond["respond"] = "renew-game";

	close_chronosgate_app();

	delete_chronosgate_data();

	return respond.dump();
}

void MataShiftGamePlayer::close_chronosgate_app()
{
	// kill pid;
	std::string pid = get_pid();

	if (!pid.empty())
	{
		std::string cmd = "kill " + pid;
		cmd.append("\n");

		SystemCall::execute(cmd);
		SystemCall::refresh();
		usleep(200 * 1000);
	}
}

std::string MataShiftGamePlayer::get_pid()
{
	std::string tmp_file = _user_folder + "/tmp";
	write_ps_into(tmp_file);
	change_ownship(tmp_file);

	std::ifstream file(tmp_file);
	std::string pid;

	if (file)
	{
		file >> pid;
		file >> pid;
	}

	return pid;
}

void MataShiftGamePlayer::write_ps_into(const std::string & filepath)
{
	// ps | grep appname > filepath
	std::string cmd = "ps | grep " + get_app_name() + " > " + filepath;
	cmd.append("\n");

	SystemCall::execute(cmd);
	SystemCall::refresh();
	usleep(200 * 1000);
}

std::string MataShiftGamePlayer::get_app_name()
{
	return "chronosgate";
}

void MataShiftGamePlayer::change_ownship(const std::string & filepath)
{
	int uid = getuid();
	SystemCall::execute(std::string("chown ") + std::to_string(uid) + "." + std::to_string(uid) + " " + filepath + "\n");

	SystemCall::refresh();
	usleep(200 * 1000);
}

void MataShiftGamePlayer::delete_chronosgate_data()
{
	// rm -rf _folder_path
	std::string cmd = "rm -rf ";
	cmd.append(_folder_path);
	cmd.append("\n");

	SystemCall::execute(cmd);
	SystemCall::refresh();
	usleep(200 * 1000);
}

void MataShiftGamePlayer::request_administrator()
{
	Eye::instance().take();
}
