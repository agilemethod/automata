#include "gtest/gtest.h"
#include <core/worker.hpp>

TEST(WorkerTest, UpAndDown)
{
	Worker worker;
}

TEST(WorkerTest, Dispatch)
{
	int i = 0;
	auto foo = [&i]() { i++; };
	Worker worker;
	worker.dispatch(foo);
	std::this_thread::sleep_for(std::chrono::milliseconds(500));
	EXPECT_EQ(i, 1);
}

TEST(WorkerTest, ClearAllTasks)
{
	int i = 0;
	auto foo = [&i]() { i++; };
	auto sleep = []()
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1500));
	};
	Worker worker;
	worker.dispatch(foo);
	worker.dispatch(sleep);
	worker.dispatch(foo);
	worker.dispatch(foo);

	std::this_thread::sleep_for(std::chrono::milliseconds(500));
	worker.clear_all_tasks();
	std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	EXPECT_EQ(i, 1);
}

TEST(WorkerTest, StressTest)
{
	int counter = 0;
	std::mutex worker_mutex;
	std::mutex counter_mutex;
	Worker worker;

	auto adder = [&]()
	{
		std::lock_guard<std::mutex> lock(counter_mutex);
		counter++;
	};

	auto minuser = [&]()
	{
		std::lock_guard<std::mutex> lock(counter_mutex);
		counter--;
	};

	auto inc = [&]()
	{
		for (size_t i = 0; i < 10000; i++)
		{
			std::lock_guard<std::mutex> lock(worker_mutex);
			worker.dispatch(adder);
		}
	};

	auto dec = [&]()
	{
		for (size_t i = 0; i < 10000; i++)
		{
			std::lock_guard<std::mutex> lock(worker_mutex);
			worker.dispatch(minuser);
		}
	};

	std::thread t1(inc);
	std::thread t2(dec);

	t1.join();
	t2.join();

	while (worker.n_loading())
	{
		std::this_thread::yield();
	}
	
	EXPECT_EQ(counter, 0);
}
