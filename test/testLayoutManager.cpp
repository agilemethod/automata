#include "gtest/gtest.h"
#include <cstdio>
#include <core/image.h>
#include <matapath/layout.h>
#include <matapath/io.hpp>

const std::string SCREEN_DIR = "./tos_testcase/tos_screen";

class LayoutMangerTest: public ::testing::Test
{
public:
	LayoutMangerTest()
		: layout_manager(std::make_shared<TosGameLayout>())
	{
	}

protected:
	virtual void SetUp()
	{
		layout_manager.set_rows_count(5);
		layout_manager.set_cols_count(6);
	}

	virtual void TearDown()
	{
	}

protected:
	LayoutManager layout_manager;
};

TEST_F(LayoutMangerTest, Init)
{
	EXPECT_EQ(layout_manager.n_rows(), 5);
	EXPECT_EQ(layout_manager.n_cols(), 6);
}

TEST_F(LayoutMangerTest, SetResolutionFit)
{
	layout_manager.set_resolution(640, 960);
	auto board = layout_manager.board();

	EXPECT_EQ(board.x, 0);
	EXPECT_EQ(board.y, 430);
	EXPECT_EQ(board.width, 640);
	EXPECT_EQ(board.height, 530);
}

TEST_F(LayoutMangerTest, SetResolutionIpad)
{
	Image img(SCREEN_DIR + "/tos_ipad.png");
	layout_manager.set_resolution(img.width(), img.height());
	auto board = layout_manager.board();

	Image img_board = img.crop(board.x, board.y, board.width, board.height);
	img_board.save("tos_ipad_board.png");
}

TEST_F(LayoutMangerTest, SetResolutionIphone)
{
	Image img(SCREEN_DIR + "/tos_iphone.png");
	layout_manager.set_resolution(img.width(), img.height());
	auto board = layout_manager.board();

	Image img_board = img.crop(board.x, board.y, board.width, board.height);
	img_board.save("tos_iphone_board.png");
}

TEST_F(LayoutMangerTest, SetResolutionAndroid)
{
	Image img(SCREEN_DIR + "/tos_android.png");
	layout_manager.set_resolution(img.width(), img.height());
	auto board = layout_manager.board();

	Image img_board = img.crop(board.x, board.y, board.width, board.height);
	img_board.save("tos_android_board.png");
}

TEST_F(LayoutMangerTest, SetResolutionAndroidMenu)
{
	Image img(SCREEN_DIR + "/tos_android_menu.png");
	img = img.crop(0, 0, 1080, 1772);
	
	layout_manager.set_resolution(img.width(), img.height());
	auto board = layout_manager.board();
	
	Image img_board = img.crop(board.x, board.y, board.width, board.height);
	img_board.save("tos_android_menu_board.png");
}

TEST_F(LayoutMangerTest, GetRunesIphone)
{
	Image img(SCREEN_DIR + "/tos_iphone.png");
	layout_manager.set_resolution(img.width(), img.height());
	auto board = layout_manager.board();

	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		for (size_t j = 0; j < layout_manager.n_cols(); j++)
		{
			auto cell = layout_manager.cell(i, j);
			Image img_cell = img.crop(cell.x, cell.y, cell.width, cell.height);
			std::string filename("tospic/tos_iphone_cell_");
			filename.append(std::to_string(i));
			filename.append("_");
			filename.append(std::to_string(j));
			filename.append(".png");
			img_cell.save(filename);
		}
	}
}

TEST_F(LayoutMangerTest, GetRunesAndroid)
{
	Image img(SCREEN_DIR + "/tos_android.png");
	layout_manager.set_resolution(img.width(), img.height());
	auto board = layout_manager.board();

	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		for (size_t j = 0; j < layout_manager.n_cols(); j++)
		{
			auto cell = layout_manager.cell(i, j);
			Image img_cell = img.crop(cell.x, cell.y, cell.width, cell.height);
			std::string filename("tospic/tos_android_cell_");
			filename.append(std::to_string(i));
			filename.append("_");
			filename.append(std::to_string(j));
			filename.append(".png");
			img_cell.save(filename);
		}
	}
}

TEST_F(LayoutMangerTest, GetBlackWhiteRunes)
{
#if 0
	Image img(SCREEN_DIR + "/tos_black_white.png");
	layout_manager.set_resolution(img.width(), img.height());
	auto board = layout_manager.board();
	Image img_board = img.crop(board.x, board.y, board.width, board.height);
	img_board.save("tos_bw_board.png");

	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		for (size_t j = 0; j < layout_manager.n_cols(); j++)
		{
			auto cell = layout_manager.cell(i, j);
			Image img_cell = img.crop(cell.x, cell.y, cell.width, cell.height);
			std::string filename("tospic/tos_bw_cell_");
			filename.append(std::to_string(i));
			filename.append("_");
			filename.append(std::to_string(j));
			filename.append(".png");
			img_cell.resize(128, 128);
			img_cell.save(filename);
		}
	}
#endif
}

TEST_F(LayoutMangerTest, GetAllBorder)
{
	Image img_board;
	{
		Image img(SCREEN_DIR + "/pingtu02.jpg");
		layout_manager.set_resolution(img.width(), img.height());
		auto board = layout_manager.board();
		img_board = img.crop(board.x, board.y, board.width, board.height);
	}

	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		for (size_t j = 0; j <= layout_manager.n_cols(); j++)
		{
			auto border = layout_manager.vborder(i, j);
			Image img_border = img_board.crop(border.x, border.y, border.width, border.height);
			std::string filename("tospic/tos_vborder_");
			filename.append(std::to_string(i));
			filename.append("_");
			filename.append(std::to_string(j));
			filename.append(".png");
			img_border.save(filename);
		}
	}

	for (size_t i = 0; i <= layout_manager.n_rows(); i++)
	{
		for (size_t j = 0; j < layout_manager.n_cols(); j++)
		{
			auto border = layout_manager.hborder(i, j);
			Image img_border = img_board.crop(border.x, border.y, border.width, border.height);
			std::string filename("tospic/tos_hborder_");
			filename.append(std::to_string(i));
			filename.append("_");
			filename.append(std::to_string(j));
			filename.append(".png");
			img_border.save(filename);
		}
	}
}
