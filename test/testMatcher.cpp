#include "gtest/gtest.h"
#include <core/image.h>
#include <core/matcher.h>

TEST(TestMatcher, MatchMethod)
{
	MatchMethod match_method_sqdiff = MatchMethod::Sqdiff();
	EXPECT_TRUE(match_method_sqdiff.method == MatchMethod::Sqdiff().method);
	EXPECT_TRUE(match_method_sqdiff.prefer_higher_value == false);

	MatchMethod match_method_ccorr = MatchMethod::Ccorr();
	EXPECT_TRUE(match_method_ccorr.method == MatchMethod::Ccorr().method);
	EXPECT_TRUE(match_method_ccorr.prefer_higher_value == true);

	MatchMethod match_method_ccoeffNormed0 = MatchMethod::CcoeffNormed();
	// assgin constructor
	MatchMethod match_method_ccoeffNormed = match_method_ccoeffNormed0;
	EXPECT_TRUE(match_method_ccoeffNormed.method == MatchMethod::CcoeffNormed().method);
	EXPECT_TRUE(match_method_ccoeffNormed.prefer_higher_value == true);
}

#ifndef NDEBUG
TEST(TestMatcher, MatchResult)
{
	MatchResult match_result;
	match_result.setup(0);

	EXPECT_TRUE(match_result.count() == 6);
	EXPECT_FLOAT_EQ(match_result.max(), 0.6f);
	EXPECT_FLOAT_EQ(match_result.min(), 0.1f);

	auto score = match_result.score();
	EXPECT_TRUE(score.size() == 6);
	float s = 0.1;
	for (size_t i = 0; i < score.size(); i++)
	{
		EXPECT_FLOAT_EQ(score[i], s);
		s += 0.1;
	}

	MatchResult match_result2 = std::move(match_result);
	match_result2.normalize();
	score = match_result2.score();
	EXPECT_TRUE(match_result2.count() == 6);
	EXPECT_FLOAT_EQ(match_result2.max(), 1.0f);
	EXPECT_FLOAT_EQ(match_result2.min(), 0.0f);
}
#endif

TEST(TestMatcher, Matcher)
{
	Matcher matcher;
	matcher.set_match_method(MatchMethod::Ccoeff());

	MatchMethod other = MatchMethod::Ccoeff();
	EXPECT_TRUE(other.method == matcher.match_method().method);
	EXPECT_TRUE(other.prefer_higher_value == matcher.match_method().prefer_higher_value);

	Image img_source("sample.jpg");
	Image img_templ("pattern.jpg");
	auto result = matcher.match(img_source, img_templ);
	auto max_loc = result.max_location();

	EXPECT_EQ(std::get<0>(max_loc), 115);
	EXPECT_EQ(std::get<1>(max_loc), 175);
}
