#include "gtest/gtest.h"
//#include "matapath/matapath-player.h"
#include "gen-cpp/metapath.pb.h"
#include <core/worker.hpp>

using namespace io::automata::metapath;

#include <cstdio>

class PathMap
{
};

class Board
{
};

class Requirement
{
};

// mock: MataPathPlayer
class MataPathPlayer
{
public:
	Board load_board()
	{
		std::printf("MataPathPlayer::load_board()\n");
		Board board;
		return board;
	}

	PathMap calculate_path(PathMap previous_path, Board board, Requirement requirement)
	{
		std::printf("MataPathPlayer::calculate_path()\n");
		PathMap path_map;
		return path_map;
	}

	void move_rune(PathMap path)
	{
		std::printf("MataPathPlayer::move_rune()\n");
	}
};

// mock: c++ sends a proto::Respond to java
void send_respond(const proto::Respond & respond)
{
	std::printf("send respond!\n");
}


// create task from proto::Request
class TaskMaker
{
public:
	typedef std::function<void()> task_t;

	static task_t create(const proto::Request & request, MataPathPlayer & player)
	{
		switch (request.type())
		{
		case proto::Request::LoadBoard:
			return std::move(load_board_task(request, player));
		case proto::Request::CalculatePath:
			return std::move(calculate_path_task(request, player));
		case proto::Request::MoveRune:
			return std::move(move_rune_task(request, player));
		default:
			return task_t();
		}
	}

	static task_t load_board_task(const proto::Request & request, MataPathPlayer & player)
	{
		auto task = [&player]()
		{
			proto::Respond respond;
			respond.set_type(proto::Respond::LoadBoardDone);

			auto board_ = player.load_board(); // FIXME
			std::printf("cover Board into proto::Board\n");
			proto::Board * board = new proto::Board();
			respond.set_allocated_board(board);
			send_respond(respond);
		};
		
		return task;
	}

	static task_t calculate_path_task(const proto::Request & request, MataPathPlayer & player)
	{
		auto task = [&player]()
		{
			proto::Respond respond;
			respond.set_type(proto::Respond::CalculatePathDone);

			std::printf("player calculates the paths...\n");
			/*
			auto pathmap = player.calculate_path(
					pathmap_from_task(task),
					board_from_task(task),
					requirement_from_task(task));
			*/

			proto::PathMap * pathmap = new proto::PathMap();
			respond.set_allocated_map(pathmap);
			send_respond(respond);
		};

		return task;
	}

	static task_t move_rune_task(const proto::Request & request, MataPathPlayer & player)
	{
		auto task = [&player]()
		{
			proto::Respond respond;
			respond.set_type(proto::Respond::MoveRuneDone);

			std::printf("player moves runes...\n");
			//player.move_rune(pathmap_from_task(task));

			send_respond(respond);
		};

		return task;
	}
};

// helpers
namespace
{

// global player
MataPathPlayer player;

// mock: java send a proto::Request that will be used to generate a task
std::function<void()> get_load_board_task()
{
	proto::Request request;
	request.set_type(proto::Request::LoadBoard);

	return TaskMaker::create(request, player);
}

std::function<void()> get_clculate_path_task()
{
	proto::Request request;
	request.set_type(proto::Request::CalculatePath);

	return TaskMaker::create(request, player);
}

std::function<void()> get_move_rune_task()
{
	proto::Request request;
	request.set_type(proto::Request::MoveRune);

	return TaskMaker::create(request, player);
}
}


//==============================================================
class MataPathLogicTest: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
	}

	virtual void TearDown()
	{
		worker.clear_all_tasks();
	}

protected:
	Worker worker;
};


TEST_F(MataPathLogicTest, LoadBoard)
{
	auto task = get_load_board_task();
	worker.dispatch(task);

	std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

TEST_F(MataPathLogicTest, CalculatePath)
{
	auto task = get_clculate_path_task();
	worker.dispatch(task);

	std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

TEST_F(MataPathLogicTest, MoveRune)
{
	auto task = get_move_rune_task();
	worker.dispatch(task);

	std::this_thread::sleep_for(std::chrono::milliseconds(200));
}
