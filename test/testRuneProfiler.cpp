#include "gtest/gtest.h"
#include <fstream>
#include <core/image.h>
#include <core/matcher.h>
#include <core/text_color.h>
#include <matapath/rune.h>
#include <matapath/rune_provider.h>
#include <matapath/layout.h>
#include <matapath/basic_types.h>
#include <matapath/gameplayer.h>

#include <sys/types.h>
#include <dirent.h>
#include <math.h>
#include <sys/stat.h>

const std::string TESTCASE_DIR = "./tos_testcase/tos_tests";
const std::string TESTCASE_CONFIG = TESTCASE_DIR + "/TosRunesProfile.txt";

class RuneProfiler
{
public:
	struct test_case_t
	{
		std::string filename;
		Board board;
	};

public:
	RuneProfiler(const std::string & profile);

public:
	const std::vector<test_case_t> & test_case() const { return _test_case; }
	
private:
	std::vector<test_case_t> _test_case;
};

RuneProfiler::RuneProfiler(const std::string & profile)
{
	std::ifstream profile_file(profile);
	if (!profile_file.is_open())
	{
		throw std::runtime_error("rune profile file open failed!");
	}

	std::string filename;
	std::string board;
	while (!profile_file.eof())
	{
		std::getline(profile_file, filename);
		if (filename.empty()) break;
		if (filename.find("#") != std::string::npos) continue;
		std::getline(profile_file, board);
		if (board.empty()) break;

		test_case_t test_case;
		test_case.filename = filename;
		test_case.board = make_board(board, Board::rows, Board::cols);
		_test_case.push_back(test_case);
	}

	profile_file.close();
}

struct BoardDiff
{
	static BoardDiff create(const Board & answer, const Board & test);

	uint32_t match_count = 0;
	uint32_t unmatch_count = 0;
	uint32_t error_count = 0;
	std::string report;
};

BoardDiff BoardDiff::create(const Board & answer, const Board & test)
{
	static std::vector<std::string> lookup(9);
	lookup[0] = "空";
	lookup[1] = "水";
	lookup[2] = "火";
	lookup[3] = "木";
	lookup[4] = "光";
	lookup[5] = "暗";
	lookup[6] = "心";
	lookup[7] = "問";
	lookup[8] = "無";


	BoardDiff result;
	std::stringstream report;
	std::stringstream detail;
	for (size_t i = 0; i < Board::rows; i++)
	{
		for (size_t j = 0; j < Board::cols; j++)
		{
			report << lookup[test.data[i][j].rune.type];
			if (answer.data[i][j].rune.type == test.data[i][j].rune.type)
			{
				report << " ";
				result.match_count++;
			}
			else if (test.data[i][j].rune.type == TosRune::RuneTypeEnd)
			{
				report << text::blue("U");
				std::stringstream info;
				info << "(answer)" << lookup[answer.data[i][j].rune.type] << " vs (wrong)" << lookup[test.data[i][j].rune.type] << " at (" << i << ", " << j << ")\n";
				detail << text::blue(info.str());
				result.unmatch_count++;
			}
			else
			{
				report << text::red("E");
				std::stringstream info;
				info << "(answer)" << lookup[answer.data[i][j].rune.type] << " vs (wrong)" << lookup[test.data[i][j].rune.type] << " at (" << i << ", " << j << ")\n";
				detail << text::red(info.str());
				result.error_count++;
			}
		}
		report << "\n";
	}
	report << "\n" << detail.str();
	result.report = report.str();
	return result;
};

TEST(RuneProfilerTest, LoadData)
{
	RuneProfiler rune_profiler(TESTCASE_CONFIG);
	auto & test_case = rune_profiler.test_case();
	EXPECT_TRUE(test_case.size() > 0);
	std::string filename = test_case[0].filename;
	Board board = test_case[0].board;
}

TEST(RuneProfilerTest, Report)
{
	RuneProfiler rune_profiler(TESTCASE_CONFIG);
	auto & test_case = rune_profiler.test_case();
	EXPECT_TRUE(test_case.size() > 0);
	Board myboard = make_board(
		"g,g,y,g,h,y,r,b,g,r,g,g,h,y,r,p,g,y,h,r,y,g,h,p,y,r,h,p,y,x",
		Board::rows, Board::cols);
	auto report = BoardDiff::create(test_case[0].board, myboard);
	if (!report.report.empty())
	{
		std::cout << report.report << "\n";
	}
	EXPECT_EQ(report.match_count, 28);
	EXPECT_EQ(report.unmatch_count, 1);
	EXPECT_EQ(report.error_count, 1);
	EXPECT_EQ(report.match_count + report.unmatch_count + report.error_count, 30);
}

TEST(RuneProfilerTest, Profiling)
{
	float total_count(0);
	uint32_t total_match_count(0);
	uint32_t total_unmatch_count(0);
	uint32_t total_error_count(0);

	RuneProfiler rune_profiler(TESTCASE_CONFIG);
	auto & test_case = rune_profiler.test_case();
	ASSERT_TRUE(test_case.size() > 0);
	GamePlayer gameplayer;
	for (size_t i = 0; i < test_case.size(); i++)
	{
		auto & tc = test_case[i];
		const std::string filepath = TESTCASE_DIR + "/" + tc.filename;
		std::printf("=== case: %u, %s ===\n", i, filepath.c_str());
		auto report = BoardDiff::create(tc.board, gameplayer.load_board(filepath));
		std::printf("Match: %u, Unmatch: %u, Error: %u\n", report.match_count, report.unmatch_count, report.error_count);
		if (!report.report.empty()) std::cout << report.report << std::endl;
		EXPECT_EQ(report.match_count + report.unmatch_count + report.error_count, 30);
		total_count += 30;
		total_match_count += report.match_count;
		total_unmatch_count += report.unmatch_count;
		total_error_count += report.error_count;
	}

	std::printf(
		"Match nums: %5u, Unmatch nums: %5u, Error nums: %5u, Total nums: %5u \n",
		total_match_count,
		total_unmatch_count,
		total_error_count,
		static_cast<uint32_t>(total_count));
	std::printf(
		"Match rate: %5.2f, Unmatch rate: %5.2f, Error rate: %5.2f \n",
		total_match_count / total_count,
		total_unmatch_count / total_count,
		total_error_count / total_count);
}


std::vector<std::string> get_file_list(const std::string path)
{
	std::vector<std::string> file_list;
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir (path.c_str())) != NULL)
	{
		while ((ent = readdir (dir)) != NULL)
		{
			std::string filename(ent->d_name);
			if (filename != "." && filename != "..")
			{
				file_list.push_back(filename);
			}
		}
		closedir (dir);
	}
	return file_list;
}

void push_runes_image(const std::string &rune_sample_path, const std::string &output_path, std::vector<std::tuple<std::string, std::string, Image>> &rune_images)
{
	std::vector<std::string> runes_sample_list = get_file_list(rune_sample_path);
	for(auto &runes_name : runes_sample_list) {
		if (runes_name.find("PNG") == std::string::npos && runes_name.find("png") == std::string::npos)
		{
			continue;
		}
		std::string rune_origin_path = rune_sample_path + "/" + runes_name;
		std::string rune_output_path = output_path;
		rune_output_path += "/" + runes_name.substr(0, runes_name.find("."));
		Image img(rune_origin_path);
		img.resize(32, 32);
		mkdir(rune_output_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		img.save(rune_output_path + "/" + runes_name);
		rune_images.push_back(std::make_tuple(rune_output_path, runes_name, img));
	}
}

std::tuple<std::vector<float>, size_t> match_runes(Matcher &matcher, const std::vector<std::tuple<std::string, std::string, Image>> &rune_images, const Image &img_cell, size_t from, size_t to) {
	
	float best_value = 0;
	float second_value = 0;
	size_t best_sample = 0;
	std::vector<float> scores;
	for (size_t k = from; k < to; k++)
	{
		const auto &img_runs = rune_images[k];
		auto match_result = matcher.match(std::get<2>(img_runs), img_cell);
		float value = match_result.max();
		if (value > best_value)
		{
			best_value = value;
			best_sample = k;
		}
		scores.push_back(value);
	}
	std::sort(scores.begin(), scores.end(), std::greater<float>());
	return std::make_tuple(scores, best_sample);
}

std::tuple<float, float> get_agv_sd(std::vector<float> scores) {
	float sum = 0;
	float avg = 0;
	float sum2 = 0;
	float sd = 0;
	for (float score : scores)
	{
		sum += score;
	}
	avg = sum / scores.size();
	for (float score : scores)
	{
		sum2 += (score - avg) * (score - avg);
	}
	sd = sqrt(sum2 / (scores.size() - 1));
	return std::make_tuple(avg, sd);
}

void run_match(const std::string &image_path, const std::string &output_path, std::vector<std::tuple<std::string, std::string, Image>> &rune_images)
{
	Matcher matcher;
	matcher.set_match_method(MatchMethod::CcoeffNormed());
	std::vector<std::string> image_list = get_file_list(image_path);
	for (auto &image_name : image_list)
	{
		if (image_name.find("PNG") == std::string::npos && image_name.find("png") == std::string::npos)
		{
			continue;
		}
		std::string image_full_path = image_path + "/" + image_name;
		Image img(image_full_path);
		LayoutManager layout_manager(std::make_shared<TosGameLayout>());
		layout_manager.set_rows_count(5);
		layout_manager.set_cols_count(6);
		layout_manager.set_resolution(img.width(), img.height());
		
		auto board_rect = layout_manager.board();
		Image img_board = img.crop(board_rect.x, board_rect.y, board_rect.width, board_rect.height);
		img_board.resize(layout_manager.n_cols() * 32, layout_manager.n_rows() * 32);
		bool isFind = false;
		for (size_t i = 0; i < layout_manager.n_rows(); i++)
		{
			for (size_t j = 0; j < layout_manager.n_cols(); j++)
			{
				auto img_cell = img_board.crop(j * 32, i * 32, 32, 32);
				
				auto result = match_runes(matcher, rune_images, img_cell, 0, 13);
				auto scores = std::get<0>(result);
				float best_value = scores[0];
				float second_value = scores[1];
				
				auto avg_sd = get_agv_sd(scores);
				float avg = std::get<0>(avg_sd);
				float sd = std::get<1>(avg_sd);
				
				size_t best_sample = std::get<1>(result);
				
				float diff = best_value - second_value;
				
				std::string rune_full_path = output_path;
				std::string rune_image_name = std::to_string((int) (best_value * 10000))
				+ "." + std::to_string((int) (avg * 100))
				+ "." + std::to_string((int) (sd * 100))
				+ "." + std::to_string((int) (diff * 100))
				+ "." + image_name
				+ "." + std::to_string(i)
				+ "." + std::to_string(j)
				+ ".png";
				
				bool need_match_advence = false;
				
				if (diff > -1)
				{
					float th = 0.8;
					if (std::get<1>(rune_images[best_sample]) == "水普.png")
					{
						th = 0.95;
					}
					else if (std::get<1>(rune_images[best_sample]) == "火普.png")
					{
						th = 0.91;
					}
					else if (std::get<1>(rune_images[best_sample]) == "木普.png")
					{
						th = 0.94;
					}
					else if (std::get<1>(rune_images[best_sample]) == "光普.png")
					{
						th = 0.95;
					}
					else if (std::get<1>(rune_images[best_sample]) == "暗普.png")
					{
						th = 0.95;
					}
					else if (std::get<1>(rune_images[best_sample]) == "心普.png")
					{
						th = 0.96;
					}
					else if (std::get<1>(rune_images[best_sample]) == "問普.png")
					{
						th = 0.95;
					}
                    else if (std::get<1>(rune_images[best_sample]) == "水強.png")
                    {
                        th = 0.98;
                    }
                    else if (std::get<1>(rune_images[best_sample]) == "火強.png")
                    {
                        th = 0.96;
                    }
                    else if (std::get<1>(rune_images[best_sample]) == "木強.png")
                    {
                        th = 0.95;
                    }
                    else if (std::get<1>(rune_images[best_sample]) == "光強.png")
                    {
                        th = 0.95;
                    }
                    else if (std::get<1>(rune_images[best_sample]) == "暗強.png")
                    {
                        th = 0.97;
                    }
                    else if (std::get<1>(rune_images[best_sample]) == "心強.png")
                    {
                        th = 0.98;
                    }
					if (best_value > th)
						need_match_advence = false;
					else
						need_match_advence = true;
				}
				else
				{
					need_match_advence = true;
//					rune_full_path += "/" + std::to_string(rune_images.size());
//					mkdir(rune_full_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
//					rune_images.push_back(std::make_tuple(rune_full_path, image_name, img_cell));
//					img_cell.save(rune_full_path + "/" + rune_image_name);
					
					
				}
				
				if (!need_match_advence)
				{
					rune_full_path = std::get<0>(rune_images[best_sample]);
					img_cell.save(rune_full_path + "/" + rune_image_name);
				}
				else
				{
//					rune_full_path += "/" + std::to_string(0);
//					mkdir(rune_full_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
//					img_cell.save(rune_full_path + "/" + rune_image_name);
					
					auto result = match_runes(matcher, rune_images, img_cell, 0, rune_images.size());
					auto scores = std::get<0>(result);
					float best_value = scores[0];
					float second_value = scores[1];
					
					auto avg_sd = get_agv_sd(scores);
					float avg = std::get<0>(avg_sd);
					float sd = std::get<1>(avg_sd);
					
					rune_image_name = std::to_string((int) (best_value * 10000))
					+ "." + std::to_string((int) (avg * 100))
					+ "." + std::to_string((int) (sd * 100))
					+ "." + std::to_string((int) (diff * 100))
					+ "." + image_name
					+ "." + std::to_string(i)
					+ "." + std::to_string(j)
					+ ".png";
					
					size_t best_sample = std::get<1>(result);
					
                    if (best_value >= 0.8)
                    {
                        rune_full_path = std::get<0>(rune_images[best_sample]);
                        img_cell.save(rune_full_path + "/" + rune_image_name);
                    }
                    else
                    {
                        rune_full_path = output_path + "/0_oth";
                        img_cell.save(rune_full_path + "/" + rune_image_name);
                    }
					std::cout <<"best_value " << best_value << std::endl;
				}
                //std::cout << rune_full_path + "/" + rune_image_name << std::endl;
			}
		}
	}
}

void run_match2(const std::string &image_path, const std::string &output_path, std::vector<std::tuple<std::string, std::string, Image>> &rune_images, std::vector<std::tuple<std::string, std::string, Image>> &sample_rune_images)
{
    Matcher matcher;
    matcher.set_match_method(MatchMethod::CcoeffNormed());
    std::vector<std::string> image_list = get_file_list(image_path);
    for (auto &image_name : image_list)
    {
        if (image_name.find("PNG") == std::string::npos && image_name.find("png") == std::string::npos)
        {
            continue;
        }
        std::string image_full_path = image_path + "/" + image_name;
        Image img(image_full_path);
        LayoutManager layout_manager(std::make_shared<TosGameLayout>());
        layout_manager.set_rows_count(5);
        layout_manager.set_cols_count(6);
        layout_manager.set_resolution(img.width(), img.height());
        
        auto board_rect = layout_manager.board();
        Image img_board = img.crop(board_rect.x, board_rect.y, board_rect.width, board_rect.height);
        img_board.resize(layout_manager.n_cols() * 32, layout_manager.n_rows() * 32);
        bool isFind = false;
        std::cout << "rune_images.size() " << rune_images.size() << std::endl;
        for (size_t i = 0; i < layout_manager.n_rows(); i++)
        {
            for (size_t j = 0; j < layout_manager.n_cols(); j++)
            {
                auto img_cell = img_board.crop(j * 32, i * 32, 32, 32);
                
                auto result = match_runes(matcher, rune_images, img_cell, 0, rune_images.size());
                float best_value = 0;
                float second_value = 0;
                auto scores = std::get<0>(result);
                if (scores.size() > 0)
                {
                    best_value = scores[0];
                    second_value = scores[1];
                }
                
                size_t best_sample = std::get<1>(result);
                
                float diff = best_value - second_value;
                
                std::string rune_full_path = output_path;
                std::string rune_image_name = std::to_string((int) (best_value * 100))
                + "." + std::to_string((int) (diff * 100))
                + "." + image_name
                + "." + std::to_string(i)
                + "." + std::to_string(j)
                + ".png";
                
                bool need_match_advence = false;
                
                if (best_value > 0.96)
                {
                    rune_full_path += "/" + std::to_string(best_sample);
                    img_cell.save(rune_full_path + "/" + rune_image_name);
                }
                else
                {
                    need_match_advence = true;
                    rune_full_path += "/" + std::to_string(rune_images.size());
                    mkdir(rune_full_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
                    rune_images.push_back(std::make_tuple(rune_full_path, image_name, img_cell));
                    img_cell.save(rune_full_path + "/" + rune_image_name);
                    
                    auto result = match_runes(matcher, sample_rune_images, img_cell, 0, sample_rune_images.size());
                    auto scores = std::get<0>(result);
                    float best_value = scores[0];
                    size_t best_sample = std::get<1>(result);
                    std::string name = std::get<1>(sample_rune_images[best_sample]);
                    if (best_value < 0.90 && best_value > 0.70)
                    {
                        img_cell.save(std::string("runes/0_new/") + name
                                      + "." + std::to_string(std::to_string((int) (best_value * 1000))) + ".png");
                    }
                    else if (best_value <= 0.70)
                    {
                        img_cell.save(std::string("runes/0_oth/") + name
                                      + "." + std::to_string(std::to_string((int) (best_value * 1000))) + ".png");
                    }
                }
                std::cout << rune_full_path + "/" + rune_image_name << std::endl;
            }
        }
    }
}

TEST(RuneProfilerTest, CollectSameRunesFromBoard)
{
	std::string rune_path = "runes";
	std::string image_path = "/Users/Andy/Google 雲端硬碟/Automata/Source/test/tos/6";
    std::string image_path2 = "/Users/Andy/Google 雲端硬碟/Automata/Source/test/tos/4";
    std::string image_path3 = "/Users/Andy/Google 雲端硬碟/Automata/Source/test/tos/5";
    std::string image_path4 = "/Users/Andy/Google 雲端硬碟/Automata/Source/test/tos/6p";
    std::string image_path5 = "/Users/Andy/Google 雲端硬碟/Automata/Source/test/tos/6ps";
    std::string image_path6 = "/Users/Andy/Google 雲端硬碟/Automata/Source/test/tos/pad";
    std::string image_path7 = "/Users/Andy/Google 雲端硬碟/Automata/Source/test/tos/padair";
	std::string image_path8 = "/Users/Andy/Google 雲端硬碟/Automata/Source/test/tos/test";
    
	std::string rune_sample_path1 = "/Users/Andy/Google 雲端硬碟/Automata/runes/test";
	std::string rune_sample_path2 = "/Users/Andy/Google 雲端硬碟/Automata/runes/test2";
	std::vector<std::string> rune_list = get_file_list(rune_path);
	
	
	mkdir(rune_path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	mkdir("runes/0_new", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir("runes/0_oth", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	// tuple: path, name, image
	std::vector<std::tuple<std::string, std::string, Image>> sample_rune_images;
	push_runes_image(rune_sample_path1, rune_path, sample_rune_images);
	push_runes_image(rune_sample_path2, rune_path, sample_rune_images);
	
//    std::vector<std::tuple<std::string, std::string, Image>> rune_images;
//	run_match2(image_path, rune_path, rune_images, sample_rune_images);
//	run_match2(image_path2, rune_path, rune_images, sample_rune_images);
//	run_match2(image_path3, rune_path, rune_images, sample_rune_images);
//    run_match2(image_path4, rune_path, rune_images, sample_rune_images);
//    run_match2(image_path5, rune_path, rune_images, sample_rune_images);
//    run_match2(image_path6, rune_path, rune_images, sample_rune_images);
//    run_match2(image_path7, rune_path, rune_images, sample_rune_images);
    
    run_match(image_path, rune_path, sample_rune_images);
    run_match(image_path2, rune_path, sample_rune_images);
    run_match(image_path3, rune_path, sample_rune_images);
    run_match(image_path4, rune_path, sample_rune_images);
    run_match(image_path5, rune_path, sample_rune_images);
    run_match(image_path6, rune_path, sample_rune_images);
    run_match(image_path7, rune_path, sample_rune_images);
	run_match(image_path8, rune_path, sample_rune_images);
}










