#include "gtest/gtest.h"
#include <algorithm>
#include <matapath/rune.h>
#include <matapath/basic_types.h>
#include <matapath/solver.h>

namespace {

uint8_t * reorder(uint8_t input[], size_t n=30)
{
	for (size_t i = 0; i < n ; i++)
	{
		input[i]++;
	}
	return input;
}

} // namespace

TEST(SolverTest, TestCanMoveOrbInSolution)
{
	Solution solution;
	solution.cursor = Point<uint8_t>(3, 3);
	solution.paths.push_back(RR);
	for (uint8_t dir = 0; dir < 8; dir++)
	{
		if (dir == LL)
			EXPECT_EQ(Solver::can_move_orb_in_solution(solution, dir), false);
		else
			EXPECT_EQ(Solver::can_move_orb_in_solution(solution, dir), true);
	}
}

TEST(SolverTest, TestCanMoveOrb)
{
	const bool t(true);
	const bool f(false);

	Point<uint8_t> rc(0, 0);
	EXPECT_EQ(Solver::can_move_orb(rc, RR), t);
	EXPECT_EQ(Solver::can_move_orb(rc, RB), t);
	EXPECT_EQ(Solver::can_move_orb(rc, BB), t);
	EXPECT_EQ(Solver::can_move_orb(rc, LB), f);
	EXPECT_EQ(Solver::can_move_orb(rc, LL), f);
	EXPECT_EQ(Solver::can_move_orb(rc, LU), f);
	EXPECT_EQ(Solver::can_move_orb(rc, UU), f);
	EXPECT_EQ(Solver::can_move_orb(rc, RU), f);

	Point<uint8_t> rc2(0, COLS-1);
	EXPECT_EQ(Solver::can_move_orb(rc2, RR), f);
	EXPECT_EQ(Solver::can_move_orb(rc2, RB), f);
	EXPECT_EQ(Solver::can_move_orb(rc2, BB), t);
	EXPECT_EQ(Solver::can_move_orb(rc2, LB), t);
	EXPECT_EQ(Solver::can_move_orb(rc2, LL), t);
	EXPECT_EQ(Solver::can_move_orb(rc2, LU), f);
	EXPECT_EQ(Solver::can_move_orb(rc2, UU), f);
	EXPECT_EQ(Solver::can_move_orb(rc2, RU), f);

	Point<uint8_t> rc3(ROWS-1, 0);
	EXPECT_EQ(Solver::can_move_orb(rc3, RR), t);
	EXPECT_EQ(Solver::can_move_orb(rc3, RB), f);
	EXPECT_EQ(Solver::can_move_orb(rc3, BB), f);
	EXPECT_EQ(Solver::can_move_orb(rc3, LB), f);
	EXPECT_EQ(Solver::can_move_orb(rc3, LL), f);
	EXPECT_EQ(Solver::can_move_orb(rc3, LU), f);
	EXPECT_EQ(Solver::can_move_orb(rc3, UU), t);
	EXPECT_EQ(Solver::can_move_orb(rc3, RU), t);

	Point<uint8_t> rc4(ROWS-1, COLS-1);
	EXPECT_EQ(Solver::can_move_orb(rc4, RR), f);
	EXPECT_EQ(Solver::can_move_orb(rc4, RB), f);
	EXPECT_EQ(Solver::can_move_orb(rc4, BB), f);
	EXPECT_EQ(Solver::can_move_orb(rc4, LB), f);
	EXPECT_EQ(Solver::can_move_orb(rc4, LL), t);
	EXPECT_EQ(Solver::can_move_orb(rc4, LU), t);
	EXPECT_EQ(Solver::can_move_orb(rc4, UU), t);
	EXPECT_EQ(Solver::can_move_orb(rc4, RU), f);

	Point<uint8_t> rc5(1, 1);
	EXPECT_EQ(Solver::can_move_orb(rc5, RR), t);
	EXPECT_EQ(Solver::can_move_orb(rc5, RB), t);
	EXPECT_EQ(Solver::can_move_orb(rc5, BB), t);
	EXPECT_EQ(Solver::can_move_orb(rc5, LB), t);
	EXPECT_EQ(Solver::can_move_orb(rc5, LL), t);
	EXPECT_EQ(Solver::can_move_orb(rc5, LU), t);
	EXPECT_EQ(Solver::can_move_orb(rc5, UU), t);
	EXPECT_EQ(Solver::can_move_orb(rc5, RU), t);
}

TEST(SolverTest, TestMoveRc)
{
	Point<uint8_t> rc(3, 3);
	Solver::move_rc(rc, RR);
	EXPECT_EQ(rc, Point<uint8_t>(3, 4));
	Solver::move_rc(rc, RB);
	EXPECT_EQ(rc, Point<uint8_t>(4, 5));
	Solver::move_rc(rc, BB);
	EXPECT_EQ(rc, Point<uint8_t>(5, 5));
	Solver::move_rc(rc, LB);
	EXPECT_EQ(rc, Point<uint8_t>(6, 4));
	Solver::move_rc(rc, LL);
	EXPECT_EQ(rc, Point<uint8_t>(6, 3));
	Solver::move_rc(rc, LU);
	EXPECT_EQ(rc, Point<uint8_t>(5, 2));
	Solver::move_rc(rc, UU);
	EXPECT_EQ(rc, Point<uint8_t>(4, 2));
	Solver::move_rc(rc, RU);
	EXPECT_EQ(rc, Point<uint8_t>(3, 3));
}

TEST(SolverTest, TestSwapOrbInSolution)
{
	uint8_t board_input[] = {3,4,0,5,2,5,   1,2,5,2,1,4,   5,2,3,3,1,4,   3,4,3,5,5,4,   0,2,2,4,1,5};
	Solution solution;
	solution.cursor = Point<uint8_t>(0, 0);
	solution.board = make_board(reorder(board_input), Board::rows, Board::cols);
	Solver::swap_orb_in_solution(solution, RB);
	EXPECT_EQ(solution.board.data[0][0].rune.type, 3);
	EXPECT_EQ(solution.board.data[1][1].rune.type, 4);
}

TEST(SolverTest, TestEvolveSolution)
{
	std::vector<Solution> solutions;
	Solution solution;
	uint8_t board_input[] = {4,4,1,4,3,2,   2,4,4,2,2,3,   5,2,0,0,5,4,   5,3,1,2,0,2,   1,2,2,3,2,1};
	solution.board = make_board(reorder(board_input), Board::rows, Board::cols);
	solution.cursor = Point<uint8_t>(0, 0);
	solution.init_cursor = Point<uint8_t>(0, 0);
	Weights weights;
	Solver::make_weights(weights);
	solutions.push_back(solution);
	Solver::evolve_solutions(solutions, 2);
	EXPECT_EQ(solutions.size(), 3);

	EXPECT_EQ(solutions[0].cursor, Point<uint8_t>(1, 0));
	EXPECT_EQ(solutions[0].is_done, false);
	EXPECT_EQ(solutions[0].matches.size(), 1);
	EXPECT_EQ(solutions[0].matches[0].type, 5);
	EXPECT_EQ(solutions[0].matches[0].count, 3);
	EXPECT_EQ(solutions[0].paths[0], 2);
	//EXPECT_EQ(solutions[0].weight, 1);

	EXPECT_EQ(solutions[1].cursor, Point<uint8_t>(0, 0));
	EXPECT_EQ(solutions[1].is_done, true);
	EXPECT_EQ(solutions[1].matches.size(), 0);
	EXPECT_EQ(solutions[1].paths.size(), 0);
	EXPECT_EQ(solutions[1].weight, 0);

	EXPECT_EQ(solutions[2].cursor, Point<uint8_t>(0, 1));
	EXPECT_EQ(solutions[2].is_done, false);
	EXPECT_EQ(solutions[2].matches.size(), 0);
	EXPECT_EQ(solutions[2].paths[0], 0);
	EXPECT_EQ(solutions[2].weight, 0);
}



TEST(SolverTest, TestFindMatch)
{
	uint8_t board_input[] = {3,4,0,5,2,5,   1,2,5,2,1,4,   5,2,3,3,1,4,   3,4,3,5,5,4,   0,2,2,4,1,5};
	Board board = make_board(reorder(board_input), Board::rows, Board::cols);

	Board match_board;
	std::vector<Match> matches;
	Solver::find_matches(matches, board, match_board);

	EXPECT_EQ(matches.size(), 1);
	EXPECT_EQ(matches[0].type, 5);
	EXPECT_EQ(matches[0].count, 3);
}

TEST(SolverTest, TestRemoveMatch)
{
	uint8_t board_input[] = {3,4,0,5,2,5,   1,2,5,2,1,4,   5,2,3,3,1,4,   3,4,3,5,5,4,   0,2,2,4,1,5};
	Board board = make_board(reorder(board_input), Board::rows, Board::cols);
	Board match_board;

	std::vector<Match> matches;
	Solver::find_matches(matches, board, match_board);
	Solver::remove_matches(board, match_board);

	Board ans_board = board;
	ans_board.data[1][5].rune.type = 0;
	ans_board.data[2][5].rune.type = 0;
	ans_board.data[3][5].rune.type = 0;
	EXPECT_EQ(board, ans_board);
}

TEST(SolverTest, TestDropEmptySpace)
{
	uint8_t board_input[] = {3,4,0,5,2,5,   1,2,5,2,1,4,   5,2,3,3,1,4,   3,4,3,5,5,4,   0,2,2,4,1,5};
	Board board = make_board(reorder(board_input), Board::rows, Board::cols);
	
	Board match_board;
	std::vector<Match> matches;
	Solver::find_matches(matches, board, match_board);
	Solver::remove_matches(board, match_board);
	Solver::drop_empty_spaces(board);

	Board ans_board = board;
	ans_board.data[0][5].rune.type = 0;
	ans_board.data[1][5].rune.type = 0;
	ans_board.data[2][5].rune.type = 0;
	ans_board.data[3][5].rune.type = 6;
	ans_board.data[4][5].rune.type = 6;
	EXPECT_EQ(board, ans_board);
}

#if 0
TEST(SolverTest, TestComputeWeights)
{
	std::vector<Match> matches;
	matches.push_back(Match(1, 3));
	matches.push_back(Match(2, 5));
	matches.push_back(Match(1, 4));
	matches.push_back(Match(2, 6));
	Weights weights;
	Solver::make_weights(weights);
	auto weight = Solver::compute_weight(matches, weights);
	EXPECT_EQ(weight, 21);
}
#endif

TEST(SolverTest, TestEvaluateSolution)
{
	Solution solution;
	uint8_t board_input[] = {3,4,0,5,2,5,   1,2,5,2,1,4,   5,2,3,3,1,4,   3,4,3,5,5,4,   0,2,2,4,1,5};
	solution.board = make_board(reorder(board_input), Board::rows, Board::cols);
	Board old_board = solution.board;

	Weights weights;
	Solver::make_weights(weights);
	Solver::evaluate_solution(solution);

	EXPECT_EQ(old_board, solution.board);

#if 0
	EXPECT_TRUE(solution.matches.size() >= 4);
	EXPECT_EQ(solution.matches.size(), 3);
	EXPECT_EQ(solution.matches[0].type, 5);
	EXPECT_EQ(solution.matches[0].count, 3);
	EXPECT_EQ(solution.matches[1].type, 6);
	EXPECT_EQ(solution.matches[1].count, 3);
	EXPECT_EQ(solution.matches[2].type, 2);
	EXPECT_EQ(solution.matches[2].count, 3);
	EXPECT_EQ(solution.weight, 4.5);
#endif
}

TEST(SolverTest, TestSolveBoardStep)
{
	Weights weights;
	Solver::make_weights(weights);
	Solution solution;
	uint8_t board_input[] = {5,5,1,5,5,2,   0,0,1,0,2,2,   0,2,0,1,4,5,   5,4,5,2,3,0,   3,2,2,2,2,4};
	solution.board = make_board(reorder(board_input), Board::rows, Board::cols);
	solution.cursor = Point<uint8_t>(0, 0);
	solution.init_cursor = Point<uint8_t>(0, 0);
	std::vector<Solution> solutions;
	solutions.push_back(solution);
	SolveState solve_state(30, 2, 0, solutions);

	Solver::solve_board_step(solve_state);
	//EXPECT_EQ(solve_state.solutions.size(), MAX_SOLUTIONS_COUNT);

	//{
	//	auto & solution = solutions[0];
	//	EXPECT_EQ(solution.cursor.x, 2);
	//	EXPECT_EQ(solution.cursor.y, 3);
	//	EXPECT_EQ(solution.matches.size(), 4);
	//	EXPECT_EQ(solution.paths.size(), 21);
	//	EXPECT_EQ(solution.weight, 26.687500);
	//}

	float last_wieght = 99999;
	//for (size_t i = 0; i < solutions.size(); i++)
	//{
	//	auto & solution = solutions[i];
	//	EXPECT_TRUE(last_wieght >= solution.weight);
	//	last_wieght = solution.weight;

	//	std::printf("[%3lu] cursor (%u, %u), matches %lu, paths %lu, weight %f\n",
	//		i,
	//		solution.cursor.x, solution.cursor.y,
	//		solution.matches.size(), solution.paths.size(), solution.weight);
	//}
	for (size_t i = 0; i < 3; i++)
	{
		auto & solution = solutions[i];
		EXPECT_TRUE(last_wieght >= solution.weight);
		last_wieght = solution.weight;

		std::printf("[%3lu] cursor (%u, %u), matches %lu, paths %lu, weight %f\n",
			i,
			solution.cursor.x, solution.cursor.y,
			solution.matches.size(), solution.paths.size(), solution.weight);

		for (auto & p : solution.paths)
		{
			std::cout << (int)p << " ";
		}
		std::cout << "\n";
	}
}

TEST(SolverTest, TestSolveBoard)
{
	uint8_t board_input[] = {3,3,1,3,1,1,
							 0,0,3,1,5,1,
							 5,3,5,0,3,0,
							 2,0,1,3,3,5,
							 3,3,2,5,2,5};

	std::vector<Solution> solutions;
	Weights weights;
	Solver::make_weights2(weights);

	Solution seed_solution;
	seed_solution.board = make_board(reorder(board_input), Board::rows, Board::cols);
	Solver::evaluate_solution(seed_solution);
	
	for (size_t i = 0; i < ROWS; i++)
	{
		for (size_t j = 0; j < COLS; j++)
		{
			Solution s = seed_solution;
			s.init_cursor = Point<uint8_t>(i, j);
			s.cursor = Point<uint8_t>(i, j);
			solutions.push_back(s);
		}
	}

	SolveState solve_state(30, 1, 0, solutions);

	Solver::solve_board_step(solve_state);
	for (size_t i = 0; i < solutions.size(); i++)
	{
		auto & solution = solutions[i];

		//std::printf("[%3lu] cursor (%u, %u), matches %lu, paths %lu, weight %f\n",
		//	i,
		//	solution.init_cursor.x, solution.init_cursor.y,
		//	solution.matches.size(), solution.paths.size(), solution.weight);

		//for (auto & p : solution.paths)
		//{
		//	std::cout << (int)p << " ";
		//}
		//std::cout << "\n";
	}
}
