#include "gtest/gtest.h"
#include <iostream>
#include <algorithm>
#include <thirdparty/json.hpp>
#include <core/text_color.h>
#include <matapath/gameplayer.h>
#include <matapath/match_result.h>
#include <matapath/rune.h>
#include <matapath/solver.h>
#include <matapath/io.hpp>

const std::string TESTCASE_DIR = "./tos_testcase/tos_tests";

namespace {

uint8_t * auto_board(const uint8_t input[], size_t n=30)
{
	static uint8_t * result = new uint8_t[n];
	for (size_t i = 0; i < n ; i++)
	{
		if (input[i] == 0)
			result[i] = 1;
		else if (input[i] == 1)
			result[i] = 0;
		else
			result[i] = input[i];
	}
	return result;
}

uint8_t * reorder(const uint8_t input[], size_t n=30)
{
	static uint8_t * result = new uint8_t[n];
	for (size_t i = 0; i < n ; i++)
	{
		result[i] = input[i] + 1;
	}
	return result;
}

#if 0
std::ostream & operator<<(std::ostream & os, const Board & board)
{
	const size_t total = board.rows * board.cols;
	os << "[ ";
	for (size_t r = 0; r < board.rows; r++)
	{
		if (r != 0) os << ", ";
		for (size_t c = 0; c < board.cols; c++)
		{
			if (c == 0) os << (int)board.data[r][c].rune.type;
			else os << ", " << (int)board.data[r][c].rune.type;
		}
	}
	os << " ]";
	return os;
}
#endif

std::string matches_info(const std::vector<Match> & matches)
{
	std::string result;
	std::stringstream ss;

	std::string names[] = {"", "水", "火", "木", "光", "暗", "心", "問"};

	for (size_t i = 1; true; i++)
	{
		uint8_t c = 0;
		ss << "[ ";
		for (auto & match : matches)
		{
			if (match.round == i)
			{
				c++;
				ss << names[match.type];
				ss << (int)match.count << " ";
			}
		}
		ss << "]";
		if (c == 0) break;
	}

	result = ss.str();
	result.resize(result.length() - 3);
	return result;
}

}

namespace sample
{

uint8_t board1[] = {1,2,2,3,1,3,3,0,0,1,0,2,2,2,3,3,1,3,3,3,0,1,2,3,3,0,1,0,0,2};
uint8_t board2[] = {0,0,3,1,2,5,5,2,1,0,0,2,3,4,1,5,3,1,0,5,2,5,1,1,4,4,3,4,1,3};
uint8_t board3[] = {1,2,2,4,3,1,0,4,5,5,0,2,0,2,1,0,5,0,2,5,1,1,4,5,5,0,5,3,0,4};

}

class ConfigSetter
{
public:
	virtual void setup()=0;
	virtual void teardown();
};

void ConfigSetter::teardown()
{
	GamingConfig & gc = GamingConfig::inst();
	GamingConfig new_gc;

	std::swap(gc, new_gc);
};

class Evaluater
{
public:
	virtual float evaluate(const MatchResult & match_result)=0;
};


class ScoreProfiler
{
public:
	ScoreProfiler(ConfigSetter & config_setter, Evaluater & evaluater)
		: config_setter(config_setter)
		, evaluater(evaluater)
		, local_runs(0)
		, local_eval_score_sum(0.0f)
	{}
	
	~ScoreProfiler()
	{
		std::cout << "local average eval_score: " << (local_eval_score_sum / local_runs) << "\n";
		std::cout << "failed count            : " << failed_count << "\n";
	}

	float run(Board board);

private:
	GamePlayer gameplayer;
	ConfigSetter & config_setter;
	Evaluater & evaluater;
	size_t case_counter = 1;
	
	static float solve_score;
	static float eval_score;
	static uint32_t runs;
	uint32_t local_runs;
	float local_eval_score_sum;
	int failed_count = 0;
};

float ScoreProfiler::solve_score = 0;
float ScoreProfiler::eval_score = 0;
uint32_t ScoreProfiler::runs = 0;


float ScoreProfiler::run(Board board)
{
	config_setter.setup();
	GamingConfig::inst().max_steps = 50;

    std::vector<Point<size_t>> paths;
	std::vector<Solution> solutions;

	std::cout << "\n";
	std::cout << text::blue("[ CASE   ") << text::blue(std::to_string(case_counter++)) << text::blue(" ]") << "\n";
	//std::cout << "init  board: " << board << "\n";
	gameplayer.calculate_path(board, paths, solutions); 
	MatchResult best(solutions[0].matches, solutions[0].board);

	//std::cout << "result board: " << solutions[0].board << "\n";

	float score = evaluater.evaluate(best);
	std::cout << "combo count: " << (int)best.combo_count << "\n";
	std::cout << "total steps: " << solutions[0].paths.size() << "\n";
	std::cout << "solve score: " << best.calculate_score() << "\n";
	if (score < 80)
	{
		std::cout << text::red("eval  score: ") << text::red(std::to_string(score)) << "\n";
		failed_count++;
	}
	else
	{
		std::cout << "eval  score: " << score << "\n";
	}
	
	ScoreProfiler::solve_score += best.calculate_score();
	ScoreProfiler::eval_score += score;
	ScoreProfiler::runs++;
	local_runs++;
	local_eval_score_sum += score;
	
	std::cout << "total solve score: " << ScoreProfiler::solve_score << "\n";
	std::cout << "global average eval_score: " << (ScoreProfiler::eval_score / ScoreProfiler::runs) << "\n";
	
	std::cout << "match  info: " << matches_info(solutions[0].matches) << "\n";
	//std::cout << "last  board: " << solutions[0].board << "\n";

#if 0
	for (size_t i = 1; i < solutions.size(); i++)
	{
		MatchResult mr(solutions[i].matches, solutions[i].board);
		float each_score = evaluater.evaluate(mr);
		if (each_score > score)
		{
			std::cout << i << " eval score: " << (int)each_score << " raw score: " << mr.calculate_score() << "\n";
		}
	}
#endif
	
	config_setter.teardown();
	return score;
}

class ScoreProfilerTest : public ::testing::Test
{
public:
	ScoreProfilerTest()
	{
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/simple02.png"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/simple03.png"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/simple04.png"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/simple05.png"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/simple06.png"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/simple07.png"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/simple08.png"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/simple09.png"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/simple10.png"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/simple11.png"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/simple12.png"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple01.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple02.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple03.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple04.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple05.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple06.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple07.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple08.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple09.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple10.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple11.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple12.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple13.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple14.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple15.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple16.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple17.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple18.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple19.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple20.PNG"));
		sample_boards.push_back(gameplayer.load_board(TESTCASE_DIR + "/apple21.PNG"));
	}
	
protected:
	GamePlayer gameplayer;
	std::vector<Board> sample_boards;
};


TEST_F(ScoreProfilerTest, CaseKeepFireEnhance)
{
	class KFESetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().keep_enhance[TosRune::RuneFire] = 1;
		}
	} config_setter;


	class KFEEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			const float MAX_COMBO = 10.0;
			return match_result.combo_count / MAX_COMBO * 100;
		}
	} evaluater;

	ScoreProfiler sp(config_setter, evaluater);
	
	gameplayer.debug_is_android_image = true;
	GamingConfig::inst().keep_enhance[TosRune::RuneFire] = 1;
	Board board = gameplayer.load_board(TESTCASE_DIR + "/android_keep_fe.png");
	std::cout << board;
	sp.run(board);
	gameplayer.debug_is_android_image = false;
}

TEST_F(ScoreProfilerTest, CaseHighCombo)
{
	class HighComboSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
		}
	} config_setter;


	class HighComboEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			const float MAX_COMBO = 10.0;
			return match_result.combo_count / MAX_COMBO * 100;
		}
	} evaluater;

	ScoreProfiler sp(config_setter, evaluater);
	
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CaseFireRangeAttack)
{
	class FireRangeSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().rune_attack_type[TosRune::RuneFire] = Multiple;
		}
	} config_setter;


	class FireRangeEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			if (match_result.rune_results[TosRune::RuneFire].max_link_count >= 5)
			{
				result += 51;
			}
			result += match_result.combo_count * 5;
			return result;
		}
	} evaluater;
	
	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CaseTwoLink)
{
	class TwoLinkSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().rune_attack_type[TosRune::RuneFire] = TwoLink;
			GamingConfig::inst().rune_attack_type[TosRune::RuneWood] = TwoLink;
			GamingConfig::inst().rune_attack_type[TosRune::RuneHeart] = TwoLink;
			EXPECT_TRUE(GamingConfig::inst().rune_attack_type[TosRune::RuneFire] != Multiple);
		}
	} config_setter;

	class TwoLinkEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			return match_result.combo_count / 12.0 * 100;
		}
	} evaluater;

	uint8_t board_data[] = {1,2,2,3,1,3,3,0,0,1,0,2,2,2,3,3,1,3,3,3,0,1,2,3,3,0,1,0,0,2};

	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CaseBabylon)
{
	class BabylonSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			for (int i = 0; i < 6; i++)
			{
				GamingConfig::inst().column_attack_type[i] = 4;
			}
		}
	} config_setter;

	class BabylonEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			for (int i = 0; i < 6; i++)
			{
				if (match_result.column_rune_count[i] >= 4)
				{
					result += 20;
				}
			}
			result += match_result.combo_count / 8.0 * 20;
			return std::min(100.0f, result);
		}
	} evaluater;

	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CaseBottomAll)
{
	class BASetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().has_lowest_row = true;
		}
	} config_setter;


	class BAEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			if (match_result.lowest_rune_count >= 6)
			{
				result += 60;
			}
			if (match_result.combo_count >= 5)
			{
				result += std::min(40, (match_result.combo_count - 4) * 10);
			}
			return result;
		}
	} evaluater;

	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CaseKeepWater)
{
	class KWSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().rune_priority[TosRune::RuneWater] = PriorityType::Low;
		}
	} config_setter;


	class KWEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			if (match_result.rune_results[TosRune::RuneWater].count == 0)
			{
				result += 51;
			}
			result += match_result.combo_count * 5;
			return result;
		}
	} evaluater;

	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CaseNeedRune)
{
	class NRSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().rune_priority[TosRune::RuneWood] = PriorityType::High;
		}
	} config_setter;

	class NREval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			if (match_result.rune_results[TosRune::RuneWood].count > 0)
			{
				result += 51;
			}
			result += match_result.combo_count * 5;
			return result;
		}
	} evaluater;

	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CaseHasFirstRound)
{
	class HFRSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().rune_round_type[TosRune::RuneWood] = RoundType::First;
		}
	} config_setter;

	class HFRval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			if (match_result.rune_results[TosRune::RuneWood].has_first_round)
			{
				result += 51;
			}
			result += match_result.combo_count * 5;
			return result;
		}
	} evaluater;

	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CaseHasNoFirstRound)
{
	class NFRSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().rune_round_type[TosRune::RuneWood] = RoundType::Second;
		}
	} config_setter;

	class NFRval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			if (!match_result.rune_results[TosRune::RuneWood].has_first_round)
			{
				result += 51;
			}
			result += match_result.combo_count * 5;
			return result;
		}
	} evaluater;

	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CasePuzzleShield)
{
	class PSSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().puzzle_count = 5;
		}
	} config_setter;

	class PSEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			for (int i = 1; i < TosRune::RuneTypeEnd; i++)
			{
				if (match_result.rune_results[i].puzzle_count >= GamingConfig::inst().puzzle_count)
					result += 51;
			}
			result += match_result.combo_count * 5;
			return result;
		}
	} evaluater;

	ScoreProfiler sp(config_setter, evaluater);

	struct PuzzleSetter
	{
		Board operator()(const Board & board)
		{
			Board result(board);

			result.data[1][1].has_puzzle = true;
			result.data[1][2].has_puzzle = true;
			result.data[1][3].has_puzzle = true;
			result.data[2][2].has_puzzle = true;
			result.data[3][2].has_puzzle = true;
			return result;
		}

	} set_puzzle;

	Board board;
	for (auto & board : sample_boards)
	{
		sp.run(set_puzzle(board));
	}
//	board = set_puzzle(make_board(reorder(auto_board(sample::board1)), Board::rows, Board::cols));
//	sp.run(board);
//	board = set_puzzle(make_board(reorder(auto_board(sample::board2)), Board::rows, Board::cols));
//	sp.run(board);
//	board = set_puzzle(make_board(reorder(auto_board(sample::board3)), Board::rows, Board::cols));
//	sp.run(board);
}

TEST_F(ScoreProfilerTest, CaseComboCount)
{
	class NFRSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().combo_count = 5;
		}
	} config_setter;
	
	class NFRval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			if (match_result.combo_count == GamingConfig::inst().combo_count)
			{
				result += 100;
			}
			result += match_result.combo_count * 5;
			return result;
		}
	} evaluater;
	
	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CaseFirstComboCount)
{
	class NFRSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().first_combo_count = 5;
		}
	} config_setter;
	
	class NFRval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			if (match_result.first_combo_count == GamingConfig::inst().first_combo_count)
			{
				result += 51;
			}
			result += match_result.combo_count * 5;
			return result;
		}
	} evaluater;
	
	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CaseRunePriorityHigh)
{
	class PSSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
		}
	} config_setter;
	
	class PSEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 1;
			result += match_result.high_match_count * 20;
			result += match_result.combo_count * 5;
			return result;
		}
	} evaluater;
	
	ScoreProfiler sp(config_setter, evaluater);
	
	struct PrioritySetter
	{
		Board operator()(const Board & board)
		{
			Board result(board);
			
			result.data[1][1].rune.priority = High;
			result.data[3][3].rune.priority = High;
			result.data[4][5].rune.priority = High;
			
			return result;
		}
		
	} set_priority;
	
	Board board;
	for (auto & board : sample_boards)
	{
		sp.run(set_priority(board));
	}
}

TEST_F(ScoreProfilerTest, CaseRunePriorityLow)
{
	class PSSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
		}
	} config_setter;
	
	class PSEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			result = 75 - match_result.low_match_count * 25;
			result += match_result.combo_count * 3;
			return result;
		}
	} evaluater;
	
	ScoreProfiler sp(config_setter, evaluater);
	
	struct PrioritySetter
	{
		Board operator()(const Board & board)
		{
			Board result(board);
			
			result.data[1][1].rune.priority = Low;
			result.data[3][3].rune.priority = Low;
			result.data[4][5].rune.priority = Low;
			
			return result;
		}
		
	} set_priority;
	
	Board board;
	for (auto & board : sample_boards)
	{
		sp.run(set_priority(board));
	}
}

TEST_F(ScoreProfilerTest, CaseFireEntire)
{
	class FireRangeSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().rune_attack_type[TosRune::RuneFire] = Entire;
		}
	} config_setter;
	
	
	class FireRangeEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			
			if (match_result.rune_results[TosRune::RuneFire].count == match_result.rune_results[TosRune::RuneFire].total_count)
			{
				result += 51;
			}
			result += match_result.combo_count * 5;
			return result;
		}
	} evaluater;
	
	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}

TEST_F(ScoreProfilerTest, CaseFireEntireFirst)
{
	class FireRangeSetter : public ConfigSetter
	{
	public:
		virtual void setup()
		{
			GamingConfig::inst().rune_round_type[TosRune::RuneFire] = RoundType::First;
			GamingConfig::inst().rune_attack_type[TosRune::RuneFire] = Entire;
		}
	} config_setter;
	
	
	class FireRangeEval : public Evaluater
	{
	public:
		virtual float evaluate(const MatchResult & match_result)
		{
			float result = 0;
			
			if (match_result.rune_results[TosRune::RuneFire].has_first_round)
			{
				result += 31;
			}
			if (match_result.rune_results[TosRune::RuneFire].count == match_result.rune_results[TosRune::RuneFire].total_count)
			{
				result += 32;
			}
			result += match_result.combo_count * 4;
			return result;
		}
	} evaluater;
	
	ScoreProfiler sp(config_setter, evaluater);
	for (auto & board : sample_boards)
	{
		sp.run(board);
	}
}
