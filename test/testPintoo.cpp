#include "gtest/gtest.h"
#include <cstdio>
#include <vector>
#include <map>
#include <core/image.h>
#include <core/image_p.h>
#include <core/matcher.h>
#include <io/image.hpp>
#include <matapath/layout.h>

using namespace nlohmann;

const std::string SCREEN_DIR = "./tos_testcase/tos_screen";
const std::string TESTCASE_DIR = "./tos_testcase/tos_tests";


namespace next
{
	
float gray_avg(cv::Mat image)
{
	cv::cvtColor(image, image, CV_BGR2GRAY);
	cv::Scalar avg = cv::mean(image);
	return static_cast<float>(avg[0]);
}

	
class PintooFinder
{
public:
	virtual std::vector<Point<uint8_t>> find(const Image & board, const LayoutManager & layout_manager)=0;
};

class TosPintooFinder : public PintooFinder
{
public:
	virtual std::vector<Point<uint8_t>> find(const Image & board, const LayoutManager & layout_manager)
	{
		std::vector<Point<uint8_t>> result;
		
		// horizontal-borders
		for (size_t j = 0; j < layout_manager.n_cols(); j++)
		{
			bool is_open = false;
			for (size_t i = 0; i < layout_manager.n_rows(); i++)
			{
				auto border = layout_manager.hborder(i, j);
				Image img_border = board.crop(border.x, border.y, border.width, border.height);
				//std::stringstream ss;
				//ss << "hborder_" << i << "_" << j << ".png";
				//img_border.save(ss.str());
				
				float value = gray_avg(img_border._pimpl->mat);
				//printf("%d %d %f\n", i, j, value);
				if (value > 80.0f)
				{
					is_open = !is_open;
				}
				
				if (is_open)
				{
					result.push_back(Point<uint8_t>(i, j));
				}
			}
		}
		if (result.size() < 5 || result.size() > 7)
		{
			result.clear();
		}
		
		if (!result.empty())
		{
			if (!all_chained(result))
			{
				result.clear();
			}
		}
		
		return result;
	}
	
private:
	bool all_chained(const std::vector<Point<uint8_t>> & pintoos)
	{
		size_t j;
		for (size_t i = 0; i < pintoos.size(); i++)
		{
			for (j = 0; j < pintoos.size(); j++)
			{
				if (i == j) continue;
				if (near(pintoos[i], pintoos[j]))
				{
					break;
				}
			}
			if (j == pintoos.size())
			{
				return false;
			}
		}
		return true;
	}
	
	template <class P>
	inline bool near(const P & a, const P & b)
	{
		int dx = a.x - b.x;
		int dy = a.y - b.y;
		return (dx * dx + dy * dy == 1);
	}
	
private:
	float gray_avg(cv::Mat image)
	{
		cv::cvtColor(image, image, CV_BGR2GRAY);
		cv::Scalar avg = cv::mean(image);
		return static_cast<float>(avg[0]);
	}
};

}


class PintooTest: public ::testing::Test
{
public:
	PintooTest()
		: layout_manager(std::make_shared<TosGameLayout>())
	{
	}

protected:
	virtual void SetUp()
	{
		layout_manager.set_rows_count(5);
		layout_manager.set_cols_count(6);
	}

	virtual void TearDown()
	{
	}
	
	void test_pintoo(const std::string & filename, const std::vector<std::vector<bool>> & answer)
	{
		Image img_board;
		{
			Image img(filename);
			layout_manager.set_resolution(img.width(), img.height());
			auto board = layout_manager.board();
			img_board = img.crop(board.x, board.y, board.width, board.height);
		}
		
		img_board.resize(32 * layout_manager.n_cols(), 32 * layout_manager.n_rows());
		layout_manager.reset_board_size(img_board.width(), img_board.height());
		
		std::vector<std::vector<bool>> test(layout_manager.n_rows());
		
		for (size_t i = 0; i < layout_manager.n_rows(); i++)
		{
			test[i].resize(layout_manager.n_cols(), false);
		}
		
		next::TosPintooFinder pintoo_finder;
		std::vector<Point<uint8_t>> pintoos = pintoo_finder.find(img_board, layout_manager);
		for (auto & pintoo : pintoos)
		{
			test[pintoo.x][pintoo.y] = true;
		}
		
		for (size_t i = 0; i < layout_manager.n_rows(); i++)
		{
			for (size_t j = 0; j < layout_manager.n_cols(); j++)
			{
				if (test[i][j] != answer[i][j])
				{
					std::cout << i << " " << j <<  " " << test[i][j] << "\n";
				}
				EXPECT_TRUE(test[i][j] == answer[i][j]);
			}
		}
	}
	
	void test_pintoo_android_menu(const std::string & filename, const std::vector<std::vector<bool>> & answer)
	{
		Image img_board;
		{
			Image fullscreen(filename);
			Image img = fullscreen.crop(0, 0, 1080, 1776);
			layout_manager.set_resolution(img.width(), img.height());
			auto board = layout_manager.board();
			img_board = img.crop(board.x, board.y, board.width, board.height);
		}
		img_board.save("anti1.png");
		img_board.resize(32 * layout_manager.n_cols(), 32 * layout_manager.n_rows());
		img_board.save("anti2.png");
		layout_manager.reset_board_size(img_board.width(), img_board.height());
		
		std::vector<std::vector<bool>> test(layout_manager.n_rows());
		
		for (size_t i = 0; i < layout_manager.n_rows(); i++)
		{
			test[i].resize(layout_manager.n_cols(), false);
		}
		
		next::TosPintooFinder pintoo_finder;
		std::vector<Point<uint8_t>> pintoos = pintoo_finder.find(img_board, layout_manager);
		for (auto & pintoo : pintoos)
		{
			test[pintoo.x][pintoo.y] = true;
		}
		
		bool should_print_board = false;
		for (size_t i = 0; i < layout_manager.n_rows(); i++)
		{
			for (size_t j = 0; j < layout_manager.n_cols(); j++)
			{
				if (test[i][j] != answer[i][j])
				{
					should_print_board = true;
					std::cout << i << " " << j <<  " " << test[i][j] << "\n";
				}
				EXPECT_TRUE(test[i][j] == answer[i][j]);
			}
		}
		
		
	}


protected:
	LayoutManager layout_manager;
};


#if 0
TEST_F(PintooTest, SerializedBorder)
{
	Image vborder("vborder.png");
	Image hborder("hborder.png");

	json vborder_json;
	json hborder_json;
	vborder_json << vborder;
	hborder_json << hborder;

	std::cout << vborder_json.dump() << std::endl;
	std::cout << hborder_json.dump() << std::endl;
}
#endif

TEST_F(PintooTest, CutOffHBorder)
{
	Image img_board;
	{
		Image img(TESTCASE_DIR + "/pintoo02.PNG");
		layout_manager.set_resolution(img.width(), img.height());
		auto board = layout_manager.board();
		img_board = img.crop(board.x, board.y, board.width, board.height);
	}
	img_board.resize(32 * layout_manager.n_cols(), 32 * layout_manager.n_rows());
	layout_manager.reset_board_size(img_board.width(), img_board.height());
	img_board.save("hborder_screen.png");

	// horizontal-borders
	for (size_t j = 0; j < layout_manager.n_cols(); j++)
	{
		for (size_t i = 0; i < layout_manager.n_rows() + 1; i++)
		{
			auto border = layout_manager.hborder(i, j);
			Image img_border = img_board.crop(border.x, border.y, border.width, border.height);
			std::cout << next::gray_avg(img_border._pimpl->mat) << " ";
			//img_border.save("hb_" + std::to_string(i) + "_" + std::to_string(j) + ".png");
		}
		std::cout << std::endl;
	}
}

TEST_F(PintooTest, CutOffVBorder)
{
	Image img_board;
	{
		Image img(SCREEN_DIR + "/pingtu02.jpg");
		layout_manager.set_resolution(img.width(), img.height());
		auto board = layout_manager.board();
		img_board = img.crop(board.x, board.y, board.width, board.height);
	}
	img_board.resize(32 * layout_manager.n_cols(), 32 * layout_manager.n_rows());
	layout_manager.reset_board_size(img_board.width(), img_board.height());
	img_board.save("vborder_screen.png");

	// vertical-borders
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		for (size_t j = 0; j < layout_manager.n_cols(); j++)
		{
			auto border = layout_manager.vborder(i, j);
			Image img_border = img_board.crop(border.x, border.y, border.width, border.height);
			//img_border.save("vb_" + std::to_string(i) + "_" + std::to_string(j) + ".png");
		}
	}
}

TEST_F(PintooTest, CalcuatePintoo)
{
	Image img_board;
	{
		Image img(SCREEN_DIR + "/pingtu02.jpg");
		layout_manager.set_resolution(img.width(), img.height());
		auto board = layout_manager.board();
		img_board = img.crop(board.x, board.y, board.width, board.height);
	}
	img_board.resize(32 * layout_manager.n_cols(), 32 * layout_manager.n_rows());
	layout_manager.reset_board_size(img_board.width(), img_board.height());
	
	std::vector<std::vector<bool>> test(layout_manager.n_rows());
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		test[i].resize(layout_manager.n_cols(), false);
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
	answer[2][2] = true;
	answer[2][4] = true;
	answer[3][2] = true;
	answer[3][3] = true;
	answer[3][4] = true;
	answer[4][2] = true;
	answer[4][4] = true;
	
	next::TosPintooFinder pintoo_finder;
	std::vector<Point<uint8_t>> pintoos = pintoo_finder.find(img_board, layout_manager);
	for (auto & pintoo : pintoos)
	{
		test[pintoo.x][pintoo.y] = true;
	}
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		for (size_t j = 0; j < layout_manager.n_cols(); j++)
		{
			EXPECT_TRUE(test[i][j] == answer[i][j]);
		}
	}
}

TEST_F(PintooTest, CalcuatePintoo0)
{
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
	answer[2][2] = true;
	answer[2][4] = true;
	answer[3][2] = true;
	answer[3][3] = true;
	answer[3][4] = true;
	answer[4][2] = true;
	answer[4][4] = true;

	test_pintoo(SCREEN_DIR + "/pingtu02.jpg", answer);
}

TEST_F(PintooTest, CalcuatePintoo1)
{
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
	answer[0][2] = true;
	answer[0][3] = true;
	answer[0][4] = true;
	answer[1][3] = true;
	answer[2][3] = true;
	
	test_pintoo(TESTCASE_DIR + "/pintoo01.PNG", answer);
}

TEST_F(PintooTest, CalcuatePintoo2)
{
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
	answer[2][1] = true;
	answer[3][1] = true;
	answer[4][0] = true;
	answer[4][1] = true;
	answer[4][2] = true;
	
	test_pintoo(TESTCASE_DIR + "/pintoo02.PNG", answer);
}

TEST_F(PintooTest, CalcuatePintoo3)
{
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
	answer[1][0] = true;
	answer[1][1] = true;
	answer[1][2] = true;
	answer[1][3] = true;
	answer[1][4] = true;
	answer[1][5] = true;
	
	test_pintoo(TESTCASE_DIR + "/pintoo03.PNG", answer);
}

TEST_F(PintooTest, CalcuatePintoo4)
{
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
	answer[0][5] = true;
	answer[1][5] = true;
	answer[2][5] = true;
	answer[3][5] = true;
	answer[4][5] = true;
	
	test_pintoo(TESTCASE_DIR + "/pintoo04.PNG", answer);
}


TEST_F(PintooTest, CalcuatePintoo5)
{
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
	answer[0][0] = true;
	answer[0][1] = true;
	answer[0][2] = true;
	answer[0][3] = true;
	answer[0][4] = true;
	answer[0][5] = true;
	
	test_pintoo(TESTCASE_DIR + "/pintoo05.PNG", answer);
}

TEST_F(PintooTest, CalcuatePintoo6)
{
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
	answer[0][2] = true;
	answer[0][3] = true;
	answer[0][4] = true;
	answer[1][4] = true;
	answer[2][4] = true;
	
	test_pintoo(TESTCASE_DIR + "/pintoo06.PNG", answer);
}

TEST_F(PintooTest, CalcuateAntiPintoo1)
{
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
	test_pintoo_android_menu(TESTCASE_DIR + "/antipintoo1.png", answer);
}

TEST_F(PintooTest, CalcuateAndroidPintoo1)
{
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
		test_pintoo_android_menu(TESTCASE_DIR + "/android-pintoo1.png", answer);
}

TEST_F(PintooTest, CalcuateAndroidPintoo2)
{
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
	answer[0][1] = true;
	answer[0][2] = true;
	answer[0][3] = true;
	answer[1][2] = true;
	answer[2][2] = true;
	
	test_pintoo_android_menu(TESTCASE_DIR + "/android-pintoo2.png", answer);
}

TEST_F(PintooTest, CalcuateAndroidPintoo3)
{
	std::vector<std::vector<bool>> answer(layout_manager.n_rows());
	
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		answer[i].resize(layout_manager.n_cols(), false);
	}
	
	test_pintoo_android_menu(TESTCASE_DIR + "/android-pintoo3.png", answer);
}

