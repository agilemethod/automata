#include "gtest/gtest.h"
#include "core/image.h"
#include <cstdio>

const static std::string SAMPLE_IMAGE = "sample.jpg";

class ImageTest : public ::testing::Test
{
public:
	virtual void SetUp()
	{
		image.load(SAMPLE_IMAGE);
	}

	virtual void TearDown()
	{
	}

protected:
	Image image;
};

TEST(ImageTestPure, Open)
{
	auto img = Image(SAMPLE_IMAGE);
	EXPECT_TRUE(img.width() > 0);
	EXPECT_TRUE(img.height() > 0);
}

TEST_F(ImageTest, WidthAndHeight)
{
	EXPECT_EQ(image.width(), 416);
	EXPECT_EQ(image.height(), 416);
}

TEST_F(ImageTest, Save)
{
	image.save("test_" + SAMPLE_IMAGE);
	Image image2("test_" + SAMPLE_IMAGE);
	EXPECT_EQ(image.width(), image2.width());
	EXPECT_EQ(image.height(), image2.height());
}

TEST_F(ImageTest, CopyCtorImage)
{
	Image image2(image);
	EXPECT_EQ(image.width(), image2.width());
	EXPECT_EQ(image.height(), image2.height());
}

TEST_F(ImageTest, PixelFormat)
{
	EXPECT_EQ(image.pixel_format(), PixelFormat::PF_8U_3C);
}

TEST_F(ImageTest, CopyByteArrayImage)
{
	Image image2(image.data(), image.width(), image.height(), image.pixel_format());
	EXPECT_EQ(image.width(), image2.width());
	EXPECT_EQ(image.height(), image2.height());
}

TEST_F(ImageTest, Crop)
{
	Image image2 = image.crop(100, 100, 256, 256);
	image2.save("crop_" + SAMPLE_IMAGE);
	EXPECT_EQ(image2.width(), 256);
	EXPECT_EQ(image2.height(), 256);
}

TEST_F(ImageTest, Resize)
{
	Image image2 = image.crop(100, 100, 256, 256);
	image2.resize(128, 128);
	EXPECT_EQ(image2.width(), 128);
	EXPECT_EQ(image2.height(), 128);
	image.save("resize_1_" + SAMPLE_IMAGE);
	image2.save("resize_2_" + SAMPLE_IMAGE);
}

TEST_F(ImageTest, Gray)
{
	EXPECT_EQ(image.pixel_format(), PixelFormat::PF_8U_3C);
	image.gray();
	EXPECT_EQ(image.pixel_format(), PixelFormat::PF_8U_1C);
}

TEST_F(ImageTest, Gray2)
{
	EXPECT_EQ(image.pixel_format(), PixelFormat::PF_8U_3C);
	image.gray();
	image.gray();
	EXPECT_EQ(image.pixel_format(), PixelFormat::PF_8U_1C);
}

