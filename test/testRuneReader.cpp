#include "gtest/gtest.h"
#include <memory>
#include <string>
#include <core/image.h>
#include <core/matcher.h>
#include <matapath/rune.h>
#include <matapath/rune_provider.h>
#include <matapath/layout.h>

const std::string SAMPLE_DIR = "./tos_samples";
const std::string TESTCASE_DIR = "./tos_testcase/tos_tests"; 
const std::string SCREEN_DIR = "./tos_testcase/tos_screen"; 

#if 0

TEST(RuneReaderTest, RuneSample)
{
	Image image(SAMPLE_DIR + "/water_normal.png");
	Rune rune(TosRune::RuneWater, TosRune::PropNormal);
	RuneSample rune_sample(image, rune);
	
	Image image_test = rune_sample.image();
	EXPECT_TRUE(image_test.width() == image.width());
	EXPECT_TRUE(image_test.height() == image.height());
	EXPECT_TRUE(rune_sample.rune().type == rune.type);
	EXPECT_TRUE(rune_sample.rune().property == rune.property);
}

TEST(RuneReaderTest, RuneProvider)
{
	enum { SampleCount = 6 };

	TosRuneProvider tos_rune_provider;
	RuneProvider & rune_provider = tos_rune_provider;

	rune_provider.load();
	EXPECT_TRUE(rune_provider.sample_count() >= SampleCount);

	Size<std::size_t> sample_size(
		rune_provider.sample(0).image().width(),
		rune_provider.sample(0).image().height());

	auto rune_sample_0 = rune_provider.sample(0);
	EXPECT_TRUE(rune_sample_0.rune().type == TosRune::RuneWater);
	EXPECT_TRUE(rune_sample_0.rune().property == TosRune::PropNormal);

	auto rune_sample_1 = rune_provider.sample(1);
	EXPECT_TRUE(rune_sample_1.rune().type == TosRune::RuneFire);
	EXPECT_TRUE(rune_sample_1.rune().property == TosRune::PropNormal);

	auto rune_sample_2 = rune_provider.sample(2);
	EXPECT_TRUE(rune_sample_2.rune().type == TosRune::RuneWood);
	EXPECT_TRUE(rune_sample_2.rune().property == TosRune::PropNormal);

#if 0
	for (size_t i = 0; i < rune_provider.sample_count(); i++)
	{
		auto rune_sample = rune_provider.sample(i);
		EXPECT_TRUE(rune_sample.image().width() == sample_size.width);
		EXPECT_TRUE(rune_sample.image().height() == sample_size.height);
	}
#endif

	EXPECT_ANY_THROW(rune_provider.sample(rune_provider.sample_count()));
}


TEST(RuneReaderTest, SimpleCase1)
{
	TosRuneProvider rune_provider;
	rune_provider.load();
	std::string test_rune = TESTCASE_DIR + "/rune1.png";
    Image target_rune(test_rune);
    
    Matcher matcher;
    matcher.set_match_method(MatchMethod::Ccoeff());
    float best_value = 0;
    size_t best_sample = 0;
    for (size_t i = 0; i < rune_provider.sample_count(); i++)
    {
        auto sample_rune = rune_provider.sample(i).image();
        sample_rune.resize(target_rune.width(), target_rune.height());
        auto result = matcher.match(sample_rune, target_rune);
        float value = result.max();
        if (value > best_value)
        {
            best_value = value;
            best_sample = i;
        }
    }

	Rune result = rune_provider.sample(best_sample).rune();
	EXPECT_TRUE(result.type == static_cast<uint8_t>(TosRune::RuneWater));
	EXPECT_TRUE(result.property == static_cast<uint8_t>(TosRune::PropNormal));
}


TEST(RuneReaderTest, SimpleCase2)
{
	TosRuneProvider rune_provider;
	rune_provider.load();
	std::string test_rune = TESTCASE_DIR + "/rune2.png";
	Image target_rune(test_rune);

	Matcher matcher;
	matcher.set_match_method(MatchMethod::Ccoeff());
	float best_value = 0;
	size_t best_sample = 0;
	for (size_t i = 0; i < rune_provider.sample_count(); i++)
	{
		auto sample_rune = rune_provider.sample(i).image();
        sample_rune.resize(target_rune.width(), target_rune.height());
		auto result = matcher.match(sample_rune, target_rune);
		float value = result.max();
		if (value > best_value)
		{
			best_value = value;
			best_sample = i;
		}
	}

	Rune result = rune_provider.sample(best_sample).rune();
	EXPECT_TRUE(result.type == static_cast<uint8_t>(TosRune::RuneFire));
	EXPECT_TRUE(result.property == static_cast<uint8_t>(TosRune::PropNormal));
}


TEST(RuneReaderTest, SimpleCase3)
{
	TosRuneProvider rune_provider;
	rune_provider.load();
	std::string test_rune = TESTCASE_DIR + "/rune3.png";
    Image target_rune(test_rune);
    
    Matcher matcher;
    matcher.set_match_method(MatchMethod::Ccoeff());
    float best_value = 0;
    size_t best_sample = 0;
    for (size_t i = 0; i < rune_provider.sample_count(); i++)
    {
        auto sample_rune = rune_provider.sample(i).image();
        sample_rune.resize(target_rune.width(), target_rune.height());
        auto result = matcher.match(sample_rune, target_rune);
        float value = result.max();
        if (value > best_value)
        {
            best_value = value;
            best_sample = i;
        }
    }

	Rune result = rune_provider.sample(best_sample).rune();
	EXPECT_TRUE(result.type == static_cast<uint8_t>(TosRune::RuneWood));
	EXPECT_TRUE(result.property == static_cast<uint8_t>(TosRune::PropNormal));
}

TEST(RuneReaderTest, SimpleCase4)
{
	TosRuneProvider rune_provider;
	rune_provider.load();
	std::string test_rune = TESTCASE_DIR + "/rune4.png";
    Image target_rune(test_rune);
    
    Matcher matcher;
    matcher.set_match_method(MatchMethod::Ccoeff());
    float best_value = 0;
    size_t best_sample = 0;
    for (size_t i = 0; i < rune_provider.sample_count(); i++)
    {
        auto sample_rune = rune_provider.sample(i).image();
        sample_rune.resize(target_rune.width(), target_rune.height());
        auto result = matcher.match(sample_rune, target_rune);
        float value = result.max();
        if (value > best_value)
        {
            best_value = value;
            best_sample = i;
        }
    }

	Rune result = rune_provider.sample(best_sample).rune();
	EXPECT_TRUE(result.type == static_cast<uint8_t>(TosRune::RuneLight));
	EXPECT_TRUE(result.property == static_cast<uint8_t>(TosRune::PropNormal));
}


TEST(RuneReaderTest, SimpleCase5)
{
	TosRuneProvider rune_provider;
	rune_provider.load();
	std::string test_rune = TESTCASE_DIR + "/rune5.png";
    Image target_rune(test_rune);
    
    Matcher matcher;
    matcher.set_match_method(MatchMethod::Ccoeff());
    float best_value = 0;
    size_t best_sample = 0;
    for (size_t i = 0; i < rune_provider.sample_count(); i++)
    {
        auto sample_rune = rune_provider.sample(i).image();
        sample_rune.resize(target_rune.width(), target_rune.height());
        auto result = matcher.match(sample_rune, target_rune);
        float value = result.max();
        if (value > best_value)
        {
            best_value = value;
            best_sample = i;
        }
    }

	Rune result = rune_provider.sample(best_sample).rune();
	EXPECT_TRUE(result.type == static_cast<uint8_t>(TosRune::RuneDark));
	EXPECT_TRUE(result.property == static_cast<uint8_t>(TosRune::PropNormal));
}


TEST(RuneReaderTest, SimpleCase6)
{
	TosRuneProvider rune_provider;
	rune_provider.load();
	std::string test_rune = TESTCASE_DIR + "/rune6.png";
    Image target_rune(test_rune);
    
    Matcher matcher;
    matcher.set_match_method(MatchMethod::Ccoeff());
    float best_value = 0;
    size_t best_sample = 0;
    for (size_t i = 0; i < rune_provider.sample_count(); i++)
    {
        auto sample_rune = rune_provider.sample(i).image();
        sample_rune.resize(target_rune.width(), target_rune.height());
        auto result = matcher.match(sample_rune, target_rune);
        float value = result.max();
        if (value > best_value)
        {
            best_value = value;
            best_sample = i;
        }
    }

	Rune result = rune_provider.sample(best_sample).rune();
	EXPECT_TRUE(result.type == static_cast<uint8_t>(TosRune::RuneHeart));
	EXPECT_TRUE(result.property == static_cast<uint8_t>(TosRune::PropNormal));
}

TEST(RuneReaderTest, TosGameCase)
{
	LayoutManager layout_manager(std::make_shared<TosGameLayout>());
	layout_manager.set_rows_count(5);
	layout_manager.set_cols_count(6);
	Image img(SCREEN_DIR + "/tos_iphone.png");
	layout_manager.set_resolution(img.width(), img.height());
	auto board = layout_manager.board();

	TosRuneProvider rune_provider;
	rune_provider.load();
	Matcher matcher;
	matcher.set_match_method(MatchMethod::Ccoeff());
	auto sample_size = rune_provider.sample_size();
	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		for (size_t j = 0; j < layout_manager.n_cols(); j++)
		{
			auto cell = layout_manager.cell(i, j);
			Image img_cell = img.crop(cell.x, cell.y, cell.width, cell.height);
			img_cell.resize(sample_size.width, sample_size.height);

			float best_value = 0;
			size_t best_sample = 0;
			for (size_t i = 0; i < rune_provider.sample_count(); i++)
			{
				auto sample_rune = rune_provider.sample(i).image();
                sample_rune.resize(sample_size.width, sample_size.height);
				auto result = matcher.match(sample_rune, img_cell);
				float value = result.max();
				if (value > best_value)
				{
					best_value = value;
					best_sample = i;
				}
			}
			Rune result = rune_provider.sample(best_sample).rune();
			std::cout << (uint16_t)result.type << " ";
		}
		std::cout << "\n";
	}
}

TEST(RuneReaderTest, RunePropCase)
{
	const std::string dedicated_file = "ice01.PNG";
	const size_t dedicated_row = 3;
	const size_t dedicated_col = 1;

	std::printf("%s row: %d col: %d\n", dedicated_file.c_str(), dedicated_row, dedicated_col);

	LayoutManager layout_manager(std::make_shared<TosGameLayout>());
	layout_manager.set_rows_count(5);
	layout_manager.set_cols_count(6);
	Image img(TESTCASE_DIR + "/" + dedicated_file);
	layout_manager.set_resolution(img.width(), img.height());

	auto dedicated_cell = layout_manager.cell(dedicated_row, dedicated_col);
	Image dedicated_rune = img.crop(dedicated_cell.x, dedicated_cell.y, dedicated_cell.width, dedicated_cell.height);
	dedicated_rune.save("dedicated_" + std::to_string(dedicated_row) + "_" + std::to_string(dedicated_col) + ".jpg");
	dedicated_rune.resize(32, 32);

	TosRuneProvider rune_provider;
	rune_provider.load();
	Matcher matcher;
	matcher.set_match_method(MatchMethod::CcoeffNormed());

	for (size_t i = 0; i < rune_provider.sample_count(); i++)
	{
		auto sample_rune = rune_provider.sample(i).image();
		sample_rune.resize(32, 32);
		auto result = matcher.match(sample_rune, dedicated_rune);
		std::cout << i << ": " << (float)result.max() << "\n";
	}
}

TEST(RuneReaderTest, RunePropCase2)
{
	const std::string dedicated_file = "ice01.PNG";
	const size_t dedicated_row = 3;
	const size_t dedicated_col = 2;

	std::printf("%s row: %d col: %d\n", dedicated_file.c_str(), dedicated_row, dedicated_col);

	LayoutManager layout_manager(std::make_shared<TosGameLayout>());
	layout_manager.set_rows_count(5);
	layout_manager.set_cols_count(6);
	Image img(TESTCASE_DIR + "/" + dedicated_file);
	layout_manager.set_resolution(img.width(), img.height());

	auto dedicated_cell = layout_manager.cell(dedicated_row, dedicated_col);
	Image dedicated_rune = img.crop(dedicated_cell.x, dedicated_cell.y, dedicated_cell.width, dedicated_cell.height);
	dedicated_rune.save("dedicated_" + std::to_string(dedicated_row) + "_" + std::to_string(dedicated_col) + ".jpg");
	dedicated_rune.resize(32, 32);

	TosRuneProvider rune_provider;
	rune_provider.load();
	Matcher matcher;
	matcher.set_match_method(MatchMethod::CcoeffNormed());

	for (size_t i = 0; i < rune_provider.sample_count(); i++)
	{
		auto sample_rune = rune_provider.sample(i).image();
		sample_rune.resize(32, 32);
		auto result = matcher.match(sample_rune, dedicated_rune);
		std::cout << i << ": " << (float)result.max() << "\n";
	}
}

#endif 

#if 0

namespace util
{

std::string str_type(uint8_t n)
{
	static std::string lookup[] = {"", "水", "火", "木", "光", "暗", "心", "問", "未"};
	return lookup[n];
}

std::string str_prop(uint8_t n)
{
	static std::string lookup[] = {"", "普", "強", "燃", "風", "冰", "凍", "閃", "蝕", "鎖", "束", "隱"};
	return lookup[n];
}

std::vector<std::vector<Image>> extract_cells(const Image & screen_img)
{
	std::vector<std::vector<Image>> result;

	LayoutManager layout_manager(std::make_shared<TosGameLayout>());
	layout_manager.set_rows_count(5);
	layout_manager.set_cols_count(6);
	layout_manager.set_resolution(screen_img.width(), screen_img.height());

	for (size_t i = 0; i < layout_manager.n_rows(); i++)
	{
		std::vector<Image> each_row;
		for (size_t j = 0; j < layout_manager.n_cols(); j++)
		{
			auto cell = layout_manager.cell(i, j);
			Image cell_img = screen_img.crop(cell.x, cell.y, cell.width, cell.height);
			cell_img.resize(32, 32);
			each_row.push_back(cell_img);
		}
		result.push_back(each_row);
	}

	return result;
}

uint8_t find_rune_type(const Image & cell_img, float & match_value)
{
	uint8_t result = TosRune::RuneTypeEnd;

	Matcher matcher;
	matcher.set_match_method(MatchMethod::CcoeffNormed());
	TosRuneProvider rune_provider;
	rune_provider.load();
	
	float best_value = 0;
	size_t best_sample = 0;
	std::vector<float> match_scores;
	match_scores.resize(rune_provider.sample_count(), 0.0f);

	for (size_t i = 0; i < rune_provider.sample_count(); i++)
	{
		Image sample_img = rune_provider.sample(i).image();
		sample_img.resize(32, 32);
		auto match_result = matcher.match(sample_img, cell_img);
		float value = match_result.max();
		match_scores[i] = value;
		if (value > best_value)
		{
			best_value = value;
			best_sample = i;
		}
	}

	std::nth_element(match_scores.begin(), match_scores.begin() + 1, match_scores.end(), std::greater<float>());
	float second_value = match_scores[1];
	float diff = best_value - second_value;
	if (diff > 0.03)
	{
		result = rune_provider.sample(best_sample).rune().type;
	}
	else 
	{
		result = TosRune::RuneTypeEnd;
	}

	match_value = best_value;
	return result;
}

uint8_t find_rune_prop(const Image & cell_img, bool verbose=false)
{
	uint8_t result = TosRune::PropNormal;
	std::vector<Image> props;

	Image dummy("prop_dummy.png");
	dummy.resize(32, 32);
	Image enhanced("prop_enhanced.png");
	enhanced.resize(32, 32);
	Image weathering("prop_weathering.png");
	weathering.resize(32, 32);
	Image lightening("prop_lightening.png");
	lightening.resize(32, 32);
	Image black("prop_dummy.png");
	black.resize(32, 32);
	Image freezed_init("prop_freezed.png");
	freezed_init.resize(32, 32);
	Image freezed("prop_freezed.png");
	freezed.resize(32, 32);
	Image etched("prop_etched.png");
	etched.resize(32, 32);
	Image stolen("prop_lock.png");
	stolen.resize(32, 32);
	Image laser("prop_laser.png");
	laser.resize(32, 32);
	Image ignited("prop_ignited.png");
	laser.resize(32, 32);

	props.push_back(dummy);
	props.push_back(dummy);
	props.push_back(enhanced);
	props.push_back(ignited);
	props.push_back(weathering);
	props.push_back(freezed_init);
	props.push_back(freezed);
	props.push_back(lightening);
	props.push_back(etched);
	props.push_back(stolen);
	props.push_back(laser);

	Matcher matcher;
	matcher.set_match_method(MatchMethod::CcoeffNormed());

	float best_value = 0;
	for (size_t i = 2; i < props.size(); i++)
	{
		auto match_result = matcher.match(props[i], cell_img);
		float value = match_result.max();
		if (best_value < value)
		{
			best_value = value;
			result = i;
		}
		if (verbose)
		{
			std::cout << str_prop(i) << " " << value << "\n";
		}
	}

	return result;
}


}

TEST(RuneReaderTest, WildCase)
{
	const std::string filepath = "wild01.jpg";
	const size_t row = 1;
	const size_t col = 2;
	const uint8_t UNKNOWN_TYPE = TosRune::RuneTypeEnd;
	const uint8_t NORMAL_PROP = TosRune::PropNormal;
	const float THRESHOLD_VALUE = 0.8;

	Image screen_img(TESTCASE_DIR + "/" + filepath);
	std::vector<std::vector<Image>> cell_imgs = util::extract_cells(screen_img);
	Image cell_img = cell_imgs[row][col];

	uint8_t rune_type = UNKNOWN_TYPE;
	uint8_t rune_prop = NORMAL_PROP;

	float match_value;	
	rune_type = util::find_rune_type(cell_img, match_value);
	if (rune_type != UNKNOWN_TYPE)
	{
		if (match_value < THRESHOLD_VALUE)
		{
			rune_prop = util::find_rune_prop(cell_img);
		}
	}

	std::cout << "RuneType: " << util::str_type(rune_type) << "\n";
	std::cout << "RuneProp: " << util::str_prop(rune_prop) << "\n";
}

TEST(RuneReaderTest, FreezeCase)
{
	const std::string filepath = "aaaaa.png";
	const size_t row = 3;
	const size_t col = 0;
	const uint8_t UNKNOWN_TYPE = TosRune::RuneTypeEnd;
	const uint8_t NORMAL_PROP = TosRune::PropNormal;
	const float THRESHOLD_VALUE = 0.8;

	Image screen_img(TESTCASE_DIR + "/" + filepath);
	std::vector<std::vector<Image>> cell_imgs = util::extract_cells(screen_img);
	Image cell_img = cell_imgs[row][col];

	uint8_t rune_type = UNKNOWN_TYPE;
	uint8_t rune_prop = NORMAL_PROP;

	float match_value;	
	rune_type = util::find_rune_type(cell_img, match_value);
	if (rune_type != UNKNOWN_TYPE)
	{
		if (match_value < THRESHOLD_VALUE)
		{
			rune_prop = util::find_rune_prop(cell_img);
		}
	}

	std::cout << "RuneType: " << util::str_type(rune_type) << "\n";
	std::cout << "RuneProp: " << util::str_prop(rune_prop) << "\n";
}

TEST(RuneReaderTest, LighteningCase)
{
	const std::string filepath = "electric01.jpg";
	const size_t row = 1;
	const size_t col = 5;
	const uint8_t UNKNOWN_TYPE = TosRune::RuneTypeEnd;
	const uint8_t NORMAL_PROP = TosRune::PropNormal;
	const float THRESHOLD_VALUE = 0.8;

	Image screen_img(TESTCASE_DIR + "/" + filepath);
	std::vector<std::vector<Image>> cell_imgs = util::extract_cells(screen_img);
	Image cell_img = cell_imgs[row][col];

	uint8_t rune_type = UNKNOWN_TYPE;
	uint8_t rune_prop = NORMAL_PROP;

	float match_value;	
	rune_type = util::find_rune_type(cell_img, match_value);
	if (rune_type != UNKNOWN_TYPE)
	{
		if (match_value < THRESHOLD_VALUE)
		{
			rune_prop = util::find_rune_prop(cell_img, true);
		}
	}

	std::cout << "RuneType: " << util::str_type(rune_type) << "\n";
	std::cout << "RuneProp: " << util::str_prop(rune_prop) << "\n";
}

TEST(RuneReaderTest, Lightening2Case)
{
	const std::string filepath = "electric02.PNG";
	const size_t row = 0;
	const size_t col = 2;
	const uint8_t UNKNOWN_TYPE = TosRune::RuneTypeEnd;
	const uint8_t NORMAL_PROP = TosRune::PropNormal;
	const float THRESHOLD_VALUE = 0.8;

	Image screen_img(TESTCASE_DIR + "/" + filepath);
	std::vector<std::vector<Image>> cell_imgs = util::extract_cells(screen_img);
	Image cell_img = cell_imgs[row][col];

	uint8_t rune_type = UNKNOWN_TYPE;
	uint8_t rune_prop = NORMAL_PROP;

	float match_value;	
	rune_type = util::find_rune_type(cell_img, match_value);
	if (rune_type != UNKNOWN_TYPE)
	{
		if (match_value < THRESHOLD_VALUE)
		{
			rune_prop = util::find_rune_prop(cell_img, true);
		}
	}

	std::cout << "RuneType: " << util::str_type(rune_type) << "\n";
	std::cout << "RuneProp: " << util::str_prop(rune_prop) << "\n";
}

TEST(RuneReaderTest, LaserCase)
{
	const std::string filepath = "raser01.PNG";
	const size_t row = 4;
	const size_t col = 1;
	const uint8_t UNKNOWN_TYPE = TosRune::RuneTypeEnd;
	const uint8_t NORMAL_PROP = TosRune::PropNormal;
	const float THRESHOLD_VALUE = 0.8;

	Image screen_img(TESTCASE_DIR + "/" + filepath);
	std::vector<std::vector<Image>> cell_imgs = util::extract_cells(screen_img);
	Image cell_img = cell_imgs[row][col];

	uint8_t rune_type = UNKNOWN_TYPE;
	uint8_t rune_prop = NORMAL_PROP;

	float match_value;	
	rune_type = util::find_rune_type(cell_img, match_value);
	if (rune_type != UNKNOWN_TYPE)
	{
		if (match_value < THRESHOLD_VALUE)
		{
			rune_prop = util::find_rune_prop(cell_img, true);
		}
	}

	std::cout << "RuneType: " << util::str_type(rune_type) << "\n";
	std::cout << "RuneProp: " << util::str_prop(rune_prop) << "\n";
}

TEST(RuneReaderTest, IgnitedCase)
{
	const std::string filepath = "ignite01.jpg";
	const size_t row = 4;
	const size_t col = 4;
	const uint8_t UNKNOWN_TYPE = TosRune::RuneTypeEnd;
	const uint8_t NORMAL_PROP = TosRune::PropNormal;
	const float THRESHOLD_VALUE = 0.8;

	Image screen_img(TESTCASE_DIR + "/" + filepath);
	std::vector<std::vector<Image>> cell_imgs = util::extract_cells(screen_img);
	Image cell_img = cell_imgs[row][col];

	uint8_t rune_type = UNKNOWN_TYPE;
	uint8_t rune_prop = NORMAL_PROP;

	float match_value;	
	rune_type = util::find_rune_type(cell_img, match_value);
	if (rune_type != UNKNOWN_TYPE)
	{
		if (match_value < THRESHOLD_VALUE)
		{
			rune_prop = util::find_rune_prop(cell_img, true);
		}
	}

	std::cout << "RuneType: " << util::str_type(rune_type) << "\n";
	std::cout << "RuneProp: " << util::str_prop(rune_prop) << "\n";
}

TEST(RuneReaderTest, Ignited2Case)
{
	const std::string filepath = "ignite03.png";
	const size_t row = 0;
	const size_t col = 1;
	const uint8_t UNKNOWN_TYPE = TosRune::RuneTypeEnd;
	const uint8_t NORMAL_PROP = TosRune::PropNormal;
	const float THRESHOLD_VALUE = 0.8;

	Image screen_img(TESTCASE_DIR + "/" + filepath);
	std::vector<std::vector<Image>> cell_imgs = util::extract_cells(screen_img);
	Image cell_img = cell_imgs[row][col];

	uint8_t rune_type = UNKNOWN_TYPE;
	uint8_t rune_prop = NORMAL_PROP;

	float match_value;	
	rune_type = util::find_rune_type(cell_img, match_value);
	if (rune_type != UNKNOWN_TYPE)
	{
		if (match_value < THRESHOLD_VALUE)
		{
			rune_prop = util::find_rune_prop(cell_img, true);
		}
	}

	std::cout << "RuneType: " << util::str_type(rune_type) << "\n";
	std::cout << "RuneProp: " << util::str_prop(rune_prop) << "\n";
}

TEST(RuneReaderTest, Ignited3Case)
{
	const std::string filepath = "ignite03.png";
	const size_t row = 4;
	const size_t col = 1;
	const uint8_t UNKNOWN_TYPE = TosRune::RuneTypeEnd;
	const uint8_t NORMAL_PROP = TosRune::PropNormal;
	const float THRESHOLD_VALUE = 0.8;

	Image screen_img(TESTCASE_DIR + "/" + filepath);
	std::vector<std::vector<Image>> cell_imgs = util::extract_cells(screen_img);
	Image cell_img = cell_imgs[row][col];

	uint8_t rune_type = UNKNOWN_TYPE;
	uint8_t rune_prop = NORMAL_PROP;

	float match_value;	
	rune_type = util::find_rune_type(cell_img, match_value);
	if (rune_type != UNKNOWN_TYPE)
	{
		if (match_value < THRESHOLD_VALUE)
		{
			rune_prop = util::find_rune_prop(cell_img, true);
		}
	}

	std::cout << "RuneType: " << util::str_type(rune_type) << "\n";
	std::cout << "RuneProp: " << util::str_prop(rune_prop) << "\n";
}

#endif
