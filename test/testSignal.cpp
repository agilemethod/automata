#include "gtest/gtest.h"
#include <functional>
#include <core/signal.hpp>

using namespace std::placeholders;

struct EventHandler
{
	EventHandler() : last_event(0)
	{
	}

	void handle_event(uint8_t event)
	{
		last_event = event;
	}

	uint8_t last_event;
};

TEST(SignalTest, Connect)
{
	EventHandler event_handler;
	Signal<void(uint8_t)> on_clicked;
	on_clicked.connect(std::bind(&EventHandler::handle_event, &event_handler, _1));

	EXPECT_EQ(on_clicked.slot_counts(), 1);
}

TEST(SignalTest, Emit)
{
	EventHandler event_handler;
	Signal<void(uint8_t)> on_clicked;
	on_clicked.connect(std::bind(&EventHandler::handle_event, &event_handler, _1));

	EXPECT_EQ(on_clicked.slot_counts(), 1);
	on_clicked.emit(10);
	EXPECT_EQ(event_handler.last_event, 10);
}

TEST(SignalTest, MultiConnect)
{
	EventHandler event_handler;
	EventHandler event_handler1;
	EventHandler event_handler2;
	Signal<void(uint8_t)> on_clicked;
	on_clicked.connect(std::bind(&EventHandler::handle_event, &event_handler, _1));
	on_clicked.connect(std::bind(&EventHandler::handle_event, &event_handler1, _1));
	on_clicked.connect(std::bind(&EventHandler::handle_event, &event_handler2, _1));

	EXPECT_EQ(on_clicked.slot_counts(), 3);
	on_clicked.emit(10);
	EXPECT_EQ(event_handler.last_event, 10);
	EXPECT_EQ(event_handler1.last_event, 10);
	EXPECT_EQ(event_handler2.last_event, 10);
}

TEST(SignalTest, Disconnect)
{
	EventHandler event_handler;
	Signal<void(uint8_t)> on_clicked;
	size_t sid = on_clicked.connect(std::bind(&EventHandler::handle_event, &event_handler, _1));

	EXPECT_EQ(on_clicked.slot_counts(), 1);
	on_clicked.emit(10);
	EXPECT_EQ(event_handler.last_event, 10);
	event_handler.last_event = 0;

	on_clicked.disconnect(sid);
	EXPECT_EQ(on_clicked.slot_counts(), 0);
	on_clicked.emit(10);
	EXPECT_EQ(event_handler.last_event, 0);
}

TEST(SignalTest, Disconnect_all)
{
	EventHandler event_handler;
	EventHandler event_handler1;
	EventHandler event_handler2;
	Signal<void(uint8_t)> on_clicked;
	on_clicked.connect(std::bind(&EventHandler::handle_event, &event_handler, _1));
	on_clicked.connect(std::bind(&EventHandler::handle_event, &event_handler1, _1));
	on_clicked.connect(std::bind(&EventHandler::handle_event, &event_handler2, _1));

	EXPECT_EQ(on_clicked.slot_counts(), 3);
	on_clicked.disconnect_all();
	on_clicked.emit(10);
	EXPECT_EQ(on_clicked.slot_counts(), 0);
	EXPECT_EQ(event_handler.last_event, 0);
	EXPECT_EQ(event_handler1.last_event, 0);
	EXPECT_EQ(event_handler2.last_event, 0);
}
