#include "gtest/gtest.h"
#include <memory>
#include <functional>
#include <thirdparty/json.hpp>
#include <matapath/rune.h>
#include <matapath/basic_types.h>
#include <matapath/gameplayer.h>

using json = nlohmann::json;
using namespace std::placeholders;

namespace {

uint8_t * reorder(uint8_t input[], size_t n=30)
{
	for (size_t i = 0; i < n ; i++)
	{
		input[i]++;
	}
	return input;
}

Requirement create_tos_default_requirement()
{
	Requirement result;

	result.max_steps = 30;
	result.eight_direction_allowed = false;

	for (uint8_t i = TosRune::RuneWater; i < TosRune::RuneTypeEnd; i++)
	{
		RuneRequirement rune_requirement;
		rune_requirement.type = i;
		rune_requirement.config = RuneRequirement::Link3;
		result.rune_requirements.push_back(rune_requirement);
	}

	return result;
};

} // end of namespace


TEST(GamePlayerTest, TestLoadBoard)
{
	GamePlayer gameplayer;
	auto board = gameplayer.load_board();
	show_board(board);
}

TEST(GamePlayerTest, TestCalculatePath)
{
	PathMap previous_path;
	uint8_t board_input[] = {3,3,1,3,1,1,
							 0,0,3,1,5,1,
							 5,3,5,0,3,0,
							 2,0,1,3,3,5,
							 3,3,2,5,2,5};
	Board board = make_board(reorder(board_input), Board::rows, Board::cols);
	Requirement requirement = create_tos_default_requirement();
	GamePlayer gameplayer;
    std::vector<Point<size_t>> paths;
	std::vector<Solution> solution;
	auto pathmap = gameplayer.calculate_path(board, paths, solution);

	//for (auto p : pathmap)
	//{
	//	std::printf("%u %u\n", (uint32_t)p.x, (uint32_t)p.y);
	//}
}

void handle_requested(const std::string & request)
{
	//std::cout << request << std::endl;
}

TEST(GamePlayerTest, TestSerialization)
{
	GamePlayer gameplayer;
	auto board = gameplayer.load_board("./tos_testcase/tos_screen/tos_iphone.png");
	json board_json;
	board_json << board;
	std::cout << board_json.dump() << std::endl;
}

TEST(GamePlayerTest, TestDeserialization)
{
	std::string data_json = R"({"cells":[[{"puzzle":true,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":3}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":2,"type":2}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}}],[{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":6}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}}],[{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":5}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":3}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":5}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":3}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}}],[{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":6}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":3}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":3}}],[{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":6}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":6}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}},{"puzzle":true,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}}]]})";
	json board_json = json::parse(data_json);
	Board board;
	board_json >> board;
	EXPECT_EQ(board.data[0][0].has_puzzle, true);
	EXPECT_EQ(board.data[0][1].has_puzzle, false);
	EXPECT_EQ(board.data[4][5].has_puzzle, true);
}

TEST(GamePlayerTest, TestPuzzle)
{
	std::string command_text = R"({"request":"calculate_path","board":{"cells":[[{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":5}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":6}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}}],[{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}},{"puzzle":true,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":2,"type":3}},{"puzzle":true,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}},{"puzzle":true,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":6}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}}],[{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}},{"puzzle":true,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":5}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}}],[{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":3}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":5}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":5}},{"puzzle":true,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":1}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}}],[{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":2}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":6}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":5}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":4}},{"puzzle":false,"rune":{"enhanced":false,"matched":false,"moveable":true,"priority":0,"property":1,"type":3}}]]},"previous_path":[],"config":{"rune_priority":[0,0,0,0,0,0,0],"rune_round_type":[0,0,0,0,0,0,0],"rune_attack_type":[0,0,0,0,0,0,0],"link_count":[0],"column_attack_type":[1,1,1,1,1,1],"combo_count":0,"first_combo_count":0,"has_lowest_row":false,"max_steps":48,"eight_direction_allowed":false,"start_position":0,"ignited_path":6,"max_solutions_size":2880,"width":1080,"height":1776}})";
	
	//json command_json = json::parse(command_text);
	GamePlayer gameplayer;
	gameplayer.execute(command_text);
}

TEST(GamePlayerTest, TestGetAdId)
{
    json command;
    command["request"] = "get_ad_id";
    GamePlayer gameplayer;
    gameplayer.on_execute_done.connect(std::bind(handle_requested, _1));
    std::string respond = gameplayer.execute(command.dump());
    std::cout << respond << std::endl;
}


#if 0
TEST(GamePlayerTest, TestExecuteLoadBoard)
{
	json command;
	command["request"] = "load_board";
	GamePlayer gameplayer;
	gameplayer.on_execute_done.connect(std::bind(handle_requested, _1));
    std::string respond = gameplayer.execute(command.dump());
    std::cout << respond << std::endl;
}

TEST(GamePlayerTest, TestJsonConfigToGamingConfig)
{
    std::string json_string = "{\"rune_priority\":[0,0,0,0,0,0,0],\"rune_round_type\":[0,0,0,0,0,0,0],\"rune_attack_type\":[0,0,0,0,0,0,0],\"column_attack_type\":[1,1,1,1,1,1],\"combo_count\":0,\"first_combo_count\":0,\"has_lowest_row\":false,\"max_steps\":32,\"eight_direction_allowed\":false,\"start_position\":0,\"ignited_path\":6,\"max_solutions_size\":30,\"width\":1080,\"height\":1772}";
    json json_object = json::parse(json_string);
    json_object >> GamingConfig::inst();
    EXPECT_EQ(GamingConfig::inst().combo_count, 0);
    EXPECT_EQ(GamingConfig::inst().first_combo_count, 0);
}

TEST(GamePlayerTest, TestPeekAndPlay)
{
    GamePlayer gameplayer;
    gameplayer.peek_and_play("./tos_testcase/tos_screen/tos_iphone.png");
}


#endif
