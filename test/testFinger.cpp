#include "gtest/gtest.h"
#include "human/finger.h"
#include "core/gesture.h"

#include <vector>

class FingerAndGestureTest: public ::testing::Test
{
protected:
	virtual void SetUp() 
	{
	}

	virtual void TearDown()
	{
	}

protected:
	Gesture gesture;
};

TEST_F(FingerAndGestureTest, Tap)
{
	gesture.tap(100, 200, 1000.0);
}

TEST_F(FingerAndGestureTest, Swipe)
{
	gesture.swipe(100, 100, 200, 200, 1000.0);
}

TEST_F(FingerAndGestureTest, Navigate)
{
	typedef Point<size_t> Point;

	std::vector<Point> paths;
	paths.push_back(Point(100, 100));
	paths.push_back(Point(200, 200));
	paths.push_back(Point(300, 300));
	paths.push_back(Point(400, 400));

	gesture.navigate(paths, 100);
}
