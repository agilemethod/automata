cmake_minimum_required (VERSION 2.6)

project (Test)

# include gtest
enable_testing()
find_package(GTest REQUIRED)

set(TestCaseSrc
	testMain.cpp
	testImage.cpp
	testLayoutManager.cpp
	testMatcher.cpp
	testRuneReader.cpp
	testSolver.cpp
	testGamePlayer.cpp
	testRuneProfiler.cpp
	testSignal.cpp
	testScoreProfiler.cpp
	testPintoo.cpp
	testResource.cpp)
# note: testEye and testFinger were set to escape testing

add_executable (testAll ${TestCaseSrc})
target_link_libraries (testAll ${Automata_LIBS} ${Matapath_LIBS} ${OpenCV_LIBS} ${GTEST_LIBRARIES} ${PROTOBUF_LIBRARIES})

# add unit test
set(TestExecutable ${CMAKE_SOURCE_DIR}/bin/testAll --gtest_color=yes)
add_test(testImage ${TestExecutable} --gtest_filter=ImageTest*)
add_test(testEye ${TestExecutable} --gtest_filter=EyeTest*)
add_test(testFinger ${TestExecutable} --gtest_filter=FingerAndGestureTest*)
add_test(testLayout ${TestExecutable} --gtest_filter=LayoutMangerTest*)
add_test(testMatcher ${TestExecutable} --gtest_filter=MatcherTest*)
add_test(testRuneReader ${TestExecutable} --gtest_filter=RuneReaderTest*)
add_test(testSolver ${TestExecutable} --gtest_filter=SolverTest*)
add_test(testGamePlayer ${TestExecutable} --gtest_filter=GamePlayerTest*)
add_test(testRuneProfiler ${TestExecutable} --gtest_filter=RuneProfilerTest*)
add_test(testSignal ${TestExecutable} --gtest_filter=Signalest*)
add_test(testScoreProfiler ${TestExecutable} --gtest_filter=ScoreProfilerTest*)
add_test(testPintoo ${TestExecutable} --gtest_filter=PintooTest*)
add_test(testResource ${TestExecutable} --gtest_filter=ResourceTest*)
add_test(testScoreProfilerSmall ${TestExecutable} --gtest_filter=*CaseBottomAll*)

# copy files used by uint test
execute_process(COMMAND cp -r ${CMAKE_SOURCE_DIR}/misc/ ${CMAKE_SOURCE_DIR}/build/)
# for xcode
execute_process(COMMAND cp -r ${CMAKE_SOURCE_DIR}/misc/ ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/Debug/)

