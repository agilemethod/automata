#include "gtest/gtest.h"
#include <thirdparty/json.hpp>
#include <thirdparty/base64.hpp>
#include <core/image.h>
#include <io/image.hpp>
#include <matapath/rune_sample_basic.h>
#include <matapath/rune_provider.h>

using namespace nlohmann;

class ResourceTest : public ::testing::Test
{
protected:
	virtual void SetUp()
	{
	}

	virtual void TearDown()
	{
	}

protected:
};

TEST_F(ResourceTest, SimpleTest)
{
	std::string str = "hello base64!!!";
	std::string tmp = base64_encode(str.data(), str.size());

	EXPECT_EQ(base64_decode(tmp), str);
}

TEST_F(ResourceTest, ImageTest)
{
	Image source("sample.jpg");

	json tmp1;
	tmp1["sample"] << source;

	Image tmp2;
	tmp1["sample"] >> tmp2;
	tmp2.save("tmp_decode_sample.jpg");

	EXPECT_EQ(tmp2.width(), source.width());
	EXPECT_EQ(tmp2.height(), source.height());
}

TEST_F(ResourceTest, HeaderTest)
{
	json basic_rune_sample = json::parse(RuneSampleArchive::BASIC);

	EXPECT_EQ(basic_rune_sample["version"], "1.0");

	std::cout << "nums of sample: " << basic_rune_sample["runes"].size() << "\n";
	Image img;
	basic_rune_sample["runes"][0]["image"] >> img;
	std::cout << basic_rune_sample["runes"][0]["type"] << "\n";
	std::cout << basic_rune_sample["runes"][0]["prop"] << "\n";
	std::cout << basic_rune_sample["runes"][0]["threshold"] << "\n";
	img.save("rune_from_sample.jpg");
}

TEST_F(ResourceTest, RuneProviderTest)
{
	RuneProvider rp(RuneSampleArchive::BASIC);
	int i = 0;
	for (auto s : rp.samples)
	{
		s.image.save("basic_sample" + std::to_string(i++) + ".jpg");
		std::cout << (int)s.type << std::endl;
	}
}
