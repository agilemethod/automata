// Usage:
// imgprof <input img> -roi <x y w h>
// imgprof <input img> -roia <ax ay aw ah>
// imgprof <input img> -cut <output img> -roi <x y w h> 
// imgprof <input img> -diff <other img>
// imgprof <input img> -match <templ img> -method <n>

#include <cstdlib>
#include <cstdio>
#include <exception>
#include <string>
#include <iostream>
#include <sstream>
#include <opencv2/opencv.hpp>

class ImageProfiler
{
public:
    void open_image(const std::string & filename);
    void profile();
    void cut(const std::string & outfilename);
    void diff(const std::string & otherfilename);
    void match(const std::string & otherfilename, int method);

public:
    float roi_x = 0;
    float roi_y = 0;
    float roi_w = 0;
    float roi_h = 0;
    bool enable_ratio_mode = false;

private:
	void profile_impl(const cv::Mat & img, cv::Scalar * rgb, cv::Scalar * hsv);
	cv::Rect get_roi();

private:
    cv::Mat input_image;
};

class ArgumentHelper
{
public:
    ArgumentHelper(int argc, char * argv[])
        : argc(argc), argv(argv)
    {
    }

    void parse_roi(ImageProfiler * image_profiler, int start_position)
    {
        image_profiler->roi_x = std::atof(argv[start_position + 0]);
        image_profiler->roi_y = std::atof(argv[start_position + 1]);
        image_profiler->roi_w = std::atof(argv[start_position + 2]);
        image_profiler->roi_h = std::atof(argv[start_position + 3]);
    }

private:
    int argc;
    char ** argv;
};

void show_usage()
{
	std::string usage = R"(
Usage:
imgprof <input img> -roi <x y w h>
imgprof <input img> -roia <ax ay aw ah>
imgprof <input img> -cut <output img> -roi <x y w h> 
imgprof <input img> -diff <other img>
imgprof <input img> -match <templ img> -method <n>
	)";

	std::cout << usage << std::endl;
}

int main(int argc, char * argv[])
{
    if (argc < 3)
    {
        show_usage();
        return 0;
    }

    ImageProfiler image_profiler;
    ArgumentHelper argument_helper(argc, argv);

    image_profiler.open_image(argv[1]);

    std::string action(argv[2]);
    if (action == "-cut")
    {
        std::string roi(argv[4]);
        if (roi == "-roi")
        {
            argument_helper.parse_roi(&image_profiler, 5);
            image_profiler.enable_ratio_mode = false;
        }
        else if (roi == "-roia")
        {
            argument_helper.parse_roi(&image_profiler, 5);
            image_profiler.enable_ratio_mode = true;
        }
        else
        {
            show_usage();
            return 0;
        }

        image_profiler.cut(argv[3]);
    }
    else if (action == "-diff")
    {
        image_profiler.diff(argv[3]);
    }
    else if (action == "-match")
    {
        image_profiler.match(argv[3], std::atoi(argv[5]));
    }
    else
    {
        std::string roi(argv[2]);
        if (roi == "-roi")
        {
            argument_helper.parse_roi(&image_profiler, 3);
            image_profiler.enable_ratio_mode = false;
        }
        else if (roi == "-roia")
        {
            argument_helper.parse_roi(&image_profiler, 3);
            image_profiler.enable_ratio_mode = true;
        }
        else
        {
            show_usage();
            return 0;
        }

        image_profiler.profile();
    }
    
    return 0;
}

void ImageProfiler::open_image(const std::string & filename)
{
	input_image = cv::imread(filename);
	if (input_image.cols == 0 || input_image.rows == 0)
	{
		throw std::runtime_error("open image failed!");
	}
}

void ImageProfiler::profile()
{
	cv::Scalar rgb;
	cv::Scalar hsv;

	cv::Mat roi_image(input_image, get_roi());

	profile_impl(roi_image, &rgb, &hsv);

	std::printf("SIZE %d %d\n", roi_image.cols, roi_image.rows);
	std::printf("RGB %05.1f %05.1f %05.1f\n", rgb[0], rgb[1], rgb[2]);
	std::printf("HSV %05.1f %05.1f %05.1f\n", hsv[0], hsv[1]*100, hsv[2]/255*100);
}

void ImageProfiler::profile_impl(const cv::Mat & img, cv::Scalar * rgb, cv::Scalar * hsv)
{
	cv::Scalar mean = cv::mean(img);
	*rgb = mean;

	cv::Mat pixel(1, 1, CV_32FC3, mean);
	cv::cvtColor(pixel ,pixel ,CV_RGB2HSV);
	*hsv = cv::mean(pixel);
}

cv::Rect ImageProfiler::get_roi()
{
	cv::Rect result;
	if (enable_ratio_mode)
	{
		result.x = input_image.cols * roi_x;
		result.y = input_image.rows * roi_y;
		result.width = input_image.cols * roi_w;
		result.height = input_image.rows * roi_h;
	}
	else
	{
		result.x = roi_x;
		result.y = roi_y;
		result.width = roi_w;
		result.height = roi_h;
	}
	//std::printf("ROI %d %d %d %d\n", result.x, result.y, result.width, result.height);
	return result;
}

void ImageProfiler::cut(const std::string & outfilename)
{
	cv::Mat cropped_image = input_image(get_roi());
	cv::imwrite(outfilename, cropped_image);
}

void ImageProfiler::diff(const std::string & otherfilename)
{
	cv::Mat other = cv::imread(otherfilename);
	if (other.cols == 0 || other.rows == 0)
	{
		throw std::runtime_error("open other image failed!");
	}

	cv::Scalar rgb1;
	cv::Scalar hsv1;

	profile_impl(input_image, &rgb1, &hsv1);

	cv::Scalar rgb2;
	cv::Scalar hsv2;

	profile_impl(other, &rgb2, &hsv2);

	std::cout << "RGBNorm " << cv::norm(cv::Mat(1, 1, CV_32FC3, rgb1), cv::Mat(1, 1, CV_32FC3, rgb2)) << std::endl;
	std::cout << "HSVNorm " << cv::norm(cv::Mat(1, 1, CV_32FC3, hsv1), cv::Mat(1, 1, CV_32FC3, hsv2)) << std::endl;
}

void ImageProfiler::match(const std::string & otherfilename, int method)
{
	std::cout << "match " << otherfilename << " " << method << std::endl;
}
