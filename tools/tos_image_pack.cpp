#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <iostream>
#include <string>
#include <vector>
#include <matapath/rune.h>
#include <core/image.h>
#include <io/image.hpp>

std::vector<std::string> open_dir(const std::string & folder_path)
{
	std::vector<std::string> file_list;
	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir (folder_path.c_str())) != NULL)
	{
		while ((ent = readdir (dir)) != NULL)
		{
			std::string filename(ent->d_name);
			if (filename != "." && filename != "..")
			{
				file_list.push_back(filename);
			}
		}
		closedir (dir);
	}
	return file_list;
}

int rune_type(const std::string & filename)
{
	static std::vector<std::string> keywords = {"", "水", "火", "木", "光", "暗", "心", "問"};
	for (size_t i = 1; i < keywords.size(); i++)
	{
		size_t pos = filename.find(keywords[i]);
		if (pos != std::string::npos)
		{
			return i; // TosRune::RuneType
		}
	}

	throw std::runtime_error("cannot find rune type");
	return 0;
}

int rune_prop(const std::string & filename)
{
	static std::map<std::string, int> lookup;
	if (lookup.empty())
	{
		lookup["普"] = TosRune::RuneProp::PropNormal;
		lookup["強"] = TosRune::RuneProp::PropEnhanced;
		lookup["燃"] = TosRune::RuneProp::PropIgnited;
		lookup["風"] = TosRune::RuneProp::PropWild;
		lookup["凍1"] = TosRune::RuneProp::PropFreezedInit;
		lookup["凍2"] = TosRune::RuneProp::PropFreezed;
		lookup["凍3"] = TosRune::RuneProp::PropFreezed;
		lookup["凍4"] = TosRune::RuneProp::PropFreezed;
		lookup["閃"] = TosRune::RuneProp::PropLightning;
		lookup["蝕"] = TosRune::RuneProp::PropEtched;
		lookup["鎖"] = TosRune::RuneProp::PropLocked;
		lookup["束"] = TosRune::RuneProp::PropLaser;
		lookup["黑"] = TosRune::RuneProp::PropNormal;
	}

	for (auto & item : lookup)
	{
		size_t pos = filename.find(item.first);
		if (pos != std::string::npos)
		{
			return item.second;
		}
	}

	throw std::runtime_error("cannot find rune prop");
	return 0;
}

float rune_threshold(const std::string & filename)
{
	float th = 0.8;
	if (filename == "水普.png")
	{
		th = 0.95;
	}
	else if (filename == "火普.png")
	{
		th = 0.91;
	}
	else if (filename == "木普.png")
	{
		th = 0.94;
	}
	else if (filename == "光普.png")
	{
		th = 0.95;
	}
	else if (filename == "暗普.png")
	{
		th = 0.95;
	}
	else if (filename == "心普.png")
	{
		th = 0.96;
	}
	else if (filename == "問普.png")
	{
		th = 0.95;
	}
	else if (filename == "問風.png")
	{
		th = 0.97;
	}
	else if (filename == "水強.png")
	{
		th = 0.98;
	}
	else if (filename == "火強.png")
	{
		th = 0.96;
	}
	else if (filename == "木強.png")
	{
		th = 0.95;
	}
	else if (filename == "光強.png")
	{
		th = 0.95;
	}
	else if (filename == "暗強.png")
	{
		th = 0.97;
	}
	else if (filename == "心強.png")
	{
		th = 0.98;
	}
	else
	{
		th = 0.8;
	}

	return th;
}

int main(int argc, char * argv[])
{
	if (argc != 2)
	{
		std::cerr << "Usage: folder_path\n";
		return 0;
	}

	std::vector<std::string> file_list = open_dir(argv[1]);

	/*  result
		- version: str
		- runes: array
			- image: str
			- type: str
			- prop: str
			- threshold: real
	*/

	nlohmann::json result;
	result["version"] = "1.1";
	result["runes"] = nlohmann::json::array();
	std::string prefix(argv[1]);
	for (auto & file : file_list)
	{
		std::cout << "packing... " << file << std::endl;
		nlohmann::json each;
		Image img(prefix + "/" + file);
		img.resize(32, 32);

		each["image"] << img;
		each["type"] = rune_type(file);
		each["prop"] = rune_prop(file);
		each["threshold"] = rune_threshold(file);

		result["runes"].push_back(each);
	}

	std::string temp = result.dump();
	std::string text;
	text.reserve(temp.size());
	for (size_t i = 0; i < temp.length(); i++)
	{
		if (temp[i] == '"')
		{
			text += "\\";
		}
		text += temp[i];
	}

	std::cout << text;

	return 0;
}
