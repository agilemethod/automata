var fs = require('fs');
var mkdirp = require('mkdirp');
var exec = require('child_process').exec;

var CELL_CUT_BIN = "/Users/jeremy/automata/bin/tos_cell_cut";
var PATH_PREFIX = "/Users/jeremy/automata/build/tos_testcase/tos_tests/";
var FILE_EXTENSION = ".png";

var CellInfo = function(str)
{
	var args = str.split(" ");

	this.screen = args[0];
	this.row = args[1];
	this.col = args[2];
	this.out = args[3];
}

function directory(file_path)
{	
	return file_path.substr(0, file_path.lastIndexOf("/"));
}

var cells = [];

// Water
cells.push(new CellInfo(PATH_PREFIX + "IMG_0045.PNG 0 4 rune/water/lightning" + FILE_EXTENSION));
cells.push(new CellInfo(PATH_PREFIX + "IMG_0052.PNG 1 3 rune/water/etched" + FILE_EXTENSION));

// Fire
cells.push(new CellInfo(PATH_PREFIX + "IMG_0035.PNG 1 2 rune/fire/etched" + FILE_EXTENSION));

// Wood
cells.push(new CellInfo(PATH_PREFIX + "IMG_0018.PNG 0 2 rune/wood/lightning" + FILE_EXTENSION));
cells.push(new CellInfo(PATH_PREFIX + "IMG_0035.PNG 2 2 rune/wood/etched" + FILE_EXTENSION));

// Light
cells.push(new CellInfo(PATH_PREFIX + "IMG_0084.PNG 4 2 rune/light/lightning" + FILE_EXTENSION));
cells.push(new CellInfo(PATH_PREFIX + "IMG_0049.PNG 1 3 rune/light/etched" + FILE_EXTENSION));

// Dark
cells.push(new CellInfo(PATH_PREFIX + "IMG_0040.PNG 3 1 rune/dark/lightning" + FILE_EXTENSION));
cells.push(new CellInfo(PATH_PREFIX + "IMG_0044.PNG 2 2 rune/dark/etched" + FILE_EXTENSION));

// Heart
cells.push(new CellInfo(PATH_PREFIX + "IMG_0040.PNG 4 4 rune/heart/lightning" + FILE_EXTENSION));
cells.push(new CellInfo(PATH_PREFIX + "IMG_0043.PNG 2 1 rune/heart/etched" + FILE_EXTENSION));

for (var i = 0, l = cells.length; i < l; i++)
{
	var func = function(index)
	{
		return function(exists)
		{
			mkdirp(directory(cells[index].out), function(err)
			{
				if (err)
				{
					console.log(err);
				}
				else
				{
					console.log("creating...", cells[index].out);
					var cmd = CELL_CUT_BIN + " " + cells[index].screen + " " + cells[index].row + " " + cells[index].col + " " + cells[index].out;
					exec(cmd, function(error, stdout, stderr)
					{
						if (error || stdout || stderr)
							console.log("error", error, "stdout", stdout, "stderr", stderr);
					});
				}
			});
		}
	};

	fs.exists(cells[i].screen, func(i));
}
