#include <cstdlib>
#include <iostream>
#include <vector>
#include <core/image.h>
#include <core/matcher.h>
#include <matapath/rune.h>
#include <matapath/layout.h>

Image extract_cell(const Image & screen_img, size_t row, size_t col)
{
	LayoutManager layout_manager(std::make_shared<TosGameLayout>());
	layout_manager.set_rows_count(5);
	layout_manager.set_cols_count(6);
	layout_manager.set_resolution(screen_img.width(), screen_img.height());

	auto cell = layout_manager.cell(row, col);
	return screen_img.crop(cell.x, cell.y, cell.width, cell.height);
}


int main(int argc, char *argv[])
{
	if (argc != 5)
	{
		std::cerr << "Usage: screen_image row col output_image\n";
		return 0;
	}

	Image cell = extract_cell(Image(argv[1]), std::atoi(argv[2]), std::atoi(argv[3]));
	cell.save(argv[4]);

	return 0;
}
