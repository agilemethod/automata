const BLUE=0;
const RED=1;
const GREEN=2;
const YELLOW=3;
const HEART=4;
const COPPER=5;
const SLIVER=6;
const GOLD=7;
const DIAMOND=8;
const UNIVERSAL=9;


var MatchFinder = function()
{
	this._board = null;
	this._step = 1;
}

MatchFinder.prototype.set_board = function(board)
{
	this._board = board;
	this._step = 1;
}

MatchFinder.prototype.test = function(nth)
{
	var l = this._board.length;

	var type = this._board[nth];
	var count = 1;
	var index = [nth];
	this._step = 1;
	
	for (var i = nth + 1; i < l; i++)
	{
		var box = this._board[i];
		if (type == UNIVERSAL && box != UNIVERSAL)
		{
			type = box;
		}
		if (box == type)
		{
			count++;
			this._step++;
			index.push(i);
		}
		else if (box >= COPPER && box <= DIAMOND)
		{
			this._step++;
		}
		else if (box == UNIVERSAL)
		{
			count++;
			this._step++;
			index.push(i);
		}
		else
		{
			break;
		}
	}

	if (count < 2)
	{
		count = 0;
		index = [];
		this._step = 1;
	}

	return {"count": count, "type": type, "index": index};
}

MatchFinder.prototype.step_count = function()
{
	return this._step;
}

match_finder = new MatchFinder();

function find_match(board)
{
	var result = [];
	for (var round = 1; true; round++)
	{
		var matches = find_match_iteration(board);

		var has_more_than_three = false;
		for (var i = 0; i < matches.length; i++)
		{
			if (matches[i].count >= 3)
			{
				has_more_than_three = true;
				result.push(matches[i]);
				board = remove_matched_box(board, matches[i])
			}
		}
		if (!has_more_than_three) break;
	}
	return result;
}

function find_match_iteration(board)
{
	var matches = [];
	match_finder.set_board(board);
	for (var i = 0, l = board.length; i < l;)
	{
		var match = match_finder.test(i);
		if (match.count > 1)
		{
			matches.push(match);
		}

		i += match_finder.step_count();
	}

	return matches;
}

function remove_matched_box(board, match)
{
	var new_board = [];

	var fill_type = COPPER;
	var fill_count = Math.floor(match.count / 3);
	if (match.type == DIAMOND)
	{
		fill_type = DIAMOND;
	}
	else if (match.type >= COPPER)
	{
		fill_type = match.type + 1;
	}

	for (var i = 0; i < match.index.length; i++)
	{
		if (fill_count > 0)
		{
			board[match.index[i]] = fill_type;
			fill_count--;
		}
		else
		{
			board[match.index[i]] = -1;
		}
	}

	for (var i in board)
	{
		if (board[i] != -1)
		{
			new_board.push(board[i]);
		}
	}
	return new_board;
}

testcase = {};
testcase.board1 = [BLUE, BLUE, RED, RED, UNIVERSAL, BLUE];
testcase.board2 = [BLUE, BLUE, COPPER, SLIVER, GOLD, DIAMOND, BLUE];
testcase.board3 = [BLUE, UNIVERSAL, UNIVERSAL, BLUE, UNIVERSAL, BLUE, BLUE, BLUE, BLUE];
testcase.board4 = [BLUE, BLUE];
testcase.board5 = [UNIVERSAL, BLUE, BLUE];

for (var t in testcase)
{
	console.log(t, testcase[t]);
	console.log(find_match(testcase[t]));
}

