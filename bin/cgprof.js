const fs = require('fs');
const execSync = require('child_process').execSync;
const samples = ["yellow", "red", "blue", "green", "gray", "pink", "copper", "copper_light", "sliver", "gold", "gold_light"];

const boxes = {
	"yellow"	: [{c:[189.2, 93.0, 83.3], t:"normal"}, {c:[184.7, 65, 95], t:"normal"}],
	"red"		: [{c:[239.4, 88.2, 79.9], t:"normal"}, {c:[239.4, 85, 84], t:"blink"}
				 , {c:[237, 40, 95], t:"blink"}],
	"blue"		: [{c:[ 41.8, 93.0, 78.7], t:"normal"}, {c:[51.4, 45, 95], t:"blink"}],
	"green"		: [{c:[116.3, 97.8, 61.6], t:"normal"}, {c:[111.6, 65.3, 95], t:"blink"}],
	"gray"		: [{c:[ 90.9,  3.4, 79.5], t:"normal"}],
	"pink"		: [{c:[252.5, 39.1, 96.0], t:"normal"}, {c:[264.5, 7.8, 95], t:"blink"}],
	"copper"	: [{c:[221.2, 59.3, 63.2], t:"normal"}, {c:[216.1, 31.7, 85.6], t:"normal"}
				 , {c:[218.8, 46.5, 79.2], t:"normal"}, {c:[215, 45, 92], t:"blink"}, {c:[212, 30, 95], t:"blink"}],
	"sliver"	: [{c:[ 34.1, 16.7, 64.4], t:"normal"}, {c:[ 37, 12.6, 70.9], t:"normal"}],
	"gold"		: [{c:[197.6, 72.0, 56.0], t:"normal"}, {c:[207.7, 55.7, 64.2], t:"normal"}],
};

const lu_boxes = {
	"yellow"	: [182.6, 73.8, 95.7],
	"red"		: [226.3, 79.3, 88.9],
	"blue"		: [ 53.6, 61.8, 98.2],
	"green"		: [117.2, 59.1, 91.7],
	"pink"		: [    0,    0,  100],
}

const answer = {
	"cg00.png" : ['green','copper','copper','red','blue','yellow','yellow','green','pink','gray','blue'],
	"cg01.png" : ['green','copper','copper','red','blue','yellow','yellow','green','pink','gray','blue'],
	"cg02.png" : ['sliver','green','green','red','yellow','blue','green','yellow','red','red','green','green','blue'],
	"cg03.png" : ['gold','yellow','blue','copper','red','yellow','blue','green','red','blue','red','yellow','blue'],
	"cg04.png" : ['copper','copper','yellow','yellow','yellow','green','red','red','red','green','yellow','red'],
	//"cg05.png" : ['gold','yellow','blue','copper','red','yellow','blue','green','red','blue','red','yellow','blue'],
	//"cg06.png" : ['yellow','red','blue','copper','copper','blue','copper','yellow','blue','gray','yellow','blue','blue'],
	//"cg07.png" : ['sliver','green','green','red','yellow','blue','green','yellow','red','red','green','green','blue'],
	//"cg08.png" : ['yellow','red','blue','green','green','green','green','green','yellow','green','yellow','yellow','blue'],
	//"cg09.png" : ['sliver','sliver','blue','red','red','pink','red','blue','blue','yellow','yellow','red'],
	//"cg10.png" : ['yellow','yellow','red','pink','pink','green','green','green','green'],
	//"cg11.png" : ['sliver', 'red', 'copper', 'green', 'green', 'green', 'green' ],
	//"cg12.png" : ['copper','sliver','copper','yellow','pink','blue','yellow','green','blue'],
	//"cg13.png" : ['yellow','yellow','yellow','copper','yellow','green','red','green','blue','red','blue','green'],
	//"cg14.png" : ['copper', 'copper','blue','blue','blue','blue','blue','blue','pink','yellow','red','blue'],
	//"cg15.png" : ['green','green','blue','red','red','red','red','blue','pink','red','red','blue','green'],
	//"cg16.png" : ['copper','copper','copper','copper','copper','copper','green','green','yellow','green','blue','green','blue'],
};

function system(command)
{
	return execSync(command).toString();
}

function get_lu_board(filename, w, h)
{
	var board_w = (44 * w/h + 60) * h/512;
	var board_h = 5 * h/512;
	var board_t = board_h * 3;
	command = "./imgprof " + filename + " -cut lu_board.png -roi " + 0 + " " + board_t + " " + board_w + " " + board_h;	
	console.log(command);
	system(command);
	var box_w = board_w / 5;
	var sample_w = box_w / 4;
	var sample_h = board_h / 2;
	for (var i = 4; i >= 0; i--)
	{
		var outfile = "lu_" + i + ".png";
		var x = box_w * i + box_w / 3;
		command = "./imgprof lu_board.png -cut " + outfile + " -roi " + x + " " + 0 + " " + sample_w + " " + sample_h;
		system(command);

		command = "./imgprof " + outfile + " -roia 0 0 1 1";
		var result = system(command);
		result = result.split("\n");
		//console.log(outfile, result[1], result[2]);
		result = result[2].split(" ");
		var o_h = +result[1];
		var o_s = +result[2];
		var o_v = +result[3];
		
		var min_diff = 999;
		var box_idx = -1;
		for (var c in lu_boxes)
		{
			var box_colors = lu_boxes[c];
			var s_h = box_colors[0];
			var s_s = box_colors[1];
			var s_v = box_colors[2];
			var diff = Math.abs(s_h - o_h)/256 * 10 + Math.abs(s_s - o_s)/100 * 2 + Math.abs(s_v - o_v)/100 * 4; 
			diff = Math.round(diff / 16 * 1000)
			if (diff < min_diff)
			{
				//console.log(result, diff, i, o_h, o_s, o_v);
				min_diff = diff;
				box_idx = c
			}
		}
		
		if (min_diff < 30)
		{
			var type = box_idx;
			var status = lu_boxes[box_idx];
			console.log("LU Box", type, status);
			//board.push({type: type, status: status, fall: fall});
		}
	}
}

function get_board(filename)
{
	var command;
	var result = system("./imgprof " + filename + " -roia 0 0 1 1");
	var size = result.split("\n")[0];

	command = "rm -rf out*.png screenshot.png";
	system(command);

	var h = 1776 || size.split(" ")[2];
	var w = size.split(" ")[1];
	var board_h = Math.floor(h);
	var board_height = Math.floor(h * 0.837890625);
	var board_width = Math.floor(h * 0.193359375 - h * 0.08);
	var board_top = Math.floor(h * 0.021484375);
	var board_left = Math.floor((w - h * 0.193359375) / 2 + h * 0.02);
	var box_h = Math.floor(h * 0.064453125);
	var box_w = Math.floor(board_width);
	var sample_h = Math.floor(box_h * 0.25);
	var sample_w = Math.floor(box_w);
	var interval = Math.floor(board_height / 50.0);
	var scan_y = board_height - box_h * 0.4;

	get_lu_board(filename, w, h);
	return [];
	command = "./imgprof " + filename + " -cut screenshot.png -roi " + board_left + " " + board_top + " " + board_width + " " + board_height;
	console.log(command);
	system(command);
	
	var last_box_type = 999;
	var last_scan_y = 0;
	
	var board = [];
	
	var outfile, lastfile;
	var fall = false;
	while (scan_y > 0)
	{
		scan_y = Math.floor(scan_y);

		outfile = "out_" + scan_y + ".png";
		command = "./imgprof screenshot.png -cut " + outfile + " -roi " + 0 + " " + scan_y + " " + sample_w + " " + sample_h;
		system(command);

		command = "./imgprof " + outfile + " -roia 0 0 1 1";
		var result = system(command);
		result = result.split("\n");
		console.log(outfile, result[1], result[2]);

		result = result[2].split(" ");
		var o_h = +result[1];
		var o_s = +result[2];
		var o_v = +result[3];

		var min_diff = 999;
		var box_idx = -1;
		var color_idx = -1;
		for (var i in boxes)
		{
			var box_colors = boxes[i];
			for (var color_i in box_colors)
			{
				var result = box_colors[color_i].c;
				var s_h = result[0];
				var s_s = result[1];
				var s_v = result[2];
				var diff = Math.abs(s_h - o_h)/256 * 10 + Math.abs(s_s - o_s)/100 * 2 + Math.abs(s_v - o_v)/100 * 4; 
				diff = Math.round(diff / 16 * 1000)
				if (diff < min_diff)
				{
					console.log(result, diff, i, o_h, o_s, o_v);
					min_diff = diff;
					box_idx = i
					color_idx = color_i;
				}
			}
		}

		// is box
		if (min_diff < 30)
		{
			if (last_scan_y - scan_y >= box_h || last_box_type != box_idx)
			{
				var type = box_idx;
				var status = boxes[box_idx][color_idx].t;
				if (last_scan_y - scan_y > box_h*1.3) {
					fall = true;
				}
				console.log("Box", scan_y, box_idx, min_diff, type, status);
				board.push({type: type, status: status, fall: fall});
				last_scan_y = scan_y;
				last_box_type = box_idx;	
			}
		}
		scan_y -= interval;
		lastfile = outfile;
	}
	return board;
}

function cut_board(filename)
{
	var command;
	var result = system("./imgprof " + filename + " -roia 0 0 1 1");
	var size = result.split("\n")[0];

	command = "rm -rf out*.png screenshot.png";
	system(command);

	var h = 1776 || size.split(" ")[2];
	var w = size.split(" ")[1];
	var board_h = Math.floor(h);
	var board_height = Math.floor(h * 0.837890625);
	var board_width = Math.floor(h * 0.193359375 - h * 0.08);
	var board_top = Math.floor(h * 0.021484375);
	var board_left = Math.floor((w - h * 0.193359375) / 2 + h * 0.02);
	var box_h = Math.floor(h * 0.064453125);
	var box_w = Math.floor(board_width);
	var sample_h = Math.floor(box_h * 0.25);
	var sample_w = Math.floor(box_w);
	var interval = Math.floor(board_height / 50);

	command = "./imgprof " + filename + " -cut screenshot.png -roi " + board_left + " " + board_top + " " + board_width + " " + board_height;
	console.log(command);
	system(command);

	var scan_y = board_height - box_h * 0.4;
	var outfile, lastfile;
	while (scan_y > 0)
	{
		scan_y = Math.floor(scan_y);

		outfile = "out_" + scan_y + ".png";
		command = "./imgprof screenshot.png -cut " + outfile + " -roi " + 0 + " " + scan_y + " " + sample_w + " " + sample_h;
		system(command);

		command = "./imgprof " + outfile + " -roia 0 0 1 1";
		var result = system(command);
		result = result.split("\n");
		console.log(outfile, result[1], result[2]);

		result = result[2].split(" ");
		var o_h = result[1];
		var o_s = result[2];
		var o_v = result[3];

		for (var i in samples)
		{
			var sample_name = "samples/" + samples[i] + ".png";
			command = "./imgprof " + sample_name + " -roia 0 0 1 1";	
			var result = system(command);
			result = result.split("\n");
			result = result[2].split(" ");
			var s_h = result[1];
			var s_s = result[2];
			var s_v = result[3];
			var diff = Math.abs(s_h - o_h)/256 * 2 + Math.abs(s_s - o_s)/100 + Math.abs(s_v - o_v)/100; 
			diff = Math.round(diff / 4 * 100)
			console.log("Diff" , diff, "[", samples[i], "]");
			//command = "./imgprof " + sample_name + " -diff " + outfile;
			//console.log(command);
			//var result = system(command);
			//result = result.split("\n");
			//console.log(result[0], result[1], "[", samples[i], "]");
		}

		scan_y -= interval;
		lastfile = outfile;
	}
}

for(var filename in answer)
{
	var board = get_board(filename);
	var ans_board = answer[filename];
	console.log(board);
	for (var i in ans_board)
	{
		var box = ans_board[i];
		if (box.type == undefined) break;
		if (box != board[i].type)
		{
			console.log("Different box:", filename, box, board[i]);
		}	
	}
}
